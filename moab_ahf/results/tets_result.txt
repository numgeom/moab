nverts = 6, nedges = 1, nfaces = 4, ncells = 4
conn[0] = 1, conn[1] = 3, conn[2] = 2, conn[3] = 4, conn[4] = 0, conn[5] = 0, 
conn[0] = 1, conn[1] = 2, conn[2] = 5, conn[3] = 4, conn[4] = 0, conn[5] = 0, 
conn[0] = 1, conn[1] = 2, conn[2] = 3, conn[3] = 6, conn[4] = 0, conn[5] = 0, 
conn[0] = 1, conn[1] = 5, conn[2] = 2, conn[3] = 6, conn[4] = 0, conn[5] = 0, 

Total storage = 416
Total amortized storage = 991
Entity storage = 416
Amortized entity storage = 416
Adjacency storage = 0
Amortized adjacency storage = 16
Tag storage = 0
Amortized tag storage = 559

MeshType = 6
Finished creating sibling half-verts map for curve mesh
Finished creating incident half-verts
Finished constructing sibling halfedges map for a surface mesh
Finished creating incident half-edges map
Finished constructing sibling halffaces map for volume mesh
Finished creating incident half-faces
Time taken to construct the MDS = 0.000244856 secs

Total storage = 1520
Total amortized storage = 3169
Entity storage = 416
Amortized entity storage = 416
Adjacency storage = 528
Amortized adjacency storage = 592
Tag storage = 576
Amortized tag storage = 2161

1D QUERIES
For vertex 1: 
Number of incident ents = 1
adjents[0] = <1152921504606846977, 

For vertex 2: 
Number of incident ents = 1
adjents[0] = <1152921504606846977, 

For vertex 3: 
Number of incident ents = 0

For vertex 4: 
Number of incident ents = 0

For vertex 5: 
Number of incident ents = 0

For vertex 6: 
Number of incident ents = 0

Average time taken to compute incident edges to a vertex =  7.15256e-06 secs

For edge 1152921504606846977: 
Number of neighbor ents = 0

Average time taken to compute neighbor edges of an edge = 9.05991e-06 secs

2D QUERIES
For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = 2305843009213693953, 
adjents[1] = 2305843009213693954, 
adjents[2] = 2305843009213693955, 
adjents[3] = 2305843009213693956, 

Average time taken to compute incident faces on an edge =  4.60148e-05 secs

For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = <2305843009213693953, 0>, 
adjents[1] = <2305843009213693954, 0>, 
adjents[2] = <2305843009213693955, 0>, 
adjents[3] = <2305843009213693956, 0>, 

Average time taken to compute incident half-edges on an edge =  3.29018e-05 secs

For face 2305843009213693953: 
Number of neighbor ents = 3
adjents[0] = 2305843009213693954, 
adjents[1] = 2305843009213693955, 
adjents[2] = 2305843009213693956, 

For face 2305843009213693954: 
Number of neighbor ents = 3
adjents[0] = 2305843009213693955, 
adjents[1] = 2305843009213693956, 
adjents[2] = 2305843009213693953, 

For face 2305843009213693955: 
Number of neighbor ents = 3
adjents[0] = 2305843009213693956, 
adjents[1] = 2305843009213693953, 
adjents[2] = 2305843009213693954, 

For face 2305843009213693956: 
Number of neighbor ents = 3
adjents[0] = 2305843009213693953, 
adjents[1] = 2305843009213693954, 
adjents[2] = 2305843009213693955, 

Average time taken to compute neighbor faces of a face = 1.24574e-05 secs

3D QUERIES
For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = 5764607523034234884, 
adjents[1] = 5764607523034234882, 
adjents[2] = 5764607523034234881, 
adjents[3] = 5764607523034234883, 

Average time taken to compute incident cells on an edge  =  2.81334e-05 secs

For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = <5764607523034234884, 2>, 
adjents[1] = <5764607523034234882, 0>, 
adjents[2] = <5764607523034234881, 2>, 
adjents[3] = <5764607523034234883, 0>, 

Average time taken to compute incident implicit edges of cells on an edge  =  5.6982e-05 secs

For face 2305843009213693953: 
Number of incident ents = 2
adjents[0] = 5764607523034234882, 
adjents[1] = 5764607523034234881, 

For face 2305843009213693954: 
Number of incident ents = 2
adjents[0] = 5764607523034234883, 
adjents[1] = 5764607523034234881, 

For face 2305843009213693955: 
Number of incident ents = 2
adjents[0] = 5764607523034234884, 
adjents[1] = 5764607523034234883, 

For face 2305843009213693956: 
Number of incident ents = 2
adjents[0] = 5764607523034234884, 
adjents[1] = 5764607523034234882, 

Average time taken to compute incident cells on a face  =  1.52588e-05 secs

For face 2305843009213693953: 
Number of incident ents = 2
adjents[0] = <5764607523034234882, 0>, 
adjents[1] = <5764607523034234881, 2>, 

For face 2305843009213693954: 
Number of incident ents = 2
adjents[0] = <5764607523034234883, 3>, 
adjents[1] = <5764607523034234881, 3>, 

For face 2305843009213693955: 
Number of incident ents = 2
adjents[0] = <5764607523034234884, 2>, 
adjents[1] = <5764607523034234883, 0>, 

For face 2305843009213693956: 
Number of incident ents = 2
adjents[0] = <5764607523034234884, 3>, 
adjents[1] = <5764607523034234882, 3>, 

Average time taken to compute incident half-faces on a face =  1.35303e-05 secs

For cell 5764607523034234881: 
Number of neighbor ents = 2
adjents[0] = 5764607523034234882, 
adjents[1] = 5764607523034234883, 

For cell 5764607523034234882: 
Number of neighbor ents = 2
adjents[0] = 5764607523034234881, 
adjents[1] = 5764607523034234884, 

For cell 5764607523034234883: 
Number of neighbor ents = 2
adjents[0] = 5764607523034234884, 
adjents[1] = 5764607523034234881, 

For cell 5764607523034234884: 
Number of neighbor ents = 2
adjents[0] = 5764607523034234883, 
adjents[1] = 5764607523034234882, 

Average time taken to compute neighbor cells of a cell =  1.0252e-05 secs


Total storage = 944
Total amortized storage = 1567
Entity storage = 416
Amortized entity storage = 416
Adjacency storage = 528
Amortized adjacency storage = 592
Tag storage = 0
Amortized tag storage = 559

