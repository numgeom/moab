nverts = 10, nedges = 1, nfaces = 4, ncells = 4
conn[0] = 1, conn[1] = 10, conn[2] = 3, conn[3] = 2, conn[4] = 9, conn[5] = 4, 
conn[0] = 6, conn[1] = 10, conn[2] = 1, conn[3] = 5, conn[4] = 9, conn[5] = 2, 
conn[0] = 1, conn[1] = 3, conn[2] = 7, conn[3] = 2, conn[4] = 4, conn[5] = 8, 
conn[0] = 1, conn[1] = 7, conn[2] = 6, conn[3] = 2, conn[4] = 8, conn[5] = 5, 

Total storage = 608
Total amortized storage = 1183
Entity storage = 608
Amortized entity storage = 608
Adjacency storage = 0
Amortized adjacency storage = 16
Tag storage = 0
Amortized tag storage = 559

MeshType = 6
Finished creating sibling half-verts map for curve mesh
Finished creating incident half-verts
Finished constructing sibling halfedges map for a surface mesh
Finished creating incident half-edges map
Finished constructing sibling halffaces map for volume mesh
Finished creating incident half-faces
Time taken to construct the MDS = 0.000313997 secs

Total storage = 2176
Total amortized storage = 3881
Entity storage = 608
Amortized entity storage = 608
Adjacency storage = 752
Amortized adjacency storage = 848
Tag storage = 816
Amortized tag storage = 2425

1D QUERIES
For vertex 1: 
Number of incident ents = 1
adjents[0] = <1152921504606846977, 

For vertex 2: 
Number of incident ents = 1
adjents[0] = <1152921504606846977, 

For vertex 3: 
Number of incident ents = 0

For vertex 4: 
Number of incident ents = 0

For vertex 5: 
Number of incident ents = 0

For vertex 6: 
Number of incident ents = 0

For vertex 7: 
Number of incident ents = 0

For vertex 8: 
Number of incident ents = 0

For vertex 9: 
Number of incident ents = 0

For vertex 10: 
Number of incident ents = 0

Average time taken to compute incident edges to a vertex =  4.72069e-06 secs

For edge 1152921504606846977: 
Number of neighbor ents = 0

Average time taken to compute neighbor edges of an edge = 9.05991e-06 secs

2D QUERIES
For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = 3458764513820540930, 
adjents[1] = 3458764513820540931, 
adjents[2] = 3458764513820540932, 
adjents[3] = 3458764513820540929, 

Average time taken to compute incident faces on an edge =  4.29153e-05 secs

For edge 1152921504606846977: 
Number of incident ents = 4
adjents[0] = <3458764513820540930, 3>, 
adjents[1] = <3458764513820540931, 0>, 
adjents[2] = <3458764513820540932, 0>, 
adjents[3] = <3458764513820540929, 0>, 

Average time taken to compute incident half-edges on an edge =  3.29018e-05 secs

For face 3458764513820540929: 
Number of neighbor ents = 3
adjents[0] = 3458764513820540930, 
adjents[1] = 3458764513820540931, 
adjents[2] = 3458764513820540932, 

For face 3458764513820540930: 
Number of neighbor ents = 3
adjents[0] = 3458764513820540931, 
adjents[1] = 3458764513820540932, 
adjents[2] = 3458764513820540929, 

For face 3458764513820540931: 
Number of neighbor ents = 3
adjents[0] = 3458764513820540932, 
adjents[1] = 3458764513820540929, 
adjents[2] = 3458764513820540930, 

For face 3458764513820540932: 
Number of neighbor ents = 3
adjents[0] = 3458764513820540929, 
adjents[1] = 3458764513820540930, 
adjents[2] = 3458764513820540931, 

Average time taken to compute neighbor faces of a face = 1.44839e-05 secs

3D QUERIES
For edge 1152921504606846977: 
Number of incident ents = 3
adjents[0] = 8070450532247928836, 
adjents[1] = 8070450532247928835, 
adjents[2] = 8070450532247928834, 

Average time taken to compute incident cells on an edge  =  2.7895e-05 secs

For edge 1152921504606846977: 
Number of incident ents = 3
adjents[0] = <8070450532247928836, 3>, 
adjents[1] = <8070450532247928835, 2>, 
adjents[2] = <8070450532247928834, 8>, 

Average time taken to compute incident implicit edges of cells on an edge  =  5.79357e-05 secs

For face 3458764513820540929: 
Number of incident ents = 2
adjents[0] = 8070450532247928834, 
adjents[1] = 8070450532247928833, 

For face 3458764513820540930: 
Number of incident ents = 2
adjents[0] = 8070450532247928833, 
adjents[1] = 8070450532247928835, 

For face 3458764513820540931: 
Number of incident ents = 2
adjents[0] = 8070450532247928836, 
adjents[1] = 8070450532247928835, 

For face 3458764513820540932: 
Number of incident ents = 2
adjents[0] = 8070450532247928836, 
adjents[1] = 8070450532247928834, 

Average time taken to compute incident cells on a face  =  1.24574e-05 secs

For face 3458764513820540929: 
Number of incident ents = 2
adjents[0] = <8070450532247928834, 1>, 
adjents[1] = <8070450532247928833, 0>, 

For face 3458764513820540930: 
Number of incident ents = 2
adjents[0] = <8070450532247928833, 2>, 
adjents[1] = <8070450532247928835, 0>, 

For face 3458764513820540931: 
Number of incident ents = 2
adjents[0] = <8070450532247928836, 0>, 
adjents[1] = <8070450532247928835, 2>, 

For face 3458764513820540932: 
Number of incident ents = 2
adjents[0] = <8070450532247928836, 2>, 
adjents[1] = <8070450532247928834, 2>, 

Average time taken to compute incident half-faces on a face =  1.2219e-05 secs

For cell 8070450532247928833: 
Number of neighbor ents = 2
adjents[0] = 8070450532247928834, 
adjents[1] = 8070450532247928835, 

For cell 8070450532247928834: 
Number of neighbor ents = 2
adjents[0] = 8070450532247928833, 
adjents[1] = 8070450532247928836, 

For cell 8070450532247928835: 
Number of neighbor ents = 2
adjents[0] = 8070450532247928833, 
adjents[1] = 8070450532247928836, 

For cell 8070450532247928836: 
Number of neighbor ents = 2
adjents[0] = 8070450532247928835, 
adjents[1] = 8070450532247928834, 

Average time taken to compute neighbor cells of a cell =  8.46386e-06 secs


Total storage = 1360
Total amortized storage = 2015
Entity storage = 608
Amortized entity storage = 608
Adjacency storage = 752
Amortized adjacency storage = 848
Tag storage = 0
Amortized tag storage = 559

