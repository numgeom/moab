/*This function tests the AHF datastructures on CST meshes*/
#include <iostream>
#include <vector>
#include <algorithm>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "moab/MeshTopoUtil.hpp"
#include "../mbahf/HalfFacetRep.hpp"
#include <sys/time.h>
#include <assert.h>
#include <time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core moab;
  Interface* mbImpl = &moab;
  MeshTopoUtil mtu(mbImpl);

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  /*Create ranges for handles of explicit elements of the mixed mesh*/
  Range verts, edges, faces, cells;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 1, edges);
  error = mbImpl->get_entities_by_dimension( 0, 2, faces); 
  error = mbImpl->get_entities_by_dimension( 0, 3, cells);
  
  int nverts = verts.size(); 
  int nedges = edges.size();
  int nfaces = faces.size();
  int ncells = cells.size();

  std::cout<<"nverts = "<<nverts<<", nedges = "<<nedges<<", nfaces = "<<nfaces<<", ncells = "<<ncells<<std::endl;

  std::cout<<"start_cell = "<<*cells.begin()<<std::endl;
  for (Range::iterator c = cells.begin(); c != cells.end(); ++c)
    {
      std::vector<EntityHandle> conn(6);
      error = mbImpl->get_connectivity(&*c, 1, conn);
      if (MB_SUCCESS != error) return error;
      for (int k = 0; k< 6; k++)
        std::cout<<"conn["<<k<<"] = "<<conn[k]-1<<", ";
      std::cout<<std::endl;
    }


  // Create an ahf instance
  HalfFacetRep ahf(mbImpl);

  // Call the initialize function which creates the maps for each dimension
  ahf.initialize();

  //Perform queries
  std::vector<EntityHandle> adjents;
  Range mbents, ahfents;

  //1D Queries //
  //IQ1: For every vertex, obtain incident edges 
  for (Range::iterator i = verts.begin(); i != verts.end(); ++i) {    
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 1, adjents);
    mbents.clear();
    error = mbImpl->get_adjacencies( &*i, 1, 1, false, mbents );

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  //NQ1:  For every edge, obtain neighbor edges  
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {    
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);
    mbents.clear();
    error = mtu.get_bridge_adjacencies( *i, 0, 1, mbents);

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  // 2D Queries
  //IQ2: For every edge, obtain incident faces
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {   
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 2, adjents);
    mbents.clear();
    error = mbImpl->get_adjacencies( &*i, 1, 2, false, mbents);

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  //NQ2: For every face, obtain neighbor faces 
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {   
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);
    mbents.clear();
    error = mtu.get_bridge_adjacencies( *i, 1, 2, mbents);

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  // 3D Queries
  // IQ 31: For every edge, obtain incident cells
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents);
    mbents.clear();
    error = mbImpl->get_adjacencies(&*i, 1, 3, false, mbents);

   /* assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);*/
  }

  //IQ32: For every face, obtain incident cells
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents);
    mbents.clear();
    error = mbImpl->get_adjacencies(&*i, 1, 3, false, mbents);

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  //NQ3: For every cell, obtain neighbor cells
  for (Range::iterator i = cells.begin(); i != cells.end(); ++i) {   
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);
    mbents.clear();
    error = mtu.get_bridge_adjacencies( *i, 2, 3, mbents);

    assert(adjents.size() == mbents.size());

    std::sort(adjents.begin(), adjents.end());
    std::copy(adjents.begin(), adjents.end(), range_inserter(ahfents));
    mbents = subtract(mbents, ahfents);
    assert(mbents.size() == 0);
  }

  std::cout<<"Finished Comparison"<<std::endl;
  ahf.deinitialize();

  return 0;
}

