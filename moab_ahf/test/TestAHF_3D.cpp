/*This function tests the AHF datastructures*/
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "../mbahf/ArrayHalfFacetMDS.hpp"
#include <sys/time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core moab;
  Interface* mbImpl = &moab;

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  // Create ranges for handles of all vertices and cells/
  Range verts, cells;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 3, cells); 
  
  EntityHandle start_cell = *cells.begin(); 
  ArrayHalfFacetMDS ahf(mbImpl);
  int index = ahf.get_index_from_type(start_cell);
  int nvpc = ahf.lConnMap3D[index].num_verts_in_cell;
  int nepc = ahf.lConnMap3D[index].num_edges_in_cell;
  int nfpc = ahf.lConnMap3D[index].num_faces_in_cell;

  // Create dense tags: sibhfs_cid, sibhfs_lfid, v2hf_cid, v2hf_lfid
  Tag sibhfs_cid, sibhfs_lfid, v2hf_cid, v2hf_lfid;
  EntityHandle * sdefval = new EntityHandle[nfpc];
  int * sval = new int[nfpc];
  EntityHandle idefval = 0;	
  int ival = 0;
  for (int i = 0; i < nfpc; i++){
    sdefval[i] = 0;
    sval[i] = 0;
  }

  error = mbImpl->tag_get_handle("SIBHFS_CID", nfpc, MB_TYPE_HANDLE, sibhfs_cid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
  error = mbImpl->tag_get_handle("SIBHFS_LFID", nfpc, MB_TYPE_INTEGER, sibhfs_lfid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
  error = mbImpl->tag_get_handle("V2HF_CID", 1, MB_TYPE_HANDLE, v2hf_cid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
  error = mbImpl->tag_get_handle("V2HF_LFID", 1, MB_TYPE_INTEGER, v2hf_lfid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
  std::cout << "Created the half-face tags" << std::endl;

  std::cout<<"index = "<<index<<"nvpc = "<<nvpc<<", nepc = "<<nepc<<", nfpc = "<<nfpc<<std::endl;

  // Construct the AHF maps
  double time_start, time_elapsed;
  time_start = wtime();
  error = ahf.determine_sibling_halffaces(cells, sibhfs_cid, sibhfs_lfid);
  error = ahf.determine_incident_halffaces(cells, sibhfs_cid, v2hf_cid, v2hf_lfid);
  time_elapsed = wtime()-time_start;
  std::cout<<"Time taken to create the AHF maps = "<<time_elapsed<<std::endl;
  
  // Print out
  std::cout<<"start_cell = "<<start_cell<<std::endl; 
  std::cout<<"<SIBHES_CID,SIBHES_LFID>"<<std::endl;	
  for (Range::iterator i = cells.begin(); i != cells.end(); ++i){
    std::vector<EntityHandle> cid(nfpc); 
    std::vector<int> lfid(nfpc);
    error = mbImpl->tag_get_data(sibhfs_cid, &*i, 1, &cid[0]);    
    error = mbImpl->tag_get_data(sibhfs_lfid, &*i, 1, &lfid[0]);
    for(int j = 0; j < nfpc; j++)
      if (cid[j] == 0)
	std::cout<<"<"<<cid[j]<<","<<lfid[j]<<">"<<" ";
      else
	std::cout<<"<"<<(cid[j]-start_cell+1)<<","<<lfid[j]<<">"<<" ";
    std::cout<<std::endl;
  }
  std::cout<<" Finished printing AHF:sibhfs "<<std::endl;
 
  std::cout<<"<V2HF_CID, V2HF_LFID>"<<std::endl;
  for (Range::iterator i = verts.begin(); i !=verts.end(); ++i){
    EntityHandle fid; int lid;
    error = mbImpl->tag_get_data(v2hf_cid, &*i, 1, &fid);
    error = mbImpl->tag_get_data(v2hf_lfid, &*i, 1, &lid);
    if (fid==0)
      std::cout<<"For vertex = "<<*i<<"::Incident halfface "<<fid<<"  "<<lid<<std::endl;
    else 
      std::cout<<"For vertex = "<<*i<<"::Incident halfface "<<(fid-start_cell+1)<<"  "<<lid<<std::endl;
  }

  std::cout<<" Finished printing AHF:v2hf"<<std::endl;
  std::cout<<" Starting incidence queries"<<std::endl;

  // Perform upward incidence and face-connected neighborhood queries
  
  // Query 1: Given an explicit edge, get all incident cells along with the local edge id corresponding to the given edge. 
  // First create an explicit edge. 
  std::vector<EntityHandle> conn(nvpc);
  error = mbImpl->get_connectivity(&*cells.begin(),1,conn);

  std::cout<<"nvpc = "<<nvpc<<std::endl;
  for (int c = 0; c< (int)conn.size(); c++)
    std::cout<<"conn["<<c<<"] = "<<conn[c];
  std::cout<<std::endl;

  EntityHandle exp_edge;
  error = mbImpl->create_element(MBEDGE, &conn[0], 2, exp_edge);
  assert(error == MB_SUCCESS);

  bool * ctags= new bool[cells.size()];
  for (int i = 0; i < (int)cells.size(); ++i){
    ctags[i] = false;
  }
  std::vector<EntityHandle> cells_1ring; 
  std::vector<int> leids_1ring;
  error = ahf.get_upward_incidences_3d(exp_edge, cells, sibhfs_cid, sibhfs_lfid, v2hf_cid, ctags, cells_1ring, leids_1ring);
  for (int i = 0; i < (int)cells.size(); ++i)
    assert(ctags[i]==false);

  std::cout<<" Finished Q1: Find incident cells with corresponding local edge ids on a given explicit edge"<<std::endl;

  // Query 2: Given an implicit edge, get all incident cells as well as local edge ids. 
  for (Range::iterator cid = cells.begin(); cid != cells.end(); ++cid){
    for (int leid = 0; leid < nepc ; leid ++){
      cells_1ring.clear(); leids_1ring.clear();
      std::cout<<"For cell = "<<(*cid-*cells.begin()+1)<<", leid = "<<leid<<":: "<<std::endl;

      error = ahf.get_upward_incidences_3d(*cid, leid, sibhfs_cid, sibhfs_lfid, cells_1ring, leids_1ring);  
      assert(cells_1ring.size() == leids_1ring.size());
      std::cout<<"For cell = "<<(*cid-*cells.begin()+1)<<", leid = "<<leid<<", #incident_cells = "<<cells_1ring.size()<<std::endl;
    }
  }

  std::cout<<" Finished Q2: Find incident cells with corresponding local edge ids on a given implicit edge"<<std::endl;
  
  // Query 3: Given an explicit face, get the incident cells.
  // First creat an explicit face. 
 
  int nv = ahf.lConnMap3D[index].hf2v_num[0]; 
  std::vector<EntityHandle> face_conn(nv);
  for (int i = 0; i < nv; i++)
    {
      int id = ahf.lConnMap3D[index].hf2v[0][i];
      face_conn.push_back(conn[id]);
    }
  EntityHandle exp_face;
  if (nv == 3)
    error = mbImpl->create_element(MBTRI, &face_conn[0], nv, exp_face);
  else if (nv == 4)
    error = mbImpl->create_element(MBQUAD, &face_conn[0], nv, exp_face);

  std::vector<EntityHandle> upin;
  error = ahf.get_upward_incidences_3d(exp_face, cells, sibhfs_cid, v2hf_cid,  ctags, upin);

  std::cout<<" Finished Q3: Find incident cells on a given explicit face"<<std::endl;

  // Query 4: Given an implicit face, get the incident cells.
  for (Range::iterator cid = cells.begin(); cid != cells.end(); ++cid){
    for (int lfid = 0; lfid < nfpc; lfid ++){
      upin.clear();
      error = ahf.get_upward_incidences_3d(*cid, lfid, nfpc, sibhfs_cid, upin);
    }
  }

  std::cout<<" Finished Q4: Find incident cells on a given implicit face"<<std::endl;

  // Query 5: Given a cell, get the face-connected neighbor cells
  std::vector<EntityHandle> ngb;
  for (Range::iterator c = cells.begin(); c != cells.end(); ++c){
    ngb.clear();
    error = ahf.get_neighbor_adjacencies_3d(*c, sibhfs_cid, ngb);
  }

  std::cout<<" Finished Q5: Find neighbor cells of a given cell"<<std::endl;


  delete [] sdefval;
  delete [] sval;
  delete [] ctags;
  
}

