/*This function tests the AHF datastructures*/
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "../mbahf/ArrayHalfFacetMDS.hpp"
#include <sys/time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core mb;
  Interface* mbImpl = &mb;

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  //Create ranges for handles of all vertices and edges
  Range verts, edges;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 1, edges); 
  
  EntityHandle start_edge = *edges.begin();

  // Create dense tags: sibhvs_eid, sibhvs_lvid, v2hv_eid, v2hv_lvid 
  Tag sibhvs_eid, sibhvs_lvid; 
  EntityHandle sdefval[2] = {0,0};  int sval[2] = {0,0};
  Tag v2hv_eid, v2hv_lvid;
  EntityHandle idefval = 0; int ival = 0;
  
  error = mbImpl->tag_get_handle("SIBHVS_EID", 2, MB_TYPE_HANDLE, sibhvs_eid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
  error = mbImpl->tag_get_handle("SIBHVS_LVID", 2, MB_TYPE_INTEGER, sibhvs_lvid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
  error = mbImpl->tag_get_handle("V2HV_EID", 1, MB_TYPE_HANDLE, v2hv_eid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
  error = mbImpl->tag_get_handle("V2HV_LVID", 1, MB_TYPE_INTEGER, v2hv_lvid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);  

  std::cout << "Created the half-vert tags" << std::endl;

  // Construct the AHF maps
  double time_start, time_elapsed;
  ArrayHalfFacetMDS ahf(mbImpl);
  time_start = wtime();
  error = ahf.determine_sibling_halfverts(verts, edges, sibhvs_eid, sibhvs_lvid);
  error = ahf.determine_incident_halfverts(edges, v2hv_eid, v2hv_lvid);
  time_elapsed = wtime()-time_start;
  std::cout<<"Time taken to create the AHF maps = "<<time_elapsed<<std::endl;

  // Print out the tags  
  std::cout<<"start_edge = "<<start_edge<<std::endl;
  std::cout<<"<SIBHVS_EID,SIBHVS_LVID>"<<std::endl;	
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i){
    EntityHandle eid[2];  int lvid[2];
    error = mbImpl->tag_get_data(sibhvs_eid, &*i, 1, eid);    
    error = mbImpl->tag_get_data(sibhvs_lvid, &*i, 1, lvid);
    if (eid[0] == 0)
      std::cout<<"<"<<eid[0]<<","<<lvid[0]<<">"<<"      "<<"<"<<(eid[1]-start_edge)<<","<<lvid[1]<<">"<<std::endl;
    else if (eid[1] == 0)
      std::cout<<"<"<<(eid[0]-start_edge)<<","<<lvid[0]<<">"<<"      "<<"<"<<eid[1]<<","<<lvid[1]<<">"<<std::endl;    
  }
 
  std::cout<<"<V2HV_EID, V2HV_LVID>"<<std::endl;
  for (Range::iterator i = verts.begin(); i !=verts.end(); ++i){
    EntityHandle eid; int lvid;
    error = mbImpl->tag_get_data(v2hv_eid, &*i, 1, &eid);
    error = mbImpl->tag_get_data(v2hv_lvid, &*i, 1, &lvid);   
    std::cout<<"For vertex = "<<*i<<"::Incident halfvertex "<<(eid-start_edge)<<"  "<<lvid<<std::endl;
  }  
  
  // Perfom upward incidence and vertex-connected neighborhood queries
  std::vector<EntityHandle> upin, ngb;
  
  //IQ1: For every vertex, obtain incident edges
  time_start = wtime();
  for (Range::iterator i = verts.begin(); i != verts.end(); ++i) {    
    upin.clear();
    error = ahf.get_upward_incidences_1d( *i, sibhvs_eid, sibhvs_lvid, v2hv_eid, v2hv_lvid, upin);        
    
    if (*i == *verts.begin()+99){
      std::cout<<"IQ1 for vertex ="<<*i<<" "<<upin.size()<<std::endl;
      int nents = upin.size();
      for (int j=0;j<nents;++j){
	std::cout<<"Entity "<<j<<" = "<<upin[j]<<std::endl;
      }
    }
  }
  time_elapsed = (wtime()-time_start)/(double)verts.size();
  std::cout<<"Average time taken to compute incident edges =  "<< time_elapsed<<" secs" <<std::endl;
  std::cout<<std::endl;

  //NQ1:  For every edge, obtain neighbor edges  
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {    
    ngb.clear();
    error = ahf.get_neighbor_adjacencies_1d( *i, sibhvs_eid, sibhvs_lvid, ngb);   

    if (*i == *edges.begin()+99){
      std::cout<<"NQ1 for edge = "<<*i<<" "<<ngb.size()<<std::endl;
      int nents = ngb.size();
      for (int j=0;j<nents;++j){
	std::cout<<"Entity "<<j<<" = "<<ngb[j]<<std::endl;
      }
    }
  }
  time_elapsed = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute neighbor edges = "<<time_elapsed<<" secs" << std::endl;  
  std::cout<<std::endl;
}

