#include "moab/Core.hpp"
#include "../mbahf/ArrayHalfFacetMDS.hpp"
#include "../mbadapt/MeshAdaptWithAHF.hpp"
#include "moab/Interface.hpp"

#include <iostream>
#include <sstream>
#include <map>
#include "sys/time.h"

using namespace moab;

#define STRINGIFY_(A) #A
#define STRINGIFY(A) STRINGIFY_(A)
#ifdef MESHDIR
std::string TestDir( STRINGIFY(MESHDIR) );
#else
std::string TestDir( "./" );
#endif

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main( int argc, char* argv[] )
{
  
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }

  char* input_filename = argv[1];
  ErrorCode error;
  
  Interface* mbImpl = new Core; // ( rank, nprocs );
  
  error = mbImpl->load_file( input_filename);
  if (MB_SUCCESS != error) {
    std::cout << "Trouble reading mesh file " << input_filename << ", exiting." << std::endl;
    return 1;
  }
  
  Range verts, edges, faces;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 1, edges);
  error = mbImpl->get_entities_by_dimension( 0, 2, faces); 
 
  int nverts = verts.size(); 
  int nedges = edges.size();
  int nfaces = faces.size();


  std::cout<<"nverts = "<<nverts<<", nedges = "<<nedges<<", nfaces = "<<nfaces<<std::endl;

  
  double time_start, time_elapsed;

  // Refine the mesh
  MeshAdaptWithAHF uref(mbImpl);
  int level = 2;


  Tag sibhes_fid, sibhes_leid, v2he_fid, v2he_leid;
  int nepf = uref.local_maps_2d(*faces.begin()); 
  
   EntityHandle * sdefval = new EntityHandle[nepf];
   int * sval = new int[nepf];
   EntityHandle  idefval = 0;
   int ival = 0;
   for (int i=0; i<nepf; i++){ 
     sdefval[i]=0; 
     sval[i]=0;
   }
   error = mbImpl->tag_get_handle("SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
   error = mbImpl->tag_get_handle("SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
   error = mbImpl->tag_get_handle("V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
   error = mbImpl->tag_get_handle("V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
   std::cout << "Created the half-edge tags" << std::endl;
   
   bool *ftags = new bool[faces.size()];
   for (int i = 0; i<(int)faces.size(); i++)
     ftags[i] = false;


   time_start = wtime();
   error = uref.determine_sibling_halfedges(faces, sibhes_fid, sibhes_leid);
   error = uref.determine_incident_halfedges(faces, sibhes_fid, v2he_fid, v2he_leid);
   time_elapsed = wtime()-time_start;
   std::cout<<"Time taken to create the AHF maps = "<<time_elapsed<<std::endl;
   
   time_start = wtime();
   
   error = uref.uniform_refinement_mixed_2d(verts, edges, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, level, MULTI_LEVEL);
   
   Range newverts, newedges, newfaces;
   error = mbImpl->get_entities_by_dimension( 0, 0, newverts);
   error = mbImpl->get_entities_by_dimension( 0, 1, newedges);
   error = mbImpl->get_entities_by_dimension( 0, 2, newfaces);

   int nwverts = newverts.size();
   int nwedges = newedges.size();
   int nwfaces = newfaces.size();
   std::cout<<"nverts = "<<nwverts<<", nedges = "<<nwedges<<", nfaces = "<<nwfaces<<std::endl;
   time_elapsed = wtime() - time_start;
   std::cout<<"Time taken for uniform refinement = "<<time_elapsed<<" secs"<<std::endl;

   delete [] ftags;
   return 0;
}

