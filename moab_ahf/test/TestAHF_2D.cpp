/*This function tests the AHF datastructures*/
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "../mbahf/ArrayHalfFacetMDS.hpp"
#include <sys/time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core moab;
  Interface* mbImpl = &moab;

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  //Create ranges for handles of all vertices and faces
  Range verts, faces;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 2, faces); 
  
  EntityHandle start_face = *faces.begin();  
  ArrayHalfFacetMDS ahf(mbImpl);
  int nepf = ahf.local_maps_2d(start_face); //Number of edges per face

  // Create dense tags: sibhes_fid, sibhes_leid, v2hf_fid, v2hf_leid
  Tag sibhes_fid, sibhes_leid, v2he_fid, v2he_leid;
  
  EntityHandle * sdefval = new EntityHandle[nepf];
  int * sval = new int[nepf];
  EntityHandle  idefval = 0;
  int ival = 0;
  for (int i=0; i<nepf; i++){ 
    sdefval[i]=0; 
    sval[i]=0;
  }
  
  error = mbImpl->tag_get_handle("SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
  error = mbImpl->tag_get_handle("SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
  error = mbImpl->tag_get_handle("V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
  error = mbImpl->tag_get_handle("V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
  std::cout << "Created the half-edge tags" << std::endl;

 std::vector<EntityHandle> Tconn(nepf);
 for (Range::iterator fid = faces.begin(); fid != faces.end(); ++fid){
   Tconn.clear();
   error = mbImpl->get_connectivity(&*fid, 1, Tconn); 
   
   for (int c = 0; c < (int)Tconn.size(); c++){
     std::cout<<"Conn["<<c<<"] = "<<Tconn[c]<<" ";
   }
   std::cout<<std::endl;
 }
 

  // Construct the AHF maps
  double time_start, time_elapsed;
  time_start = wtime();
  error = ahf.determine_sibling_halfedges(faces, sibhes_fid, sibhes_leid);
  error = ahf.determine_incident_halfedges(faces, sibhes_fid, v2he_fid, v2he_leid);
  time_elapsed = wtime()-time_start;
  std::cout<<"Time taken to create the AHF maps = "<<time_elapsed<<std::endl;

  // Print out
  std::cout<<"start_face = "<<start_face<<std::endl; 
  std::cout<<"<SIBHES_FID,SIBHES_LEID>"<<std::endl;	
  for (Range::iterator i = faces.begin(); i !=faces.end(); ++i){
    std::vector<EntityHandle> fid(nepf);  
    std::vector<int> leid(nepf);
    error = mbImpl->tag_get_data(sibhes_fid, &*i, 1, &fid[0]);    
    error = mbImpl->tag_get_data(sibhes_leid, &*i, 1, &leid[0]);
    for (int j=0; j<nepf; j++){
      if (fid[j] == 0)
	std::cout<<"<"<<fid[j]<<","<<leid[j]<<">"<<" ";
      else 
	std::cout<<"<"<<(fid[j]-start_face+1)<<","<<leid[j]<<">"<<" ";
    }
    std::cout<<std::endl;
    
  }
 
  std::cout<<"<V2HE_FID, V2HE_LEID>"<<std::endl;
  for (Range::iterator i = verts.begin(); i !=verts.end(); ++i){
    EntityHandle fid; int lid;
    error = mbImpl->tag_get_data(v2he_fid, &*i, 1, &fid);
    error = mbImpl->tag_get_data(v2he_leid, &*i, 1, &lid);
    if (fid == 0)
      std::cout<<"For vertex = "<<*i<<"::Incident halfedge "<<fid<<"  "<<lid<<std::endl;
    else 
    std::cout<<"For vertex = "<<*i<<"::Incident halfedge "<<(fid-start_face+1)<<"  "<<lid<<std::endl;
  }

  std::cout<<"Finished printing the AHF maps"<<std::endl;
  std::cout<<" Starting the incidence queries"<<std::endl;

  // Perform upward incidence and edge-connected neighborhood queries

  //Query 1: Given an implicit half-edge, get the incident faces.
  std::vector<EntityHandle> upin1, upin2;
  for (Range::iterator fid = faces.begin(); fid != faces.end(); ++fid){
    for (int leid = 0; leid <nepf; leid++){
      upin1.clear();
      error = ahf.get_upward_incidences_2d(*fid, leid, sibhes_fid, sibhes_leid, true, upin1);
      assert(upin1[0] == *fid);      

      upin2.clear();
      error = ahf.get_upward_incidences_2d(*fid, leid, sibhes_fid, sibhes_leid, false, upin2 );
      assert(upin1.size() == upin2.size()+1);
      for (int i =0; i < (int)upin2.size(); i++)
	assert(upin1[i+1]==upin2[i]);
    }
  }
  std::cout<<" Finished Q1: Find incident faces on given half-edge"<<std::endl;

  //Query 2: Given an implicit half-edge, get the incident half-edges.
  std::vector<EntityHandle> upin_fids1,upin_fids2; 
  std::vector<int> upin_leids1, upin_leids2;
  for(Range::iterator fid = faces.begin(); fid != faces.end(); ++fid){
    for (int leid = 0; leid < nepf; leid ++){
      upin_fids1.clear(); upin_leids1.clear();
      error = ahf.get_upward_incidences_2d(*fid, leid, sibhes_fid, sibhes_leid, true, upin_fids1, upin_leids1);
      assert((upin_fids1[0]==*fid)&&(upin_leids1[0]==leid));
      assert(upin_fids1.size()==upin_leids1.size());

      upin_fids2.clear(); upin_leids2.clear();
      error = ahf.get_upward_incidences_2d(*fid, leid, sibhes_fid, sibhes_leid, false, upin_fids2, upin_leids2);
      assert(upin_fids2.size()==upin_leids2.size());
      assert((upin_fids1.size()==upin_fids2.size()+1)&&(upin_leids1.size()==upin_leids2.size()+1));
      
      for (int i=0; i < (int)upin_fids2.size(); i++)
	assert((upin_fids1[i+1]==upin_fids2[i])&&(upin_leids1[i+1]==upin_leids2[i]));
    }
  }

  std::cout<<" Finished Q2: Find incident half-edges on given half-edge"<<std::endl;

  //Query 3: Given an explicit edge, get the incident faces. 
  // First create an explicit edge in the mesh and then perform the query. 
  std::vector<EntityHandle> conn(nepf);
  error = mbImpl->get_connectivity(&*faces.end(), 1, conn);
  EntityHandle exp_edge;
  error = mbImpl->create_element(MBEDGE, &conn[0], 2, exp_edge);

  std::vector<EntityHandle> upin;
  bool *ftags = new bool[faces.size()];
  for (int i=0; i < (int)faces.size(); ++i){
    ftags[i] = false;
  }
  error = ahf.get_upward_incidences_2d(exp_edge, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, upin);
  for (int i=0; i<(int)faces.size(); ++i)
    assert(ftags[i] == false);
  
  std::cout<<" Finished Q3: Find incident faces on given explicit edge"<<std::endl;

  // Query 4: Given an explicit edge, get the incident half-edges.
  std::vector<EntityHandle> fids_1ring;
  std::vector<int> leids_1ring;
  //  fids_1ring.clear(); leids_1ring.clear();
  error = ahf.get_upward_incidences_2d(exp_edge, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, fids_1ring, leids_1ring);
  
  std::cout<<" Finished Q4: Find incident half-edges on given explicit edge"<<std::endl;

  // Query 5: Given a face, get the edge-connected neighbor faces
  std::vector<EntityHandle> ngb;
  for (Range::iterator f = faces.begin(); f != faces.end(); ++f){
    ngb.clear();
    error = ahf.get_neighbor_adjacencies_2d(*f, sibhes_fid, sibhes_leid, true, ngb);
  }

  std::cout<<" Finished Q5: Find neighbor faces of given face"<<std::endl;

  delete [] sdefval;
  delete [] sval;
  delete [] ftags;
}

