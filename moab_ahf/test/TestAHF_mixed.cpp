/*This function tests the AHF datastructures on CST meshes*/
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "../mbahf/ArrayHalfFacetMDS.hpp"
#include <sys/time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core moab;
  Interface* mbImpl = &moab;

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  /*Create ranges for handles of explicit elements of the mixed mesh*/
  Range verts, edges, faces, cells;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 1, edges);
  error = mbImpl->get_entities_by_dimension( 0, 2, faces); 
  error = mbImpl->get_entities_by_dimension( 0, 3, cells);
  
  int nverts = verts.size(); 
  int nedges = edges.size();
  int nfaces = faces.size();
  int ncells = cells.size();

  std::cout<<"nverts = "<<nverts<<", nedges = "<<nedges<<", nfaces = "<<nfaces<<", ncells = "<<ncells<<std::endl;

  ArrayHalfFacetMDS ahf(mbImpl);
  int nepf = ahf.local_maps_2d(*faces.begin()); 
  int index = ahf.get_index_from_type(*cells.begin());
  int nfpc = ahf.lConnMap3D[index].num_faces_in_cell;
  
  // Create dense tags over each dimensions represented explicitly in the input mesh.
  Tag sibhvs_eid, sibhes_fid, sibhfs_cid;
  Tag sibhvs_lvid, sibhes_leid, sibhfs_lfid;
  Tag v2hv_eid, v2he_fid, v2hf_cid;
  Tag v2hv_lvid, v2he_leid, v2hf_lfid;

  EntityHandle sib_eid[2] = {0,0}, hv_eid = 0;
  int sib_lvid[2] = {0,0}, hv_lvid = 0;

  EntityHandle * sib_fid = new EntityHandle[nepf];
  int * sib_leid = new int[nepf]; 
  EntityHandle he_fid = 0; 
  int he_leid = 0;
  for (int i = 0; i < nepf; i++){
    sib_fid[i] = 0;
    sib_leid[i] = 0;
  }

  EntityHandle * sib_cid = new EntityHandle[nfpc];
  int * sib_lfid = new int[nfpc];
  EntityHandle hf_cid = 0;
  int hf_lfid = 0;
  for (int i = 0; i < nfpc; i++){
    sib_cid[i] = 0;
    sib_lfid[i] = 0;
  }

  error = mbImpl->tag_get_handle("SIBHVS_EID", 2, MB_TYPE_HANDLE, sibhvs_eid, MB_TAG_DENSE | MB_TAG_CREAT, sib_eid);
  error = mbImpl->tag_get_handle("SIBHVS_LVID", 2, MB_TYPE_INTEGER, sibhvs_lvid, MB_TAG_DENSE | MB_TAG_CREAT, sib_lvid);
  error = mbImpl->tag_get_handle("SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sib_fid);
  error = mbImpl->tag_get_handle("SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sib_leid);
  error = mbImpl->tag_get_handle("SIBHFS_CID", nfpc, MB_TYPE_HANDLE, sibhfs_cid, MB_TAG_DENSE | MB_TAG_CREAT, sib_cid);
  error = mbImpl->tag_get_handle("SIBHFS_LFID", nfpc, MB_TYPE_INTEGER, sibhfs_lfid, MB_TAG_DENSE | MB_TAG_CREAT, sib_lfid);

  error = mbImpl->tag_get_handle("V2HV_EID", 1, MB_TYPE_HANDLE, v2hv_eid, MB_TAG_DENSE | MB_TAG_CREAT, &hv_eid);  
  error = mbImpl->tag_get_handle("V2HV_LVID", 1, MB_TYPE_INTEGER, v2hv_lvid, MB_TAG_DENSE | MB_TAG_CREAT, &hv_lvid);
  error = mbImpl->tag_get_handle("V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &he_fid);  
  error = mbImpl->tag_get_handle("V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &he_leid);
  error = mbImpl->tag_get_handle("V2HF_CID", 1, MB_TYPE_HANDLE, v2hf_cid, MB_TAG_DENSE | MB_TAG_CREAT, &hf_cid);  
  error = mbImpl->tag_get_handle("V2HF_LFID", 1, MB_TYPE_INTEGER, v2hf_lfid, MB_TAG_DENSE | MB_TAG_CREAT, &hf_lfid);

  std::cout << "Created the tags for 1D (half-verts), 2D (half-edges) and 3D (half-faces)" << std::endl;


  // Create the maps for each dimension
  double time_start, time_elapsed, time_avg;
  time_start = wtime();

  //Construct half-verts
  error = ahf.determine_sibling_halfverts(verts, edges, sibhvs_eid, sibhvs_lvid);
  error = ahf.determine_incident_halfverts(edges, v2hv_eid, v2hv_lvid);

  //Construct half-edges
  error = ahf.determine_sibling_halfedges(faces, sibhes_fid, sibhes_leid);
  error = ahf.determine_incident_halfedges(faces, sibhes_fid, v2he_fid, v2he_leid);

  //Construct half-faces
  error = ahf.determine_sibling_halffaces(cells, sibhfs_cid, sibhfs_lfid);
  error = ahf.determine_incident_halffaces(cells, sibhfs_cid, v2hf_cid, v2hf_lfid);

  time_elapsed = wtime() - time_start;
  std::cout << "Time taken to construct the MDS = "<<time_elapsed<<" secs"<<std::endl;


  //Storage Costs
  unsigned long TotS, TAS, ES, AES, AS, AAS, TS, ATS;
  TotS = TAS = ES = AES = AS = AAS = TS = ATS = 0;
  mbImpl->estimated_memory_use(NULL, 0, &TotS, &TAS, &ES, &AES, &AS, &AAS, NULL, 0, &TS, &ATS);
  std::cout<<std::endl;
  std::cout<<"Total storage = "<<TotS<<std::endl;
  std::cout<<"Total amortized storage = "<<TAS<<std::endl;
  std::cout<<"Entity storage = "<<ES<<std::endl;
  std::cout<<"Amortized entity storage = "<<AES<<std::endl;
  std::cout<<"Adjacency storage = "<<AS<<std::endl;
  std::cout<<"Amortized adjacency storage = "<<AAS<<std::endl;
  std::cout<<"Tag storage = "<<TS<<std::endl;
  std::cout<<"Amortized tag storage = "<<ATS<<std::endl;
  std::cout<<std::endl;

  //Perform queries
  std::vector<EntityHandle> adj_entities, ngb_entities;

  //1D Queries //
  //IQ1: For every vertex, obtain incident edges
  time_start = wtime();
  for (Range::iterator i = verts.begin(); i != verts.end(); ++i) {    
    adj_entities.clear();
    error = ahf.get_upward_incidences_1d( *i, sibhvs_eid, sibhvs_lvid, v2hv_eid, v2hv_lvid, adj_entities);        
    
    if (*i == *verts.begin()+99){
      std::cout<<"IQ1 for vertex ="<<*i<<" "<<adj_entities.size()<<std::endl;
      for (int j=0;j<(int)adj_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<adj_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)verts.size();
  std::cout<<"Average time taken to compute incident edges =  "<< time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  //NQ1:  For every edge, obtain neighbor edges  
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {    
    ngb_entities.clear();
    error = ahf.get_neighbor_adjacencies_1d( *i, sibhvs_eid, sibhvs_lvid, ngb_entities );   

    if (*i == *edges.begin()+99){
      std::cout<<"NQ1 for edge = "<<*i<<" "<<ngb_entities.size()<<std::endl;
      for (int j=0;j<(int)ngb_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<ngb_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute neighbor edges = "<<time_avg<<" secs" << std::endl;  
  std::cout<<std::endl;


  // 2D Queries
  //IQ2: For every edge, obtain incident faces
  bool * ftags = new bool[nfaces];
  for (int i=0; i < nfaces; ++i){
    ftags[i] = false;
  }
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {   
    adj_entities.clear();
    error = ahf.get_upward_incidences_2d( *i, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, adj_entities );           
    if (*i == *edges.begin()+99){
      std::cout<<"IQ2 for edge "<<*i<<" "<<adj_entities.size()<<std::endl;
      for (int j=0;j<(int)adj_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<adj_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute incident faces =  "<<time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  //NQ2: For every face, obtain neighbor faces 
  time_start = wtime();
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {   
    ngb_entities.clear();
    error = ahf.get_neighbor_adjacencies_2d( *i, sibhes_fid, sibhes_leid, false, ngb_entities);    

    if (*i == *faces.begin()+99){
      std::cout<<"NQ2 for faces = "<<*i<<" "<<ngb_entities.size()<<std::endl;
      for (int j=0;j<(int)ngb_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<ngb_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)faces.size();
  std::cout<<"Average time taken to compute neighbor faces = "<< time_avg<<" secs" <<std::endl; 
  std::cout<<std::endl;

  // 3D Queries
  // IQ 3: For every face, obtain incident cells 
  bool * ctags = new bool[ncells];
  for (int i = 0; i<ncells; ++i){
    ctags[i] = false;
  }
  time_start = wtime();
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {
    adj_entities.clear();
    error = ahf.get_upward_incidences_3d( *i, cells, sibhfs_cid, v2hf_cid, ctags, adj_entities);
  
    if (*i == *faces.begin()+99){
      std::cout<<"IQ3 for faces = "<<*i<<" "<<adj_entities.size()<<std::endl;
      for (int j=0;j<(int)adj_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<adj_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)faces.size();
  std::cout<<"Average time taken to compute incident cells  =  "<<time_avg <<" secs"<<std::endl;
  std::cout<<std::endl;

  //NQ3: For every cell, obtain neighbor cells
  time_start = wtime();
  for (Range::iterator i = cells.begin(); i != cells.end(); ++i) {   
    ngb_entities.clear();
    error = ahf.get_neighbor_adjacencies_3d( *i, sibhfs_cid, ngb_entities);
  
    if (*i == *cells.begin()+99){
      std::cout<<"NQ3 for cells = "<<*i<<" "<<ngb_entities.size()<<std::endl;
      for (int j=0;j<(int)ngb_entities.size();++j){
	std::cout<<"Entity "<<j<<" = "<<ngb_entities[j]<<std::endl;
      }
    }
  }
  time_avg = (wtime()-time_start)/(double)cells.size();
  std::cout<<"Average time taken to compute neighbor cells =  "<< time_avg <<" secs" << std::endl;  
  std::cout<<std::endl;

  delete [] sib_fid;
  delete [] sib_leid;
  delete [] sib_cid;
  delete [] sib_lfid;
  delete [] ftags;
  delete [] ctags;

  return 0;
}

