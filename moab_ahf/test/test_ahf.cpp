/*This function tests the AHF datastructures on CST meshes*/
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "../mbahf/HalfFacetRep.hpp"
#include <sys/time.h>

using namespace moab;

double wtime() {
  double y = -1;
  struct timeval cur_time;  
  gettimeofday(&cur_time, NULL);  
  y = (double)(cur_time.tv_sec) + (double)(cur_time.tv_usec)*1.e-6;  
  return (y);
}

int main(int argc, char **argv)
{
  // Read the input mesh
  if (2!=argc){
    std::cout << "Incorrect number of inputs" << std::endl;
    return 0;
  }
  char* input_filename = argv[1];
  ErrorCode error;
  Core moab;
  Interface* mbImpl = &moab;

  error = mbImpl->load_file( input_filename );
  if (MB_SUCCESS != error) {
    std::cerr << input_filename <<": failed to load file." << std::endl;
    return error;
  }

  /*Create ranges for handles of explicit elements of the mixed mesh*/
  Range verts, edges, faces, cells;
  error = mbImpl->get_entities_by_dimension( 0, 0, verts);
  error = mbImpl->get_entities_by_dimension( 0, 1, edges);
  error = mbImpl->get_entities_by_dimension( 0, 2, faces); 
  error = mbImpl->get_entities_by_dimension( 0, 3, cells);
  
  int nverts = verts.size(); 
  int nedges = edges.size();
  int nfaces = faces.size();
  int ncells = cells.size();

  std::cout<<"nverts = "<<nverts<<", nedges = "<<nedges<<", nfaces = "<<nfaces<<", ncells = "<<ncells<<std::endl;

  for (Range::iterator c = cells.begin(); c != cells.end(); ++c)
    {
      std::vector<EntityHandle> conn(6);
      error = mbImpl->get_connectivity(&*c, 1, conn);
      if (MB_SUCCESS != error) return error;
      for (int k = 0; k< 6; k++)
        std::cout<<"conn["<<k<<"] = "<<conn[k]<<", ";
      std::cout<<std::endl;
    }

  //Storage Costs before calling ahf functionalities
  unsigned long sTotS, sTAS, sES, sAES, sAS, sAAS, sTS, sATS;
  sTotS = sTAS = sES = sAES = sAS = sAAS = sTS = sATS = 0;
  mbImpl->estimated_memory_use(NULL, 0, &sTotS, &sTAS, &sES, &sAES, &sAS, &sAAS, NULL, 0, &sTS, &sATS);
  std::cout<<std::endl;
  std::cout<<"Total storage = "<<sTotS<<std::endl;
  std::cout<<"Total amortized storage = "<<sTAS<<std::endl;
  std::cout<<"Entity storage = "<<sES<<std::endl;
  std::cout<<"Amortized entity storage = "<<sAES<<std::endl;
  std::cout<<"Adjacency storage = "<<sAS<<std::endl;
  std::cout<<"Amortized adjacency storage = "<<sAAS<<std::endl;
  std::cout<<"Tag storage = "<<sTS<<std::endl;
  std::cout<<"Amortized tag storage = "<<sATS<<std::endl;
  std::cout<<std::endl;


  double time_start, time_elapsed, time_avg;

  // Create an ahf instance
  HalfFacetRep ahf(mbImpl);

  // Call the initialize function which creates the maps for each dimension
  time_start = wtime();

  ahf.initialize();

  time_elapsed = wtime() - time_start;
  std::cout << "Time taken to construct the MDS = "<<time_elapsed<<" secs"<<std::endl;

  //Storage Costs after calling ahf initialize
  unsigned long TotS, TAS, ES, AES, AS, AAS, TS, ATS;
  TotS = TAS = ES = AES = AS = AAS = TS = ATS = 0;
  mbImpl->estimated_memory_use(NULL, 0, &TotS, &TAS, &ES, &AES, &AS, &AAS, NULL, 0, &TS, &ATS);
  std::cout<<std::endl;
  std::cout<<"Total storage = "<<TotS<<std::endl;
  std::cout<<"Total amortized storage = "<<TAS<<std::endl;
  std::cout<<"Entity storage = "<<ES<<std::endl;
  std::cout<<"Amortized entity storage = "<<AES<<std::endl;
  std::cout<<"Adjacency storage = "<<AS<<std::endl;
  std::cout<<"Amortized adjacency storage = "<<AAS<<std::endl;
  std::cout<<"Tag storage = "<<TS<<std::endl;
  std::cout<<"Amortized tag storage = "<<ATS<<std::endl;
  std::cout<<std::endl;

  //Perform queries
  std::vector<EntityHandle> adjents;
  std::vector<int> lids;

  //1D Queries //
  //IQ1: For every vertex, obtain incident edges
  std::cout<<"1D QUERIES"<<std::endl;
  time_start = wtime();
  for (Range::iterator i = verts.begin(); i != verts.end(); ++i) {    
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 1, adjents);

    std::cout<<"For vertex "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = <"<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)verts.size();
  std::cout<<"Average time taken to compute incident edges to a vertex =  "<< time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  //NQ1:  For every edge, obtain neighbor edges  
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {    
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);

    std::cout<<"For edge "<<*i<<": "<<std::endl;
    std::cout<<"Number of neighbor ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = <"<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute neighbor edges of an edge = "<<time_avg<<" secs" << std::endl;
  std::cout<<std::endl;


  // 2D Queries
  //IQ21: For every edge, obtain incident faces
  std::cout<<"2D QUERIES"<<std::endl;
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {   
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 2, adjents);

    std::cout<<"For edge "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = "<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute incident faces on an edge =  "<<time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  //IQ22: For every edge, obtain incident half-edges
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {
    adjents.clear();
    lids.clear();
    error = ahf.get_upward_incidences( *i, 2, adjents, true, &lids);

    std::cout<<"For edge "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = <"<<adjents[k]<<", "<<lids[k]<<">, "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute incident half-edges on an edge =  "<<time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  //NQ2: For every face, obtain neighbor faces 
  time_start = wtime();
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {   
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);

    std::cout<<"For face "<<*i<<": "<<std::endl;
    std::cout<<"Number of neighbor ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = "<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)faces.size();
  std::cout<<"Average time taken to compute neighbor faces of a face = "<< time_avg<<" secs" <<std::endl;
  std::cout<<std::endl;

  // 3D Queries
  // IQ 31: For every edge, obtain incident cells
  std::cout<<"3D QUERIES"<<std::endl;
  time_start = wtime();
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents);

    std::cout<<"For edge "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = "<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute incident cells on an edge  =  "<<time_avg <<" secs"<<std::endl;
  std::cout<<std::endl;

  //IQ32: For every edge, obtain incident cells along with the local ids of the corresponding edges
  for (Range::iterator i = edges.begin(); i != edges.end(); ++i) {
    adjents.clear();
    lids.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents, true, &lids);

    std::cout<<"For edge "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = <"<<adjents[k]<<", "<<lids[k]<<">, "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)edges.size();
  std::cout<<"Average time taken to compute incident implicit edges of cells on an edge  =  "<<time_avg <<" secs"<<std::endl;
  std::cout<<std::endl;

  //IQ33: For every face, obtain incident cells
  time_start = wtime();
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {
    adjents.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents);

    std::cout<<"For face "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = "<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)faces.size();
  std::cout<<"Average time taken to compute incident cells on a face  =  "<<time_avg <<" secs"<<std::endl;
  std::cout<<std::endl;

  // IQ34: For every face, obtain incident half-faces
  time_start = wtime();
  for (Range::iterator i = faces.begin(); i != faces.end(); ++i) {
    adjents.clear();
    lids.clear();
    error = ahf.get_upward_incidences( *i, 3, adjents, true, &lids);

    std::cout<<"For face "<<*i<<": "<<std::endl;
    std::cout<<"Number of incident ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = <"<<adjents[k]<<", "<<lids[k]<<">, "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)faces.size();
  std::cout<<"Average time taken to compute incident half-faces on a face =  "<<time_avg <<" secs"<<std::endl;
  std::cout<<std::endl;

  //NQ3: For every cell, obtain neighbor cells
  time_start = wtime();
  for (Range::iterator i = cells.begin(); i != cells.end(); ++i) {   
    adjents.clear();
    error = ahf.get_neighbor_adjacencies( *i, false, adjents);

    std::cout<<"For cell "<<*i<<": "<<std::endl;
    std::cout<<"Number of neighbor ents = "<<(int)adjents.size()<<std::endl;
    for (int k = 0; k<(int)adjents.size(); k++)
      std::cout<<"adjents["<<k<<"] = "<<adjents[k]<<", "<<std::endl;
    std::cout<<std::endl;
  }
  time_avg = (wtime()-time_start)/(double)cells.size();
  std::cout<<"Average time taken to compute neighbor cells of a cell =  "<< time_avg <<" secs" << std::endl;
  std::cout<<std::endl;

  ahf.deinitialize();

  //Storage Costs after calling ahf deinitialize
  unsigned long eTotS, eTAS, eES, eAES, eAS, eAAS, eTS, eATS;
  eTotS = eTAS = eES = eAES = eAS = eAAS = eTS = eATS = 0;
  mbImpl->estimated_memory_use(NULL, 0, &eTotS, &eTAS, &eES, &eAES, &eAS, &eAAS, NULL, 0, &eTS, &eATS);
  std::cout<<std::endl;
  std::cout<<"Total storage = "<<eTotS<<std::endl;
  std::cout<<"Total amortized storage = "<<eTAS<<std::endl;
  std::cout<<"Entity storage = "<<eES<<std::endl;
  std::cout<<"Amortized entity storage = "<<eAES<<std::endl;
  std::cout<<"Adjacency storage = "<<eAS<<std::endl;
  std::cout<<"Amortized adjacency storage = "<<eAAS<<std::endl;
  std::cout<<"Tag storage = "<<eTS<<std::endl;
  std::cout<<"Amortized tag storage = "<<eATS<<std::endl;
  std::cout<<std::endl;

  return 0;
}

