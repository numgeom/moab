/**
 * MOAB, a Mesh-Oriented datABase, is a software component for creating,
 * storing and accessing finite element mesh data.
 * 
 * Copyright 2004 Sandia Corporation.  Under the terms of Contract
 * DE-AC04-94AL85000 with Sandia Coroporation, the U.S. Government
 * retains certain rights in this software.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 */


#ifndef MOAB_ARRAY_HALF_FACET_MDS_HPP
#define MOAB_ARRAY_HALF_FACET_MDS_HPP

#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "moab/CN.hpp"
#include <queue>

namespace moab {

/*! 
 *  \brief   ArrayHalfFacetMDS class contains functions to
 *  \        1. determine sibling half-facets and vertex->incident half-facet maps 
 *  \        2. perform incident (upward-incidence) and neighborhood (same-dimensional) adjacency queries.
 *  \ 
 *  \        Mesh types supported: 
 *  \        1D(edges), 2D(triangles, quads), 3D(tet, pyramid, prism, hex), Mixed dimensional. 
 */ 
  
  enum MTYPE{
   CURVE = 0, //Homogeneous curve mesh
   SURFACE, // Homogeneous surface mesh
   VOLUME, // Homogeneous volume mesh
   MIXED //Volume mesh with embedded surfaces and curves
  };
   
  class ArrayHalfFacetMDS{
    
  protected: 
    Interface * mb;
        
    /*Range _verts, _edges, _faces, _cells;
    Tag _sibhvs_eid, _sibhvs_lvid, _v2hv_eid, _v2hv_lvid;
    Tag _sibhes_fid, _sibhes_leid, _v2he_fid, _v2he_leid;
    Tag _sibhfs_cid, _sibhfs_lfid, _v2hf_cid, _v2hf_lfid;*/
    
  public:
    
    ArrayHalfFacetMDS(Interface *impl) : mb(impl) {}
    
    ~ArrayHalfFacetMDS() {}
    
    /*
      ErrorCode get_upward_adjacencies(EntityHandle ent, std::vector<EntityHandle> &adj_ents);
      ErrorCode get_neighbor_adjacencies(EntityHandle ent, std::vector<EntityHandle> &ngb_ents);
      
    */
    
    
    //ErrorCode initialize();
    
    //ErrorCode deinitialize();
    
    MTYPE get_mesh_type(int nverts, int nedges, int nfaces, int ncells);
    
    // 1D Maps and queries
    
    //! Given a range of vertices and edges, determine the map for sibling half-verts as a tag.
    /** Compute all sibling half-vertices for all half-vertices in the given curve. The sibling half-verts is 
	defined in terms of the containing entity and the local id w.r.t that entity. That is, the map consists 
	of two information: <eid, lvid>
	Parameters:
	verts, edges: Range of vertices and edges
	sibhvs_eid, sibhvs_lvid: Separate dense tags over all the edges.   
    */
    
    ErrorCode determine_sibling_halfverts(Range &verts, Range &edges, Tag sibhvs_eid, Tag sibhvs_lvid);
    
    //! Given a range of edges, determine the incident half-verts as a tag.
    /** Compute a map between a vertex and an incident half-vertex. This map is not always required, but is 
	essential for local neighborhood searching as it acts like an anchor to start the search. 
	Parameters:
	edges: Range of vertices and edges
	v2hv_eid, v2hv_lvid: Separate dense tags over all the edges.   
    */
    
    
    ErrorCode determine_incident_halfverts(Range &edges, Tag v2hv_eid, Tag v2hv_lvid);
    
    //! Given a vertex handle, find the list of entities incident on it. 
    /** Given a vertex handle, it starts by first finding an incident half-vert by using the incident 
	half-vert map, and then obtaining all the sibling half-verts of the corresponding half-vertex. 
	Parameters: 
	vid: The query vertex
	edges: Edges
	sibhvs_eid, sibhvs_lvid: Sibling half-verts
	v2hv_eid, v2hv_lvid: Incident half-verts
	adj_entites: Outputs the incident edge handles 
    */
    
    ErrorCode get_upward_incidences_1d(EntityHandle vid, Tag sibhvs_eid, Tag sibhvs_lvid, Tag v2hv_eid, Tag v2hv_lvid, std::vector< EntityHandle > &adj_entities);
    
    //! Given an explicit edge, find vertex-connected neighbor edges
    /** Given an explicit edge, it gathers all the incident half-verts of each vertex of the edge.
	Parameters:
	eid: The query edge
	edges: Edges
	sibhvs_eid, sibhvs_lvid: Sibling half-verts
	v2hv_eid, v2hv_lvid: Incident half-verts
	adj_entites: Outputs the neighbor edge handles
    */
    ErrorCode get_neighbor_adjacencies_1d(EntityHandle eid, Tag sibhvs_eid, Tag sibhvs_lvid, std::vector<EntityHandle> &adj_entities);
    
    
    // 2D Maps and queries    
    
    //! Contains the local information for 2D entities
    /** Given a face, find the face type specific information
	Parameters: 
	face: Used to gather info about the type of face for which local info is required
	nepf: Return variable, contains the number of vertices/edges for each face type. 
	next, prev: Local ids of next and previous edges wrt to the face
	Note: For 2D entities, the number of vertices and edges are same and each local edge is outgoing 
	corresponding to the local vertex, i.e,
	
        v2        v3 __e2__v2
        /\          |      |   
	e2 /  \ e1    e3|      |e1
	/____\        |______|
	v0  e0  v1     v0  e0  v1
    */
    
    
    int local_maps_2d(EntityHandle face);
    
    ErrorCode local_maps_2d(EntityHandle face, int *next, int *prev);
    
    //! Given a range of faces, determine the map for sibling half-edges as a tag.
    /** Compute all sibling half-edges for all half-edges in the given curve. 
	The sibling half-edges is defined in terms of the containing entity and the local id w.r.t that entity. 
	That is, the map consists of two information: <fid, leid>
	Parameters:
	faces: Range of faces
	sibhes_fid, sibhes_leid: Separate dense tags over all the faces.   
    */
    
    ErrorCode determine_sibling_halfedges(Range &faces, Tag sibhes_fid, Tag sibhes_leid);
    
    //! Given a range of faces, determine the incident half-edges as a tag.
    /** Compute a map between a vertex and an incident half-edge.
	This map is not always required, but is essential for local neighborhood searching as it acts 
	like an anchor to start the search. 
	Parameters:
	faces: Range of vertices and edges
	sibhes_fid, sibhes_leid: Sibling half-edges map
	v2he_fid, v2he_leid: Separate dense tags over all the faces.   
    */
    
    
    ErrorCode determine_incident_halfedges(Range &faces, Tag sibhes_fid, Tag v2he_fid, Tag v2he_leid);
    
    //! Given a half-edge as <fid,leid> , find the list of faces incident on it. 
    /** Given an half-edge, obtain all the incident half-edges via the sibhes map and store all the handles 
	of the faces of the half-edges. 
	Parameters: 
	<fid, leid> : The query half-edge 
	sibhes_fid, sibhes_leid: Sibling half-edges maps
	add_iface: Option to either include or not, the face of the query half-edge to the output list 
	adj_entites: Outputs the incident face handles incident on the query half-edge 
    */
    
    
    ErrorCode get_upward_incidences_2d(EntityHandle fid, int leid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &adj_entities);
    
    //! Given a half-edge as <fid,leid> , find the list of half-edges incident on it. 
    /** Given an half-edge, obtain all the incident half-edges via the sibhes map. 
	Parameters: 
	<fid, leid> : The query half-edge 
	sibhes_fid, sibhes_leid: Sibling half-edges map
	add_iface: Option to either include(add_iface = true) or not, the query half-edge to the output list 
	fids_1ring, leids_1ring: Outputs the incident half-edges on the query half-edge 
    */
    
    ErrorCode get_upward_incidences_2d(EntityHandle fid, int leid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &fids_1ring, std::vector<int> &leids_1ring);
    
    //! Given a half-edge as <he_fid,he_lid> , find the list of half-edges incident on it and add them 
    //  to an input queue. 
    /** Given an half-edge, obtain all the incident half-edges via the sibhes map and add them to a given 
	queue of half-edges, if they do not already exist in the queue. 
	Parameters: 
	<he_fid, he_lid> : The query half-edge 
	faces: Range of faces
	sibhes_fid, sibhes_leid: Sibling half-edges map
	queue_fid, queue_lid : An input queue containing half-edges
	count : The number of half-edges in the queue at input
	ftags : A boolean tag over all faces, true if a half-edge belonging to a face already exists in the queue    */
    
    ErrorCode get_upward_incidences_2d(EntityHandle he_fid, int he_lid, Range &faces, Tag sibhes_fid, Tag sibhes_leid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, bool *ftags);
    
    //! Given an explicit edge, find the list of faces incident on it. 
    /** Given an explicit edge eid, it first finds a matching half-edge corresponding to eid, and then 
	collects all the incident half-edges/faces via the sibhes map. 
	Parameters:
	eid: The query explicit edge
	faces: Range of faces
	sibhes_fid, sibhes_leid: Sibling half-edges map
	v2he_fid, v2he_leid: Incident half-edges map
	ftags: A boolean tag over all faces. Aids in performing a local search in the neighborhood of 
	the edge by assigning true values to all faces that have been searched. 
	adj_entities: Outputs the faces of the half-edges incident on the query edge eid. 
    */
    
    ErrorCode get_upward_incidences_2d(EntityHandle eid, Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, std::vector<EntityHandle> &adj_entities);
    
    //! Given an explicit edge, find the list of half-edges incident on it. 
    /** Given an explicit edge eid, it first finds a matching half-edge corresponding to eid, and then 
	collects all the incident half-edges via the sibhes map. 
	Parameters:
	eid: The query explicit edge
	faces: Range of faces
	sibhes_fid, sibhes_leid: Sibling half-edges map
	v2he_fid, v2he_leid: Incident half-edges map
	ftags: A boolean tag over all faces. Aids in performing a local search in the neighborhood of 
	the edge by assigning true values to all faces that have been searched. 
	fids_1ring, leids_1ring: Outputs the half-edges incident on the query edge eid. 
    */
    
    ErrorCode get_upward_incidences_2d(EntityHandle eid, Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, std::vector<EntityHandle> &fids_1ring, std::vector<int> &leids_1ring);
    
    //! Given an explicit edge, find a matching half-edge corresponding to it.
    /** Given an explicit edge eid, it first collects few half-edges belonging to one-ring neighborhood of 
	the starting vertex of the given edge, and then simultaneously searches and adds to the local list 
	of half-edges for searching, till it finds a matching half-edge.   
	Parameters: 
	eid: The query edge
	faces: Range of faces
	sibhes_fid, sibhes_leid:
	v2he_fid, v2he_leid:
	ftags:
	hefid, helid: The matching half-edge corresponding to the query edge. 
    */
    
    
    ErrorCode find_matching_halfedge( EntityHandle eid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, EntityHandle *hefid, int *helid);
    
    //! Gather half-edges to a queue of half-edges. 
    /** Given a vertex vid, and a half-edge <he_fid,he_lid>, add the other half-edge in the same face and
	add all incident half-edges to the queue via sibhes map. 
	Parameters: 
	vid, <he_fid, he_lid>: A vertex belonging to the half-edge and the half-edge
	faces: Range of faces
	sibhes_fid, sibhes_leid: 
	v2he_fid, v2he_leid: 
	ftags:
	queue_fid, queue_lid, count:
    */
    
    ErrorCode gather_halfedges( EntityHandle vid,  EntityHandle he_fid,  int he_lid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, bool *ftags, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces);
    
    //! Obtain another half-edge belonging to the same face as the input half-edge
    /** It uses the local maps to find another half-edge that is either incident or outgoing depending
	on vid and input half-edge 
	Parameters:
	he2_fid, he2_lid: Outputs another half-edge in the same he_fid.
    */
    
    ErrorCode another_halfedge( EntityHandle vid,  EntityHandle he_fid,  int he_lid, EntityHandle *he2_fid, int *he2_lid);
    
    //! Collect and compare to find a matching half-edge with the given edge connectivity.
    /** Given edge connectivity, compare to an input list of half-edges to find a matching half-edge 
	and add a list of half-edges belonging to the one-ring neighborhood to a queue till it finds a match.
	Parameters: 
	edg_vert: End vertices of an edge
	faces:
	sibhes_fid, sibhes_leid:
	v2he_fid, v2he_leid:
	ftags;
	queue_fid, queue_lid, count;
	he_fid,he_lid: Matching half-edge
    */
    
    ErrorCode collect_and_compare(std::vector<EntityHandle> &edg_vert,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, bool *ftags, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces, EntityHandle *he_fid, int *he_lid);
    
    //! Given an explicit face, find all edge-connected neighbor faces. 
    /** Collect all neighbor faces via the sibhes map
	Parameters:
	fid:
	faces:
	sibhes_fid, sibhes_leid:
	add_iface: true, to include the input face to the output list
	adj_entites: The list of neighbor faces
    */
    
    ErrorCode get_neighbor_adjacencies_2d( EntityHandle fid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &adj_entities);
    
    int find_total_edges_2d(Range &faces, Tag sibhes_fid, Tag sibhes_leid);
    
    // 3D Maps and queries   
    
    //! The local maps for 3D entities. 
    /** Types of 3D entities supported: tets, pyramid, prism, hex
	Determines the type from input "cell"

	_3d_numels:
	nvpc: Number of vertices per cell
	nepc: Number of edges per cell
	nfpc: Number of faces per cell
	
	_3d_numels: 
	nvmax: The maximum number of vertices of all half-faces of the cell
	
	_3d_hf2v: Map half-face to vertices
	hf2v_num: Array storing number of vertices per half-face
	hf2v_map: Local ids of vertices of each half-face, stored in an array
	hf2v_idx: Starting index for each half-face to access vertices
	
	_3d_v2hf: Map vertex to half-face
	v2hf_num: Array storing number of incident half-faces per vertex
	v2hf_map: Local ids of incident half-faces, stored in an array
	v2hf_idx: Starting index for each vertex to access incident half-faces
	
	_3d_edges: Maps for edges
	e2v: Local edge to local vertices
	e2hf: Local edge to incident half-faces
	f2leid: Local edges for each half-faces
	
	_3d_lookup_leid: 
	nvpc: Is an input #vertices per cell
	lookup_leid: Map between local vertex v0 to local vertex v1 storing the local edge id e = <v0,v1>
    */
    
    enum {
      MAX_VERTICES = 8,
      MAX_EDGES = 12,
      MAX_FACES = 6,
      MAX_VERTS_HF = 4,
      MAX_INCIDENT_HF = 4
    };
    
    struct LocalMaps3D{
      short int num_verts_in_cell; // Number of vertices per cell
      short int num_edges_in_cell; // Number of edges per cell
      short int num_faces_in_cell; // Number of faces per cell
      
      int hf2v_num[MAX_FACES]; // 
      int hf2v[MAX_FACES][MAX_VERTS_HF]; 
      
      int v2hf_num[MAX_VERTICES];
      int v2hf[MAX_VERTICES][MAX_INCIDENT_HF];
      
      int e2v[MAX_EDGES][2];
      int e2hf[MAX_EDGES][2];
      int f2leid[MAX_FACES][MAX_VERTS_HF];
      int lookup_leids[MAX_VERTICES][MAX_VERTICES];
    };
    
    static  LocalMaps3D lConnMap3D[4];
    
    int get_index_from_type(EntityHandle cid); 
    
    //! Given a range of cells, determine the map for sibling half-faces as a tag.
    /** Compute all sibling half-faces for all half-faces in the given curve. 
	The sibling half-faces is defined in terms of the containing entity and the local id w.r.t that entity. 
	That is, the map consists of two information: <cid, lfid>
	Parameters:
	cells: Range of cells
	sibhfs_cid, sibhfs_lfid: Separate dense tags over all the cells.   
    */
    
    ErrorCode determine_sibling_halffaces( Range &cells, Tag sibhfs_cid, Tag sibhfs_lfid);
    
    //! Given a range of cells, determine the incident half-faces as a tag.
    /** Compute a map between a vertex and an incident half-face.
	This map is not always required, but is essential for local neighborhood searching as it
	acts like an anchor to start the search. 
	Parameters:
	cells: Range of cells
	sibhfs_cid, sibhfs_lfid: Sibling half-faces map
	v2he_cid, v2he_lfid: Separate dense tags over all the cells.   
    */
    
    ErrorCode determine_incident_halffaces( Range &cells, Tag sibhfs_cid, Tag v2hf_cid, Tag v2hf_lfid);
    
    //! Given a range of cells, tag border vertices
    /** Tag border vertices by using the sibhf_cid map. All vertices on half-faces with no sibling
	half-faces are considered as border vertices. 
	Parameters:
	cells:
	sibhfs_cid:
	isborder: Dense tag over all vertices. 
    */
    
    ErrorCode determine_border_vertices_tet( Range &cells, Tag sibhfs_cid, Tag isborder);
    
    //! Given an explicit edge eid, find the list of cells and corresponding local edges incident on it. 
    /** Given an explicit edge eid, obtain all the incident cells starting the search using v2hf map and
	traversing the local neighborhood via the sibhfs map. Note that we find not only the incident cells,
	but also the local edge id corresponding to the given edge in the incident cell. This information 
	is required by edge-based operations, especially adaptivity. 
	Parameters: 
	eid : The query explicit edge 
	cells: 
	sibhfs_cid, sibhfs_lfid: 
	v2hf_cid, v2hf_lfid:
	ctags: boolean tag over all cells to track all visited cells
	cells_1ring, leids_1ring: List of cells and local edge ids incident on the query edge eid. 
    */
    
    ErrorCode get_upward_incidences_3d( EntityHandle eid,  Range &cells, Tag sibhfs_cid, Tag sibhfs_lfid, Tag v2hf_cid, bool *ctags, std::vector<EntityHandle> &cells_1ring, std::vector<int> &leids_1ring);
    
    //! Given an implicit edge in a cell by <cid, leid>, find the list of cells and corresponding local
    //  edges incident on it. 
    /** Given an implicit edge <cid,leid>, obtain all the incident cells along with corresponding local edges.
	Note that we find not only the incident cells, but also the local edge id corresponding to the given 
	edge in the incident cell. This information is required by edge-based operations, especially adaptivity. 
	Parameters: 
	cid,leid : The query implicit edge 
	cells: 
	sibhfs_cid, sibhfs_lfid: 
	v2hf_cid, v2hf_lfid:
	cells_1ring, leids_1ring: List of cells and local edge ids incident on the query edge eid. 
    */
    
    ErrorCode get_upward_incidences_3d( EntityHandle cid,  int leid, Tag sibhfs_cid, Tag sibhfs_lfid, std::vector<EntityHandle> &cids_1ring, std::vector<int> &leids_1ring);
    
    //! Given an explicit face fid, find the list of cells incident on it. 
    /** Given an explicit edge fid, it first finds a matching half-face in the cell corresponding to
	the input face, and then collects all the incident cells via sibhfs map. 
	Parameters: 
	fid : The explicit face
	faces: Range of faces 
	cells: 
	sibhfs_cid, sibhfs_lfid: 
	v2hf_cid, v2hf_lfid:
	ctags: boolean tag over all cells to track all visited cells
	adj_entities: List of incident cells. 
    */
    
    ErrorCode get_upward_incidences_3d( EntityHandle fid,  Range &cells, Tag sibhfs_cid, Tag v2hf_cid,  bool *ctags, std::vector<EntityHandle> &adj_entities);
    
    //! Given a half-face <cid,lfid>, find the list of cells incident on it. 
    /** Given an explicit edge fid, it first finds a matching half-face in the cell corresponding to
	the input face, and then collects all the incident cells via sibhfs map. 
	Parameters: 
	cid,lfid : The implicit half-face
	nfpc: Number of faces per cell
	sibhfs_cid, sibhfs_lfid: 
	adj_entities: List of incident cells. 
    */
    
    ErrorCode get_upward_incidences_3d( EntityHandle cid,  int lfid, int nfpc, Tag sibhfs_cid,  std::vector<EntityHandle> &adj_entities);
    
    //! Given an explicit edge eid, find a matching local edge in an incident cell.
    /** Find a local edge with the same connectivity as the input explicit edge, belonging to an incident cell. 
	Parameters:
	eid:
	cells:
	sibhfs_cid, sibhfs_lfid:
	v2hf_cid, v2hf_lfid:
	ctags:
	cid,leid: An incident cell with local edge id matching the connectivity of the input edge.
    */
    
    ErrorCode find_matching_implicit_edge_in_cell( EntityHandle eid,  Range &cells, Tag sibhfs_cid, Tag v2hf_cid, bool *ctags, EntityHandle *cid, int *leid);
    
    //! Given a cell, find all face-connected neighbor cells. 
    /** Given a cell cid, obtain all the neighbor cells that are connected via the half-faces of the 
	input cell. Uses the sibhfs map to gather all the sibling half-faces. 
	Parameters:
	cid: 
	sibhfs_cid, sibhfs_lfid:
	adj_entities: List of neighbor cells. 
    */
    
    ErrorCode get_neighbor_adjacencies_3d( EntityHandle cid, Tag sibhfs_cid, std::vector<EntityHandle> &adj_entities);
    
    ErrorCode get_neighbor_adjacencies_3d( EntityHandle cid, Tag sibhfs_cid, Tag sibhfs_lfid, std::vector<EntityHandle> &ngb_cids, std::vector<int> &ngb_lfids);
  };

} // namespace moab 

#endif

