/**
 * MOAB, a Mesh-Oriented datABase, is a software component for creating,
 * storing and accessing finite element mesh data.
 * 
 * Copyright 2004 Sandia Corporation.  Under the terms of Contract
 * DE-AC04-94AL85000 with Sandia Coroporation, the U.S. Government
 * retains certain rights in this software.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 */


#ifndef MOAB_HALF_FACET_REP_HPP
#define MOAB_HALF_FACET_REP_HPP

#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "moab/CN.hpp"
#include <queue>

namespace moab {

/*! 
 *  \brief   HalfFacetRep class contains various functionalities of the Array-Based Half-Facet(AHF) Mesh data structure.
 *  \        The core idea of the AHF is the construction of two maps storing the following
 *  \        1. Map every half-facet in the mesh to its sibling half-facets,
 *  \        2. Map vertex to an incident half-facet
 *  \        Using these two maps, an entire range of adjacency queries can be performed. Specifically, this class contains
 *  \        functions to perform
 *  \        1. upward-incidence queries: Ex. edge -> faces, edge -> cells, etc
 *  \        2. neighborhood (same-dimensional) adjacency (also known as bridge adjacencies) queries: Ex. face -> faces, cell -> cells, etc.
 *  \ 
 *  \        Mesh types supported: 
 *  \        1D(edges), 2D(triangles, quads), 3D(tet, pyramid, prism, hex), Mixed dimensional meshes
 *  \        NOT SUPPORTED: Meshes with mixed entity types. Ex. a volume mesh with both tets and prisms.
 *  \
 */ 
  
  enum MESHTYPE{
   CURVE = 0, //Homogeneous curve mesh
   SURFACE, // Homogeneous surface mesh
   SURFACE_MIXED, // Mixed surface with embedded curves
   VOLUME, // Homogeneous volume mesh
   VOLUME_MIXED_1, // Volume mesh with embedded curves
   VOLUME_MIXED_2, // Volume mesh with embedded surface
   VOLUME_MIXED //Volume mesh with embedded surfaces and curves
  };

   
  class HalfFacetRep{
       
  public:
    
    HalfFacetRep(Interface *impl) : mb(impl) {}
    
    ~HalfFacetRep() {}

    // User interface functions

    ErrorCode initialize();

    ErrorCode deinitialize();

    ErrorCode print_tags();
    
    ErrorCode get_upward_incidences(EntityHandle ent, int out_dim, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * lids = NULL );

    ErrorCode get_neighbor_adjacencies(EntityHandle ent, bool add_inent, std::vector<EntityHandle> &adjents);

    // 1D Maps and queries
    
    ErrorCode determine_sibling_halfverts(Range &verts, Range &edges);

    ErrorCode determine_incident_halfverts(Range &edges);

    ErrorCode get_upward_incidences_1d(EntityHandle vid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * lvids = NULL);
    
    ErrorCode get_neighbor_adjacencies_1d(EntityHandle eid, bool add_inent, std::vector<EntityHandle> &adjents);
    
    
    // 2D Maps and queries    
    
    ErrorCode determine_sibling_halfedges(Range &faces);

    ErrorCode determine_incident_halfedges(Range &faces);

    ErrorCode get_upward_incidences_2d(EntityHandle eid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * leids = NULL);

    ErrorCode get_upward_incidences_2d(EntityHandle fid, int leid, bool add_inent, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * leids = NULL);
       
    ErrorCode get_neighbor_adjacencies_2d( EntityHandle fid,  bool add_inent, std::vector<EntityHandle> &adjents);
    
    int find_total_edges_2d(Range &faces);
    
    // 3D Maps and queries   

    ErrorCode determine_sibling_halffaces( Range &cells);
    
    ErrorCode determine_incident_halffaces( Range &cells);
    
    ErrorCode determine_border_vertices( Range &cells, Tag isborder);

    ErrorCode get_upward_incidences_edg_3d(EntityHandle eid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * leids = NULL);

    ErrorCode get_upward_incidences_edg_3d(EntityHandle cid, int leid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * leids = NULL);

    ErrorCode get_upward_incidences_face_3d(EntityHandle fid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int>  * lfids = NULL);

    ErrorCode get_upward_incidences_face_3d(EntityHandle cid, int lfid, std::vector<EntityHandle> &adjents, bool local_id = false, std::vector<int> * lfids = NULL);
    
    ErrorCode get_neighbor_adjacencies_3d( EntityHandle cid,  bool add_inent, std::vector<EntityHandle> &adjents);
    

  protected:

    Interface * mb;

    enum {
      MAX_VERTICES = 8,
      MAX_EDGES = 12,
      MAX_FACES = 6,
      MAX_VERTS_HF = 4,
      MAX_INCIDENT_HF = 4
    };

    Range _verts, _edges, _faces, _cells;
    Tag sibhvs_eid, sibhvs_lvid, v2hv_eid, v2hv_lvid;
    Tag sibhes_fid, sibhes_leid, v2he_fid, v2he_leid;
    Tag sibhfs_cid, sibhfs_lfid, v2hf_cid, v2hf_lfid;
    bool *ftags, *ctags;


    MESHTYPE get_mesh_type(int nverts, int nedges, int nfaces, int ncells);

    ErrorCode init_curve();
    ErrorCode init_surface();
    ErrorCode init_volume();

    ErrorCode deinit_curve();
    ErrorCode deinit_surface();
    ErrorCode deinit_volume();

    int local_maps_2d(EntityHandle face);

    ErrorCode local_maps_2d(EntityHandle face, int *next, int *prev);

    ErrorCode get_upward_incidences_2d(EntityHandle he_fid, int he_lid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid);

    bool find_matching_halfedge( EntityHandle eid, EntityHandle *hefid, int *helid);

    ErrorCode gather_halfedges( EntityHandle vid,  EntityHandle he_fid,  int he_lid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces);


    ErrorCode another_halfedge( EntityHandle vid,  EntityHandle he_fid,  int he_lid, EntityHandle *he2_fid, int *he2_lid);

    bool collect_and_compare(std::vector<EntityHandle> &edg_vert, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces, EntityHandle *he_fid, int *he_lid);



    //! The local maps for 3D entities.
    /** Types of 3D entities supported: tets, pyramid, prism, hex
        Determines the type from input "cell"

	_3d_numels:
	nvpc: Number of vertices per cell
	nepc: Number of edges per cell
	nfpc: Number of faces per cell

	_3d_numels:
	nvmax: The maximum number of vertices of all half-faces of the cell

	_3d_hf2v: Map half-face to vertices
	hf2v_num: Array storing number of vertices per half-face
	hf2v_map: Local ids of vertices of each half-face, stored in an array
	hf2v_idx: Starting index for each half-face to access vertices

	_3d_v2hf: Map vertex to half-face
	v2hf_num: Array storing number of incident half-faces per vertex
	v2hf_map: Local ids of incident half-faces, stored in an array
	v2hf_idx: Starting index for each vertex to access incident half-faces

	_3d_edges: Maps for edges
	e2v: Local edge to local vertices
	e2hf: Local edge to incident half-faces
	f2leid: Local edges for each half-faces

	_3d_lookup_leid:
	nvpc: Is an input #vertices per cell
	lookup_leid: Map between local vertex v0 to local vertex v1 storing the local edge id e = <v0,v1>
    */


    struct LocalMaps3D{
      short int num_verts_in_cell; // Number of vertices per cell
      short int num_edges_in_cell; // Number of edges per cell
      short int num_faces_in_cell; // Number of faces per cell

      int hf2v_num[MAX_FACES]; //
      int hf2v[MAX_FACES][MAX_VERTS_HF];

      int v2hf_num[MAX_VERTICES];
      int v2hf[MAX_VERTICES][MAX_INCIDENT_HF];

      int e2v[MAX_EDGES][2];
      int e2hf[MAX_EDGES][2];
      int f2leid[MAX_FACES][MAX_VERTS_HF];
      int lookup_leids[MAX_VERTICES][MAX_VERTICES];
    };

    static  LocalMaps3D lConnMap3D[4];

    int get_index_from_type(EntityHandle cid);

    bool find_matching_implicit_edge_in_cell( EntityHandle eid, EntityHandle *cid, int *leid);

    bool find_matching_halfface(EntityHandle fid, EntityHandle *cid, int *leid);
  };

} // namespace moab 

#endif

