/**
 * MOAB, a Mesh-Oriented datABase, is a software component for creating,
 * storing and accessing finite element mesh data.
 * 
 * Copyright 2004 Sandia Corporation.  Under the terms of Contract
 * DE-AC04-94AL85000 with Sandia Coroporation, the U.S. Government
 * retains certain rights in this software.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 */

#ifdef WIN32
#pragma warning (disable : 4786)
#endif

#include "HalfFacetRep.hpp"
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include <queue>
#include <stack>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "moab/CN.hpp"

namespace moab {

  MESHTYPE HalfFacetRep::get_mesh_type(int nverts, int nedges, int nfaces, int ncells)
  {
    MESHTYPE mesh_type;

    if (nverts && nedges && (!nfaces) && (!ncells))
      mesh_type = CURVE;
    else if (nverts && !nedges && nfaces && !ncells)
      mesh_type = SURFACE;
    else if (nverts && nedges && nfaces && !ncells)
      mesh_type = SURFACE_MIXED;
    else if (nverts && !nedges && !nfaces && ncells)
      mesh_type = VOLUME;
    else if (nverts && nedges && !nfaces && ncells)
      mesh_type = VOLUME_MIXED_1;
    else if (nverts && !nedges && nfaces && ncells)
      mesh_type = VOLUME_MIXED_2;
    else if (nverts && nedges && nfaces && ncells)
      mesh_type = VOLUME_MIXED;

    return mesh_type;
  }

  /*******************************************************
   * initialize                                          *
   ******************************************************/

  ErrorCode HalfFacetRep::initialize(){

    ErrorCode error;

    error = mb->get_entities_by_dimension( 0, 0, _verts);
    if (MB_SUCCESS != error) return error;

    error = mb->get_entities_by_dimension( 0, 1, _edges);
    if (MB_SUCCESS != error) return error;

    error = mb->get_entities_by_dimension( 0, 2, _faces);
    if (MB_SUCCESS != error) return error;
    
    error = mb->get_entities_by_dimension( 0, 3, _cells);
    if (MB_SUCCESS != error) return error;
    
    int nverts = _verts.size();
    int nedges = _edges.size();
    int nfaces = _faces.size();
    int ncells = _cells.size();

    MESHTYPE mesh_type = get_mesh_type(nverts, nedges, nfaces, ncells);
    std::cout<<"MeshType = "<<mesh_type<<std::endl;
 
    if (mesh_type == CURVE){
        error = init_curve();
        if (MB_SUCCESS != error) return error;
      }
    
    else if (mesh_type == SURFACE){
      error = init_surface();
      if (MB_SUCCESS != error) return error;
      }

    else if (mesh_type == SURFACE_MIXED){
        error = init_curve();
        if (MB_SUCCESS != error) return error;
        error = init_surface();
        if (MB_SUCCESS != error) return error;
      }
    else if (mesh_type == VOLUME){
        error = init_volume();
        if (MB_SUCCESS != error) return error;
      }

    else if (mesh_type == VOLUME_MIXED_1){
        error = init_curve();
        if (MB_SUCCESS != error) return error;
        error = init_volume();
        if (MB_SUCCESS != error) return error;
    }

    else if (mesh_type == VOLUME_MIXED_2){
        error = init_surface();
        if (MB_SUCCESS != error) return error;
        error = init_volume();
        if (MB_SUCCESS != error) return error;
      }

    else if (mesh_type == VOLUME_MIXED){
        error = init_curve();
        if (MB_SUCCESS != error) return error;
        error = init_surface();
        if (MB_SUCCESS != error) return error;
        error = init_volume();
        if (MB_SUCCESS != error) return error;
      }

    return MB_SUCCESS;
  }

  ErrorCode HalfFacetRep::init_curve()
  {
    ErrorCode error;

    EntityHandle sdefval[2] = {0,0};  int sval[2] = {0,0};
    EntityHandle idefval = 0; int ival = 0;

    error = mb->tag_get_handle("__SIBHVS_EID", 2, MB_TYPE_HANDLE, sibhvs_eid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__SIBHVS_LVID", 2, MB_TYPE_INTEGER, sibhvs_lvid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HV_EID", 1, MB_TYPE_HANDLE, v2hv_eid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HV_LVID", 1, MB_TYPE_INTEGER, v2hv_lvid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
    if (MB_SUCCESS != error) return error;

    error = determine_sibling_halfverts(_verts, _edges);
    if (MB_SUCCESS != error) return error;
    error = determine_incident_halfverts(_edges);
    if (MB_SUCCESS != error) return error;

    return MB_SUCCESS;
  }

  ErrorCode HalfFacetRep::init_surface()
  {
    ErrorCode error;

    int nepf = local_maps_2d(*_faces.begin());
    EntityHandle * sdefval = new EntityHandle[nepf];
    int * sval = new int[nepf];
    EntityHandle  idefval = 0;
    int ival = 0;
    for (int i=0; i<nepf; i++){
        sdefval[i]=0;
        sval[i]=0;
      }

    // Create tag handles
    error = mb->tag_get_handle("__SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
    if (MB_SUCCESS != error) return error;

    // Construct ahf maps
    error = determine_sibling_halfedges(_faces);
    if (MB_SUCCESS != error) return error;
    error = determine_incident_halfedges(_faces);
    if (MB_SUCCESS != error) return error;

    bool *_ftags = new bool[_faces.size()];
    for (int i = 0; i<(int)_faces.size(); i++)
      _ftags[i] = false;

    ftags = _ftags;

    //for (int i = 0; i<(int)_faces.size(); i++)
     // std::cout<<"ftags["<<i<<"] = "<<ftags[i]<<std::endl;

    delete [] sdefval;
    delete [] sval;
    delete [] _ftags;

    return MB_SUCCESS;
  }

  ErrorCode HalfFacetRep::init_volume()
  {
    ErrorCode error;

    int index = get_index_from_type(*_cells.begin());
    int nfpc = lConnMap3D[index].num_faces_in_cell;
    EntityHandle * sdefval = new EntityHandle[nfpc];
    int * sval = new int[nfpc];
    EntityHandle idefval = 0;
    int ival = 0;
    for (int i = 0; i < nfpc; i++){
        sdefval[i] = 0;
        sval[i] = 0;
      }

    error = mb->tag_get_handle("__SIBHFS_CID", nfpc, MB_TYPE_HANDLE, sibhfs_cid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__SIBHFS_LFID", nfpc, MB_TYPE_INTEGER, sibhfs_lfid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HF_CID", 1, MB_TYPE_HANDLE, v2hf_cid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_handle("__V2HF_LFID", 1, MB_TYPE_INTEGER, v2hf_lfid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
    if (MB_SUCCESS != error) return error;

    error = determine_sibling_halffaces(_cells);
    if (MB_SUCCESS != error) return error;
    error = determine_incident_halffaces(_cells);
    if (MB_SUCCESS != error) return error;

    bool *_ctags = new bool[_cells.size()];
    for (int i=0; i<(int)_cells.size(); i++)
      _ctags[i] = false;
    ctags = _ctags;

    //for (int i = 0; i<(int)_cells.size(); ++i)
      //std::cout<<"ctags["<<i<<"] = "<<ctags[i]<<std::endl;

    delete [] sdefval;
    delete [] sval;
    delete [] _ctags;

    return MB_SUCCESS;
  }

  /*******************************************************
   * deinitialize                                        *
   ******************************************************/
   ErrorCode HalfFacetRep::deinitialize()
   {
     ErrorCode error;
     MESHTYPE mesh_type = get_mesh_type(_verts.size(), _edges.size(), _faces.size(), _cells.size());

     if (mesh_type == CURVE){
         error = deinit_curve();
         if (MB_SUCCESS != error) return error;
       }
     else if (mesh_type == SURFACE){
         error = deinit_surface();
         if (MB_SUCCESS != error) return error;
       }
     else if (mesh_type == SURFACE_MIXED){
         error = deinit_curve();
         if (MB_SUCCESS != error) return error;
         error = deinit_surface();
         if (MB_SUCCESS != error) return error;
       }
     else if (mesh_type == VOLUME){
         error = deinit_volume();
         if (MB_SUCCESS != error) return error;
       }
     else if (mesh_type == VOLUME_MIXED_1){
         error = deinit_curve();
         if (MB_SUCCESS != error) return error;
         error = deinit_volume();
         if (MB_SUCCESS != error) return error;
       }
     else if (mesh_type == VOLUME_MIXED_2){
         error = deinit_surface();
         if (MB_SUCCESS != error) return error;
         error = deinit_volume();
         if (MB_SUCCESS != error) return error;         
       }
     else if (mesh_type == VOLUME_MIXED){
         error = deinit_curve();
         if (MB_SUCCESS != error) return error;
         error = deinit_surface();
         if (MB_SUCCESS != error) return error;
         error = deinit_volume();
         if (MB_SUCCESS != error) return error;
       }

     return MB_SUCCESS;
   }

   ErrorCode HalfFacetRep::deinit_curve(){
     ErrorCode error;
     error = mb->tag_delete(sibhvs_eid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(sibhvs_lvid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2hv_eid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2hv_lvid);
     if (MB_SUCCESS != error) return error;

     return MB_SUCCESS;
   }

   ErrorCode HalfFacetRep::deinit_surface(){
     ErrorCode error;
     error = mb->tag_delete(sibhes_fid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(sibhes_leid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2he_fid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2he_leid);
     if (MB_SUCCESS != error) return error;

     return MB_SUCCESS;
   }

   ErrorCode HalfFacetRep::deinit_volume(){
     ErrorCode error;
     error = mb->tag_delete(sibhfs_cid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(sibhfs_lfid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2hf_cid);
     if (MB_SUCCESS != error) return error;
     error = mb->tag_delete(v2hf_lfid);
     if (MB_SUCCESS != error) return error;

     return MB_SUCCESS;
   }

   //////////////////////////////////////////////////
   ErrorCode HalfFacetRep::print_tags()
   {
     ErrorCode error;
     int nepf = local_maps_2d(*_faces.begin());
     int index = get_index_from_type(*_cells.begin());
     int nfpc = lConnMap3D[index].num_faces_in_cell;

     //////////////////////////
     // Print out the tags
     EntityHandle start_edge = *_edges.begin();
     EntityHandle start_face = *_faces.begin();
     EntityHandle start_cell = *_cells.begin();
     std::cout<<"start_edge = "<<start_edge<<std::endl;
     std::cout<<"<SIBHVS_EID,SIBHVS_LVID>"<<std::endl;

     for (Range::iterator i = _edges.begin(); i != _edges.end(); ++i){
       EntityHandle eid[2];  int lvid[2];
       error = mb->tag_get_data(sibhvs_eid, &*i, 1, eid);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(sibhvs_lvid, &*i, 1, lvid);
       if (MB_SUCCESS != error) return error;

       if ((eid[0] == 0)&&(eid[1] != 0))
         std::cout<<"<"<<eid[0]<<","<<lvid[0]<<">"<<"      "<<"<"<<(eid[1]-start_edge)<<","<<lvid[1]<<">"<<std::endl;
       else if ((eid[1] == 0)&&(eid[0] != 0))
         std::cout<<"<"<<(eid[0]-start_edge)<<","<<lvid[0]<<">"<<"      "<<"<"<<eid[1]<<","<<lvid[1]<<">"<<std::endl;
     }

     std::cout<<"<V2HV_EID, V2HV_LVID>"<<std::endl;

     for (Range::iterator i = _verts.begin(); i != _verts.end(); ++i){
       EntityHandle eid; int lvid;
       error = mb->tag_get_data(v2hv_eid, &*i, 1, &eid);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(v2hv_lvid, &*i, 1, &lvid);
       if (MB_SUCCESS != error) return error;

       if (eid != 0)
         std::cout<<"For vertex = "<<*i<<"::Incident halfvertex "<<(eid-start_edge+1)<<"  "<<lvid<<std::endl;
       else
         std::cout<<"For vertex = "<<*i<<"::Incident halfvertex "<<eid<<"  "<<lvid<<std::endl;
     }

     std::cout<<"start_face = "<<start_face<<std::endl;
     std::cout<<"<SIBHES_FID,SIBHES_LEID>"<<std::endl;

     for (Range::iterator i = _faces.begin(); i != _faces.end(); ++i){
       std::vector<EntityHandle> fid(nepf);
       std::vector<int> leid(nepf);
       error = mb->tag_get_data(sibhes_fid, &*i, 1, &fid[0]);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(sibhes_leid, &*i, 1, &leid[0]);
       if (MB_SUCCESS != error) return error;

       for (int j=0; j<nepf; j++){
         if (fid[j] == 0)
           std::cout<<"<"<<fid[j]<<","<<leid[j]<<">"<<" ";
         else
           std::cout<<"<"<<(fid[j]-start_face+1)<<","<<leid[j]<<">"<<" ";
       }
       std::cout<<std::endl;
     }

     std::cout<<"<V2HE_FID, V2HE_LEID>"<<std::endl;

     for (Range::iterator i = _verts.begin(); i != _verts.end(); ++i){
       EntityHandle fid; int lid;
       error = mb->tag_get_data(v2he_fid, &*i, 1, &fid);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(v2he_leid, &*i, 1, &lid);
       if (MB_SUCCESS != error) return error;

       if (fid == 0)
         std::cout<<"For vertex = "<<*i<<"::Incident halfedge "<<fid<<"  "<<lid<<std::endl;
       else
         std::cout<<"For vertex = "<<*i<<"::Incident halfedge "<<(fid-start_face+1)<<"  "<<lid<<std::endl;
       }

     std::cout<<"start_cell = "<<start_cell<<std::endl;
     std::cout<<"<SIBHES_CID,SIBHES_LFID>"<<std::endl;

     for (Range::iterator i = _cells.begin(); i != _cells.end(); ++i){
       std::vector<EntityHandle> cid(nfpc);
       std::vector<int> lfid(nfpc);
       error = mb->tag_get_data(sibhfs_cid, &*i, 1, &cid[0]);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(sibhfs_lfid, &*i, 1, &lfid[0]);
       if (MB_SUCCESS != error) return error;

       for(int j = 0; j < nfpc; j++)
         if (cid[j] == 0)
           std::cout<<"<"<<cid[j]<<","<<lfid[j]<<">"<<" ";
         else
           std::cout<<"<"<<(cid[j]-start_cell+1)<<","<<lfid[j]<<">"<<" ";
       std::cout<<std::endl;
     }
     std::cout<<" Finished printing AHF:sibhfs "<<std::endl;

     std::cout<<"<V2HF_CID, V2HF_LFID>"<<std::endl;

     for (Range::iterator i = _verts.begin(); i != _verts.end(); ++i){
       EntityHandle fid; int lid;
       error = mb->tag_get_data(v2hf_cid, &*i, 1, &fid);
       if (MB_SUCCESS != error) return error;
       error = mb->tag_get_data(v2hf_lfid, &*i, 1, &lid);
       if (MB_SUCCESS != error) return error;

       if (fid==0)
         std::cout<<"For vertex = "<<*i<<"::Incident halfface "<<fid<<"  "<<lid<<std::endl;
       else
         std::cout<<"For vertex = "<<*i<<"::Incident halfface "<<(fid-start_cell+1)<<"  "<<lid<<std::endl;
     }
     return MB_SUCCESS;
   }

   /**********************************************************
   *      User interface for adjacency functions            *
   ********************************************************/

   ErrorCode HalfFacetRep::get_upward_incidences(EntityHandle ent, int out_dim, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int> * lids)
   {
    ErrorCode error;
    int in_dim = mb->dimension_from_handle(ent);

    if ((in_dim == 0) && (out_dim == 1))
      {
        error = get_upward_incidences_1d(ent, adjents, local_id, lids);
        if (MB_SUCCESS != error) return error;
      }

    else if ((in_dim == 1) && (out_dim == 2))
      {
        error = get_upward_incidences_2d(ent, adjents, local_id, lids);
        if (MB_SUCCESS != error) return error;
      }
    else if ((in_dim == 1) && (out_dim == 3))
      {
        error = get_upward_incidences_edg_3d(ent, adjents, local_id, lids);
        if (MB_SUCCESS != error) return error;
      }
    else if ((in_dim == 2) && (out_dim ==3))
      {
        error = get_upward_incidences_face_3d(ent, adjents, local_id, lids);
        if (MB_SUCCESS != error) return error;
      }
    return MB_SUCCESS;
   }

   ErrorCode HalfFacetRep::get_neighbor_adjacencies(EntityHandle ent, bool add_inent, std::vector<EntityHandle> &adjents)
   {
     ErrorCode error;
     int in_dim = mb->dimension_from_handle(ent);

     if (in_dim == 1)
       {
         error = get_neighbor_adjacencies_1d(ent, add_inent, adjents);
         if (MB_SUCCESS != error) return error;
       }

     else if (in_dim == 2)
       {
         error = get_neighbor_adjacencies_2d(ent, add_inent, adjents);
         if (MB_SUCCESS != error) return error;
       }
     else if (in_dim == 3)
       {
         error = get_neighbor_adjacencies_3d(ent, add_inent, adjents);
         if (MB_SUCCESS != error) return error;
       }
     return MB_SUCCESS;
   }

  /******************************************************** 
  * 1D: sibhvs, v2hv, incident and neighborhood queries   *
  *********************************************************/
  ErrorCode HalfFacetRep::determine_sibling_halfverts( Range &verts,  Range &edges)
  {
    ErrorCode error;
    EntityHandle start_edge = *edges.begin();

    // Create pointers to the tags for direct access to memory
    int count, *sibhvs_lvid_ptr; EntityHandle *sibhvs_eid_ptr;
    error = mb->tag_iterate(sibhvs_eid, edges.begin(), edges.end(), count, (void*&)sibhvs_eid_ptr);
    if (MB_SUCCESS != error) return error;
    assert(count == (int) edges.size());
    error = mb->tag_iterate(sibhvs_lvid, edges.begin(), edges.end(), count, (void*&)sibhvs_lvid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) edges.size());
 
    std::vector<EntityHandle> conn(2), adj;   
    
    // Main Loop over all vertices 
    for (Range::iterator v_it = verts.begin(); v_it != verts.end(); ++v_it) {
      EntityHandle v = *v_it;  
      
      adj.clear();
      error = mb->get_adjacencies(&*v_it, 1, 1, false, adj); 
      if (MB_SUCCESS != error) return error;
   
      if (adj.size()>1){
	 error = mb->get_connectivity(&adj[0], 1, conn); 
	 if (MB_SUCCESS != error) return error;

	 EntityHandle first_eid, prev_eid;
	 first_eid = prev_eid = adj[0];
	 int first_lvid = 1;
	 if (conn[0]==v)
	   first_lvid = 0;      
	 int prev_lvid = first_lvid;
	 
	 for(int j=1; j<(int)adj.size();++j){	   
	   sibhvs_eid_ptr[2*(prev_eid-start_edge)+prev_lvid] = adj[j];

	   error = mb->get_connectivity(&adj[j], 1, conn); 
	   if (MB_SUCCESS != error) return error;	  

	   if (conn[0]==v){	     	     
	     sibhvs_lvid_ptr[2*(prev_eid-start_edge)+prev_lvid] = 0;
	     prev_lvid = 0;
	   }
	   else	{
	     sibhvs_lvid_ptr[2*(prev_eid-start_edge)+prev_lvid] = 1;
	     prev_lvid = 1;  	   
	   }
	   prev_eid = adj[j];
	 }

	 if (prev_eid != first_eid){	   
	   sibhvs_eid_ptr[2*(prev_eid-start_edge) + prev_lvid] = first_eid;	   
	   sibhvs_lvid_ptr[2*(prev_eid-start_edge) + prev_lvid] = first_lvid;	   
	 }	 
      }
    }  


    /*for (Range::iterator i = _verts.begin(); i != _verts.end(); i++)
      {
        std::vector<EntityHandle> adj;
        error = mb->get_adjacencies(&*i, 1, 1, false, adj);
        error = mb->remove_adjacencies(*i, &adj[0], adj.size());
      }*/

    std::cout<<"Finished creating sibling half-verts map for curve mesh"<<std::endl;

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::determine_incident_halfverts( Range &edges){
    ErrorCode error;

    for (Range::iterator e_it = edges.begin(); e_it != edges.end(); ++e_it){      
      std::vector<EntityHandle> conn(2);
      error = mb->get_connectivity(&*e_it, 1, conn);
      if (MB_SUCCESS != error) return error;

      for(int i=0; i<2; ++i){
	EntityHandle v = conn[i], eid = 0;	 
	error = mb->tag_get_data(v2hv_eid, &v, 1, &eid); 
	if (MB_SUCCESS != error) return error;

	if (eid==0){
	  EntityHandle edg = *e_it;	  
	  int lvid = i;

	  error = mb->tag_set_data(v2hv_eid, &v, 1, &edg); 
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_set_data(v2hv_lvid, &v, 1, &lvid);
	  if (MB_SUCCESS != error) return error;
	}
      }      
    }  
    std::cout<<"Finished creating incident half-verts"<<std::endl;

    return MB_SUCCESS;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::get_upward_incidences_1d( EntityHandle vid, std::vector< EntityHandle > &adjents, bool local_id, std::vector<int> * lvids){
    ErrorCode error;     
    EntityHandle start_eid, eid, sibeid[2];
    int start_lid, lid, siblid[2];    
   
    error = mb->tag_get_data(v2hv_eid, &vid, 1, &start_eid);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(v2hv_lvid, &vid, 1, &start_lid);    
    if (MB_SUCCESS != error) return error;

    eid = start_eid; lid = start_lid;

    if (eid != 0){
      adjents.push_back(eid);
      if (local_id)
        lvids->push_back(lid);

      while (eid !=0) {	 
	  error = mb->tag_get_data(sibhvs_eid, &eid, 1, sibeid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhvs_lvid, &eid, 1, siblid);
	  if (MB_SUCCESS != error) return error;

	  eid = sibeid[lid];
	  lid = siblid[lid];
	  if ((!eid)||(eid == start_eid))
	    break;
	  adjents.push_back(eid);
	  if (local_id)
	    lvids->push_back(lid);
      }
    }

    return MB_SUCCESS;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::get_neighbor_adjacencies_1d( EntityHandle eid, bool add_inent, std::vector<EntityHandle> &adjents){

    ErrorCode error;

    if (add_inent)
      adjents.push_back(eid);

    EntityHandle sib_eids[2], sibhv_eid;
    int sib_lids[2], sibhv_lid;    

    error = mb->tag_get_data(sibhvs_eid, &eid, 1, sib_eids);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(sibhvs_lvid, &eid, 1, sib_lids);
    if (MB_SUCCESS != error) return error;
    
    for (int lid = 0;lid < 2; ++lid){
      sibhv_eid = sib_eids[lid];
      sibhv_lid = sib_lids[lid];

      if (sibhv_eid != 0){
        adjents.push_back(sibhv_eid);
	
	EntityHandle next_eid[2], hv_eid; 
	int next_lid[2], hv_lid;
	
	error = mb->tag_get_data(sibhvs_eid, &sibhv_eid, 1, next_eid);
	if (MB_SUCCESS != error) return error;
	error = mb->tag_get_data(sibhvs_lvid, &sibhv_eid, 1, next_lid);
	if (MB_SUCCESS != error) return error;
	
	hv_eid = next_eid[sibhv_lid];
	hv_lid = next_lid[sibhv_lid];
	
	while (hv_eid != 0){	    
	  if (hv_eid != eid)
	    adjents.push_back(hv_eid);
	  
	  error = mb->tag_get_data(sibhvs_eid, &hv_eid, 1, next_eid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhvs_lvid, &hv_eid, 1, next_lid);
	  if (MB_SUCCESS != error) return error;

	  if (next_eid[hv_lid] == sibhv_eid)
	    break;
	  hv_eid = next_eid[hv_lid];
	  hv_lid = next_lid[hv_lid];      
	}
      }
    } 

    return MB_SUCCESS;   
  }
  
  /*******************************************************
  * 2D: sibhes, v2he, incident and neighborhood queries  *
  ********************************************************/
  int HalfFacetRep::local_maps_2d( EntityHandle face)
  {
      EntityType type = mb->type_from_handle(face);
      int nepf;
    if (type == MBTRI) nepf = 3; 
    else if (type ==MBQUAD) nepf = 4;
    
    return nepf;
  }
  /////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::local_maps_2d( EntityHandle face, int *next, int *prev)
  {
    // nepf: Number of edges per face
    // next: Local ids of next edges
    // prev: Local ids of prev edges
    EntityType type = mb->type_from_handle(face);
   
    if (type == MBTRI)
    {  
      next[0] = 1; next[1] = 2; next[2] = 0;
      prev[0] = 2; prev[1] = 0; prev[2] = 1;
    }
    else if (type ==MBQUAD)
    {
      next[0] = 1; next[1] = 2; next[2] = 3; next[3] = 0;
      prev[0] = 3; prev[1] = 0; prev[2] = 1; prev[3] = 2;
    }

    return MB_SUCCESS;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::determine_sibling_halfedges( Range &faces)
  {
    ErrorCode error;
    EntityHandle start_face = *faces.begin();  

    // Create pointers to the tags for direct access to memory    
    int count, *sibhes_leid_ptr; EntityHandle *sibhes_fid_ptr;
    error = mb->tag_iterate(sibhes_fid, faces.begin(), faces.end(), count, (void*&)sibhes_fid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) faces.size());
    error = mb->tag_iterate(sibhes_leid, faces.begin(), faces.end(), count, (void*&)sibhes_leid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) faces.size());

  
    int  id, kid, next[4]={1,2,3,0};
    bool hasthree = false;
    
    int nepf = local_maps_2d(start_face);
    if(nepf == 3) hasthree = true;
     
    /* Main Loop over all faces */ 
    for (Range::iterator t = faces.begin(); t != faces.end(); ++t) {
      for (int k = 0; k < nepf; k++){
	if (sibhes_fid_ptr[nepf*(*t-start_face)+k] != 0)	  
	  continue;	       

	std::vector<EntityHandle> conn(nepf);
	error = mb->get_connectivity(&*t, 1, conn); 
	if (MB_SUCCESS != error) return error;
       	    
	std::vector<EntityHandle> curedg_verts(2), adj;

	kid = k + (hasthree & (k==2)); id = next[kid]; 
	curedg_verts[0] = conn[k]; curedg_verts[1] = conn[id];

	// Find all the incident faces on this half-edge
	error = mb->get_adjacencies(&curedg_verts[0], 2, 2, false, adj, Interface::INTERSECT);	
	if (MB_SUCCESS != error) return error;

	if (adj.size() == 1)	  
	  continue;

	/* Step 1: Connect all half-edges corresponding to this edge*/
	EntityHandle first_fid = *t, prev_fid = *t;
	int first_leid = k, prev_leid = k;

	std::vector<EntityHandle> adj_conn(nepf);

	for (int j=0; j<(int)adj.size(); ++j){
	  if (adj[j] != *t){	    
	    adj_conn.clear();
	    error = mb->get_connectivity(&adj[j], 1, adj_conn); 
	    if (MB_SUCCESS != error) return error;

	    std::vector<EntityHandle> adjedg_verts(2);
	    for (int l=0; l<nepf; ++l){
	      kid = l + (hasthree & (l==2)); id=next[kid]; 
	      adjedg_verts[0] = adj_conn[l]; adjedg_verts[1] = adj_conn[id]; 

	      int direct,offset;	      
	      bool they_match = CN::ConnectivityMatch(&adjedg_verts[0],&curedg_verts[0],2,direct,offset);	     

	      if (they_match){
		sibhes_fid_ptr[nepf*(prev_fid-start_face)+prev_leid] = adj[j];
		sibhes_leid_ptr[nepf*(prev_fid-start_face)+prev_leid] = l;
		prev_fid = adj[j];
		prev_leid = l;
		break;
	      }
	    }
	  }
	}

	/* Step 2: Complete the cycle by connecting the first and last halfedges*/
	if (prev_fid != first_fid){
	  sibhes_fid_ptr[nepf*(prev_fid-start_face) + prev_leid] = first_fid;
	  sibhes_leid_ptr[nepf*(prev_fid-start_face) + prev_leid] = first_leid;
	}
      }
    }

    /*for (Range::iterator i = _verts.begin(); i != _verts.end(); i++)
      {
        std::vector<EntityHandle> adj;
        error = mb->get_adjacencies(&*i, 1, 2, false, adj);
        if (MB_SUCCESS != error) return error;
        error = mb->remove_adjacencies(*i, &adj[0], adj.size());
        if (MB_SUCCESS != error) return error;
      }*/

    //if (MB_SUCCESS != error) return error;

    std::cout<<"Finished constructing sibling halfedges map for a surface mesh"<<std::endl;

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::determine_incident_halfedges( Range &faces)
  {
    ErrorCode error;    

    int nepf = local_maps_2d(*faces.begin());

    std::vector<EntityHandle> conn(nepf);
    std::vector<EntityHandle> sibfid(nepf);

    for (Range::iterator it = faces.begin(); it != faces.end(); ++it){      
      conn.clear();
      error = mb->get_connectivity(&*it, 1, conn); 
      if (MB_SUCCESS != error) return error;

      sibfid.clear();
      error = mb->tag_get_data(sibhes_fid, &*it, 1, &sibfid[0]);
      if (MB_SUCCESS != error) return error;

      for(int i=0; i<nepf; ++i){
	EntityHandle v = conn[i],  vfid = 0;
	error = mb->tag_get_data(v2he_fid, &v, 1, &vfid);
	if (MB_SUCCESS != error) return error;

	if ((vfid==0)||((vfid!=0) && (sibfid[i]==0))){
	  EntityHandle fid = *it;	  
	  int lid = i;
	  error = mb->tag_set_data(v2he_fid, &v, 1, &fid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_set_data(v2he_leid, &v, 1, &lid);
	  if (MB_SUCCESS != error) return error;
	}
      }      
    }     
    std::cout<<"Finished creating incident half-edges map"<<std::endl;
    
    return MB_SUCCESS;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::get_upward_incidences_2d( EntityHandle eid, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int>  * leids)
{

  // Given an explicit edge eid, find the incident faces.

    ErrorCode error;
    EntityHandle he_fid=0; int he_lid=0;

    // Step 1: Given an explicit edge, find a corresponding half-edge from the surface mesh.
    bool found = find_matching_halfedge(eid, &he_fid, &he_lid);

    // Step 2: If there is a corresponding half-edge, collect all sibling half-edges and store the incident faces.
    if (found)
      {
        error = get_upward_incidences_2d(he_fid, he_lid, true, adjents, local_id, leids);
        if (MB_SUCCESS != error) return error;
      }
    else
      std::cout<<"No matching half-edge corresponding to given edge"<<std::endl;

    return MB_SUCCESS;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::get_upward_incidences_2d( EntityHandle fid,  int leid, bool add_inent, std::vector<EntityHandle> &fids, bool local_id, std::vector<int> * leids)
  {
    // Given an implicit half-edge <fid, leid>, find the incident half-edges.
    ErrorCode error;
    int nepf = local_maps_2d(fid);

    if (fid == 0)
      {
        std::cout<<"Input face handle is zero"<<std::endl;
        return MB_SUCCESS;
      }

    if (add_inent)
      {
        fids.push_back(fid);
        if (local_id)
          leids->push_back(leid);
      }

    std::vector<EntityHandle> sib_fids(nepf);
    std::vector<int> sib_lids(nepf);
    error = mb->tag_get_data(sibhes_fid, &fid, 1, &sib_fids[0]);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(sibhes_leid, &fid, 1, &sib_lids[0]);
    if (MB_SUCCESS != error) return error;

    EntityHandle curfid = sib_fids[leid];
    int curlid = sib_lids[leid];
    
    while ((curfid != fid)&&(curfid != 0)){//Should no go into the loop when no sibling exists
        fids.push_back(curfid);
        if (local_id)
          leids->push_back(curlid);

      sib_fids.clear();
      sib_lids.clear();
      error = mb->tag_get_data(sibhes_fid, &curfid, 1, &sib_fids[0]);
      if (MB_SUCCESS != error) return error;
      error = mb->tag_get_data(sibhes_leid, &curfid, 1, &sib_lids[0]);
      if (MB_SUCCESS != error) return error;
      
      curfid = sib_fids[curlid];
      curlid = sib_lids[curlid];
    }

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::get_upward_incidences_2d( EntityHandle fid,  int lid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid)
  {
    //Given an implicit half-edge <he_fid, he_lid>, add the incident half-edges to an input queue, if the half-edge doesn't exist in the queue. 

    ErrorCode error; 
    int nepf = local_maps_2d(fid);
    EntityHandle start_face = *_faces.begin();
    
    std::vector<EntityHandle> sib_fids(nepf);
    std::vector<int> sib_lids(nepf);
    EntityHandle curfid = fid;
    int curlid = lid;

    do {
      sib_fids.clear();
      sib_lids.clear();
      error = mb->tag_get_data(sibhes_fid, &curfid, 1, &sib_fids[0]);
      if (MB_SUCCESS != error) return error;
      error = mb->tag_get_data(sibhes_leid, &curfid, 1, &sib_lids[0]);
      if (MB_SUCCESS != error) return error;

      curfid = sib_fids[curlid];
      curlid = sib_lids[curlid];

      if(curfid == 0)
        break;

      if (!(ftags[curfid-start_face])){
        queue_fid.push(curfid);
        queue_lid.push(curlid);
        }

    }while (curfid != fid);
    
    return MB_SUCCESS;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool HalfFacetRep::find_matching_halfedge( EntityHandle eid, EntityHandle *hefid, int *helid){
    ErrorCode error;
    EntityHandle start_face = *_faces.begin();
    std::vector<EntityHandle> conn(2);
    error = mb->get_connectivity(&eid, 1, conn);
    if (MB_SUCCESS != error) return error;

    EntityHandle fid; int lid;
    error = mb->tag_get_data(v2he_fid, &conn[0], 1, &fid);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(v2he_leid, &conn[0], 1, &lid);
    if (MB_SUCCESS != error) return error;

    bool found = false;
    if (fid!=0){
      std::queue<EntityHandle> queue_fid;
      std::queue<int> queue_lid;
      std::vector<EntityHandle> trackfaces;

      EntityHandle vid = conn[0];

      error = gather_halfedges(vid, fid, lid, queue_fid, queue_lid, trackfaces);
      if (MB_SUCCESS != error) return error;

      found =  collect_and_compare(conn, queue_fid, queue_lid, trackfaces, hefid, helid);

      if (found)
        {
          for (std::vector<EntityHandle>::iterator it = trackfaces.begin(); it != trackfaces.end(); ++it){
              ftags[*it - start_face] = false;
            }
        }
      }
    return found;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::gather_halfedges( EntityHandle vid,  EntityHandle he_fid,  int he_lid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces)
  {  
    ErrorCode error;
    EntityHandle he2_fid = 0; int he2_lid = 0;

    error = another_halfedge(vid, he_fid, he_lid, &he2_fid, &he2_lid);
    if (MB_SUCCESS != error) return error;

    queue_fid.push(he_fid); queue_lid.push(he_lid);
    queue_fid.push(he2_fid); queue_lid.push(he2_lid);
    trackfaces.push_back(he_fid);
    ftags[he_fid-*_faces.begin()] = true;
    
    error = get_upward_incidences_2d(he_fid, he_lid, queue_fid, queue_lid);
    if (MB_SUCCESS != error) return error;
    error = get_upward_incidences_2d(he2_fid, he2_lid, queue_fid, queue_lid);
    if (MB_SUCCESS != error) return error;

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ErrorCode HalfFacetRep::another_halfedge( EntityHandle vid,  EntityHandle he_fid,  int he_lid, EntityHandle *he2_fid, int *he2_lid)
  {    
    ErrorCode error;
    int nepf = local_maps_2d(he_fid);
    int * next = new int[nepf];
    int * prev = new int[nepf];
    error = local_maps_2d(he_fid, next, prev);
    if (MB_SUCCESS != error) return error;
    
    std::vector<EntityHandle> conn(nepf);
    error = mb->get_connectivity(&he_fid, 1, conn);
    if (MB_SUCCESS != error) return error;

    *he2_fid = he_fid;
    if (conn[he_lid] == vid)
      *he2_lid = prev[he_lid];
    else
      *he2_lid = next[he_lid];

    delete [] next;
    delete [] prev;
    return MB_SUCCESS;
  }
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool HalfFacetRep::collect_and_compare(std::vector<EntityHandle> &edg_vert,  std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces, EntityHandle *he_fid, int *he_lid)
  {
    ErrorCode error;
    int nepf = local_maps_2d(*_faces.begin());
    int *next = new int[nepf];
    int *prev = new int[nepf];
    error = local_maps_2d(*_faces.begin(), next, prev);
    if (MB_SUCCESS != error) return error;

    bool found = false;
    while (!queue_fid.empty())
      {
	EntityHandle curfid = queue_fid.front();
	int curlid = queue_lid.front();
	queue_fid.pop();
	queue_lid.pop();

	std::vector<EntityHandle> conn(nepf);
	error = mb->get_connectivity(&curfid, 1, conn);
	if (MB_SUCCESS != error) return error;

	int id = next[curlid];
	if (((conn[curlid]==edg_vert[0])&&(conn[id]==edg_vert[1]))||((conn[curlid]==edg_vert[1])&&(conn[id]==edg_vert[0]))){
	    *he_fid = curfid;
	    *he_lid = curlid;
	    found = true;
	    break;
	}

	if (ftags[curfid-*_faces.begin()])
	  continue;
	ftags[curfid-*_faces.begin()] = true;
	trackfaces.push_back(curfid);

	EntityHandle he2_fid; int he2_lid;
	error = another_halfedge(edg_vert[0], curfid, curlid, &he2_fid, &he2_lid);
	if (MB_SUCCESS != error) return error;
	error = get_upward_incidences_2d(he2_fid, he2_lid, queue_fid, queue_lid);
	if (MB_SUCCESS != error) return error;
       
      }
    
    delete [] next; 
    delete [] prev;
    return found;
  }
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::get_neighbor_adjacencies_2d( EntityHandle fid,  bool add_inent, std::vector<EntityHandle> &adjents)
  {
    ErrorCode error; 
    if (fid != 0){
      if (add_inent)
        adjents.push_back(fid);
      
      int nepf = local_maps_2d(fid);
      for (int lid = 0; lid < nepf; ++lid){
        error = get_upward_incidences_2d(fid, lid, false, adjents);
	if (MB_SUCCESS != error) return error;
      }
    }
    
    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  int HalfFacetRep::find_total_edges_2d(Range &faces)
  {
    ErrorCode error;
    int firstF = *faces.begin();
    int nepf = local_maps_2d(firstF);
    int nfaces = faces.size();

    int total_edges = nepf*nfaces;
    
    bool *trackF = new bool(total_edges);
    std::vector<EntityHandle> adj_fids;
    std::vector<int> adj_lids;

    for (Range::iterator f = faces.begin(); f != faces.end(); f++){
      for (int l = 0; l < nepf; l++){

	adj_fids.clear();
	adj_lids.clear();

	int id = nepf*(*f-firstF)+l;
	if (!trackF[id])
	  {
	    error = get_upward_incidences_2d(*f,l, false, adj_fids, true, &adj_lids);
	    if (MB_SUCCESS != error) return error;

	    total_edges -= adj_fids.size();

	    for (int i = 0; i < (int)adj_fids.size(); i++)
	      trackF[nepf*(adj_fids[i]-firstF)+adj_lids[i]] = true;
	  };
      };
   };
    
    delete [] trackF;        
    return total_edges;
  }

  /*******************************************************
  * 3D: sibhfs, v2hf, incident and neighborhood queries  *
  ********************************************************/

  int HalfFacetRep::get_index_from_type(EntityHandle cid)
  {
    int index;
    EntityType type = mb->type_from_handle(cid);
    if (type == MBTET)
      index = 0;
    else if (type == MBPYRAMID)
      index = 1;
    else if (type == MBPRISM)
      index = 2;
    else if (type == MBHEX)
      index = 3;

    return index;   
  }

  /////////////////////////////////////////////
   HalfFacetRep::LocalMaps3D HalfFacetRep::lConnMap3D[4] = 
    {
      // Tet
      {4, 6, 4, {3,3,3,3}, {{0,1,3},{1,2,3},{2,0,3},{0,2,1}},   {3,3,3,3},   {{0,2,3},{0,1,3},{1,2,3},{0,1,2}},   {{0,1},{1,2},{2,0},{0,3},{1,3},{2,3}},   {{3,0},{3,1},{3,2},{0,2},{0,1},{1,2}},   {{0,4,3},{1,5,4},{2,3,5},{2,1,0}},     {{-1,0,2,3},{0,-1,1,4},{2,1,-1,5},{3,4,5,-1}}},

      // Pyramid: Note: In MOAB pyramid follows the CGNS convention. Look up src/MBCNArrays.hpp
      {5, 8, 5, {4,3,3,3,3}, {{0,3,2,1},{0,1,4},{1,2,4},{2,3,4},{3,0,4}},  {3,3,3,3,4},   {{0,1,4},{0,1,2},{0,2,3},{0,3,4},{1,2,3,4}},   {{0,1},{1,2},{2,3},{3,0},{0,4},{1,4},{2,4},{3,4}},   {{0,1},{0,2},{0,3},{0,4},{1,4},{1,2},{2,3},{3,4}},    {{3,2,1,0},{0,5,4},{1,6,5},{2,7,6},{3,4,7}},    {{-1,0,-1,3,4},{0,-1,1,-1,5},{-1,1,-1,2,6},{3,-1,2,-1,7},{4,5,6,7,-1}}},

      // Prism
      {6, 9, 5, {4,4,4,3,3}, {{0,1,4,3},{1,2,5,4},{0,3,5,2},{0,2,1},{3,4,5}},  {3,3,3,3,3,3}, {{0,2,3},{0,1,3},{1,2,3},{0,2,4},{0,1,4},{1,4,2}},    {{0,1},{1,2},{2,0},{0,3},{1,4},{2,5},{3,4},{4,5},{5,3}},    {{0,3},{1,3},{2,3},{0,2},{0,1},{1,2},{0,4},{1,4},{2,4}},     {{0,4,6,3},{1,5,7,4},{2,3,8,5},{2,1,0},{6,7,8}},    {{-1,0,2,3,-1,-1},{0,-1,1,-1,4,-1},{2,1,-1,-1,-1,5},{3,-1,-1,-1,6,8},{-1,4,-1,6,-1,7},{-1,-1,5,8,7,-1}}},

      // Hex
      {8, 12, 6, {4,4,4,4,4,4}, {{0,1,5,4},{1,2,6,5},{2,3,7,6},{3,0,4,7},{0,3,2,1},{4,5,6,7}},   {3,3,3,3,3,3,3,3},   {{0,3,4},{0,1,4},{1,2,4},{2,3,4},{0,3,5},{0,1,5},{1,2,5},{2,3,5}},    {{0,1},{1,2},{2,3},{3,0},{0,4},{1,5},{2,6},{3,7},{4,5},{5,6},{6,7},{7,4}},     {{0,4},{1,4},{2,4},{3,4},{0,3},{0,1},{1,2},{2,3},{0,5},{1,5},{2,5},{3,5}},     {{0,5,8,4},{1,6,9,5},{2,7,10,6},{4,11,7,3},{3,2,1,0},{8,9,10,11}},     {{-1,0,-1,3,4,-1,-1,-1},{0,-1,1,-1,-1,5,-1,-1},{-1,1,-1,2,-1,-1,6,-1},{3,-1,2,-1,-1,-1,-1,7},{4,-1,-1,-1,-1,8,-1,11},{-1,5,-1,-1,8,-1,9,-1},{-1,-1,6,-1,-1,9,-1,10},{-1,-1,-1,7,11,-1,10,-1}}}
      
    };

  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::determine_sibling_halffaces( Range &cells)
  {
    ErrorCode error;
    EntityHandle start_cell = *cells.begin();
  
    int count, *sibhfs_lfid_ptr; 
    EntityHandle *sibhfs_cid_ptr;
    error = mb->tag_iterate(sibhfs_cid, cells.begin(), cells.end(), count, (void*&)sibhfs_cid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) cells.size());
    error = mb->tag_iterate(sibhfs_lfid, cells.begin(), cells.end(), count, (void*&)sibhfs_lfid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) cells.size());

    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;    

    std::vector<EntityHandle> conn(nvpc);
    /* Main Loop over all half-faces of the entire mesh */
    for (Range::iterator t = cells.begin(); t != cells.end(); ++t) {
      conn.clear();
      error = mb->get_connectivity(&*t, 1, conn); 
      if (MB_SUCCESS != error) return error;

      for (int k = 0; k < nfpc; k++){
	if (sibhfs_cid_ptr[nfpc*(*t-start_cell)+k] != 0)
	  continue;

	int nv_curF = lConnMap3D[index].hf2v_num[k];

	std::vector<EntityHandle> face_verts(nv_curF);
	for (int j=0; j < nv_curF; j++){
	  int ind = lConnMap3D[index].hf2v[k][j];
	  face_verts[j]=conn[ind];
	};	
	
	std::vector<EntityHandle> adj;
	error = mb->get_adjacencies(&face_verts[0], nv_curF, 3, false, adj, Interface::INTERSECT);
	if (MB_SUCCESS != error) return error;

	if (adj.size() == 1)   
	  continue;
	
	EntityHandle first_cid = *t, prev_cid = *t;
	int first_hfid = k, prev_hfid = k;       

	/* Step 1: Connect half-faces sharing this face*/
	for (int j=0; j<(int)adj.size(); ++j){	  
	  if (adj[j] != *t){
	    std::vector<EntityHandle> adj_conn(nvpc);	    
	    error = mb->get_connectivity(&adj[j], 1, adj_conn); 
	    if (MB_SUCCESS != error) return error;

	    for (int l=0; l < nfpc; ++l){
	      int nv_adjF = lConnMap3D[index].hf2v_num[l];
	      if (nv_adjF != nv_curF)
		continue;

	      std::vector<EntityHandle> adj_verts(nv_adjF);
	      for(int i = 0; i < nv_adjF; i++){
		int ind = lConnMap3D[index].hf2v[l][i];
		adj_verts[i] = adj_conn[ind];
	      }
	      
	      int direct,offset;     	     
	      bool they_match = CN::ConnectivityMatch(&adj_verts[0],&face_verts[0],nv_adjF,direct,offset);	     
	      if (they_match){
		sibhfs_cid_ptr[nfpc*(prev_cid-start_cell)+prev_hfid] = adj[j];
		sibhfs_lfid_ptr[nfpc*(prev_cid-start_cell)+prev_hfid] = l;
		prev_cid = adj[j]; prev_hfid = l;
		break;
	      }
	    }
	  }
	}

	/* Step 2: Complete the cycle by connecting the first and last half-faces*/
	if (prev_cid != first_cid){
	  sibhfs_cid_ptr[nfpc*(prev_cid-start_cell) + prev_hfid] = first_cid;
	  sibhfs_lfid_ptr[nfpc*(prev_cid-start_cell) + prev_hfid] = first_hfid;
	}
      }
    }

    /*for (Range::iterator i = _verts.begin(); i != _verts.end(); i++)
      {
        std::vector<EntityHandle> adj;
        error = mb->get_adjacencies(&*i, 1, 3, false, adj);
        error = mb->remove_adjacencies(*i, &adj[0], adj.size());
        if (MB_SUCCESS != error) return error;
      }*/

    std::cout<<"Finished constructing sibling halffaces map for volume mesh"<<std::endl;

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::determine_incident_halffaces( Range &cells)
  {
    ErrorCode error;

    int index = get_index_from_type(*cells.begin());
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;
  
    std::vector<EntityHandle> conn(nvpc);
    std::vector<EntityHandle> sib_cids(nfpc);

    for (Range::iterator t_it = cells.begin(); t_it != cells.end(); ++t_it){      
      EntityHandle tet = *t_it;
      conn.clear();
      error = mb->get_connectivity(&tet, 1, conn);     
      if (MB_SUCCESS != error) return error;

      sib_cids.clear(); 
      error = mb->tag_get_data(sibhfs_cid, &tet, 1, &sib_cids[0]);
      if (MB_SUCCESS != error) return error;
      
      for(int i=0; i<nvpc; ++i){
	EntityHandle v = conn[i], vcid;	
	error = mb->tag_get_data(v2hf_cid, &v, 1, &vcid);
	if (MB_SUCCESS != error) return error;

	int nhf_pv = lConnMap3D[index].v2hf_num[i];

	for (int j=0; j < nhf_pv; ++j){
	  int ind = lConnMap3D[index].v2hf[i][j];
	  if (vcid==0){
	    error = mb->tag_set_data(v2hf_cid, &v, 1, &tet);
	    if (MB_SUCCESS != error) return error;
	    error = mb->tag_set_data(v2hf_lfid, &v, 1, &ind);
	    if (MB_SUCCESS != error) return error;
	    break;
	  }
	  else if ((vcid!=0) && (sib_cids[ind]==0)){
	    error = mb->tag_set_data(v2hf_cid, &v, 1, &tet);
	    if (MB_SUCCESS != error) return error;
	    error = mb->tag_set_data(v2hf_lfid, &v, 1, &ind);
	    if (MB_SUCCESS != error) return error;
	  }
	}
      }
    }
    
    std::cout<<"Finished creating incident half-faces"<<std::endl;
    return MB_SUCCESS;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::determine_border_vertices( Range &cells, Tag isborder)
  {
    ErrorCode error;
    EntityHandle start_cell = *cells.begin();
    
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    int val= 1;
    std::vector<EntityHandle> conn(nvpc);
    std::vector<EntityHandle> sib_cids(nfpc);

    for(Range::iterator t= cells.begin(); t !=cells.end(); ++t){
      conn.clear();     
      error = mb->get_connectivity(&*t, 1, conn);
      if (MB_SUCCESS != error) return error;

      sib_cids.clear();
      error = mb->tag_get_data(sibhfs_cid, &*t, 1, &sib_cids[0]);   
      if (MB_SUCCESS != error) return error;
      
      for (int i = 0; i < nfpc; ++i){	
	if (sib_cids[i]==0){
	  int nvF = lConnMap3D[index].hf2v_num[i];
	  
	  for (int j = 0; j < nvF; ++j){
	    int ind = lConnMap3D[index].hf2v[i][j];
	    error = mb->tag_set_data(isborder, &conn[ind], 1, &val);	    
	    if (MB_SUCCESS != error) return error;
	  }
	}	
      }
    }    

    return MB_SUCCESS;
  }

 //////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::get_upward_incidences_edg_3d( EntityHandle eid, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int> * leids)
  {
    ErrorCode error; 
    
    EntityHandle cid=0;
    int leid=0;
    //Find one incident cell
    bool found = find_matching_implicit_edge_in_cell(eid, &cid, &leid);

    //Find all incident cells     
    if (found)
      {
        adjents.reserve(20);
        if (local_id)
          leids->reserve(20);
        error =get_upward_incidences_edg_3d(cid, leid, adjents, local_id, leids);
        if (MB_SUCCESS != error) return error;
      }
    else
      std::cout<<"No matching local edge corresponding to the given edge in the input cells"<<std::endl;

    return MB_SUCCESS;
  }

  //////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::get_upward_incidences_edg_3d( EntityHandle cid,  int leid, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int> * leids)
  {
    ErrorCode error;

    int index = get_index_from_type(cid);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    adjents.push_back(cid);
    if (local_id)
      leids->push_back(leid);

    //std::cout<<"cid = "<<cid<<", leid = "<<leid<<std::endl;

    //Loop over the two incident half-faces on this edge
    for(int i=0; i<2; i++)
      {
	std::vector<EntityHandle> conn(nvpc);
	error =mb->get_connectivity(&cid, 1, conn);
	if (MB_SUCCESS != error) return error;
	
	int id = lConnMap3D[index].e2v[leid][i];
	EntityHandle vert2 = conn[id];
	EntityHandle cur_cell = cid;
	int cur_leid = leid;
	int edge_ver = i;

	//std::cout<<"cur_cell = "<<cur_cell<<", cur_leid = "<<cur_leid<<std::endl;

	while(true){
	  int lfid = lConnMap3D[index].e2hf[cur_leid][edge_ver];
	  
	  std::vector<EntityHandle> sibcids(nfpc);
	  std::vector<int> siblfids(nfpc);
	  error = mb->tag_get_data(sibhfs_cid, &cur_cell,1, &sibcids[0]);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhfs_lfid, &cur_cell, 1, &siblfids[0]);
	  if (MB_SUCCESS != error) return error;
	  
	  cur_cell = sibcids[lfid];
	  lfid = siblfids[lfid];

	  //std::cout<<"cur_cell = "<<cur_cell<<", lfid = "<<lfid<<std::endl;
	  
	  //Check if loop reached starting cell or a boundary
	  if (cur_cell == cid ||  cur_cell==0)
	    break;
	  
	  std::vector<EntityHandle> temp_conn(nvpc);
	  error =mb->get_connectivity(&cur_cell, 1, temp_conn);
	  if (MB_SUCCESS != error) return error;

	  //Find the local edge id wrt to sibhf
	  int temp_lvid = -1;
	  int nv_curF = lConnMap3D[index].hf2v_num[lfid];
	  
	  for (int j=0; j<nv_curF; j++){
	    temp_lvid = lConnMap3D[index].hf2v[lfid][j];
	    EntityHandle temp_vert2 = temp_conn[temp_lvid];

	    //std::cout<<"temp_lvid = "<<temp_lvid<<", temp_vert2 = "<<temp_vert2<<std::endl;
	    //std::cout<<"vert2 = "<<vert2<<std::endl;

	    if (vert2 == temp_vert2){
	      cur_leid = lConnMap3D[index].f2leid[lfid][j];
	      //std::cout<<"cur_leid = "<<cur_leid<<std::endl;
	      break;
	    }
	  }

	  if (temp_lvid == lConnMap3D[index].e2v[cur_leid][0])
	    edge_ver = 0;
	  else
	    edge_ver = 1;
        

	  //Memory allocation
	  if (adjents.capacity()<=adjents.size()){
	    if (100<adjents.size()){
		std::cout<<"Too many incident cells"<<std::endl;
		return MB_FAILURE;
	    }
	    adjents.reserve(20+adjents.size());
	    if (local_id)
	      leids->reserve(20+leids->size());
	  }

	  //insert new incident tetrahedra and local edge ids
	  adjents.push_back(cur_cell);
	  if (local_id)
	    leids->push_back(cur_leid);
	}
	//Loop back
	if (cur_cell != 0)
	  break;
      }
    return MB_SUCCESS;
  }
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  HalfFacetRep::get_upward_incidences_face_3d( EntityHandle fid, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int> * lfids)
  {
    ErrorCode error;

    EntityHandle cid = 0;
    int lid = 0;
    bool found = find_matching_halfface(fid, &cid, &lid);

   // std::cout<<"cid = "<<cid<<", lid = "<<lid<<", found = "<<found<<std::endl;

    if (found){
      error = get_upward_incidences_face_3d(cid, lid, adjents, local_id, lfids);
      if (MB_SUCCESS != error) return error;
      }
    else
      std::cout<<"No matching half-face corresponding to given face in the input cells"<<std::endl;

    return MB_SUCCESS;
  }
  ///////////////////////////////////////////
  ErrorCode HalfFacetRep::get_upward_incidences_face_3d( EntityHandle cid,  int lfid, std::vector<EntityHandle> &adjents, bool local_id, std::vector<int> * lfids)
  {
    ErrorCode error;

    EntityHandle start_cell = *_cells.begin();
    int index = get_index_from_type(start_cell);
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    std::vector<EntityHandle> sibcids(nfpc);
    std::vector<int> siblids(nfpc);
    error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sibcids[0]);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(sibhfs_lfid, &cid, 1, &siblids[0]);
    if (MB_SUCCESS != error) return error;

    EntityHandle sibcid = sibcids[lfid];
    int siblid = siblids[lfid];
    if (sibcid > 0)
      {
        adjents.push_back(cid);
        adjents.push_back(sibcid);
        if (local_id)
          {
            lfids->push_back(lfid);
            lfids->push_back(siblid);
          }
      }
    else if (sibcid == 0)
      {
        adjents.push_back(cid);
        if (local_id)
          lfids->push_back(lfid);
      }

    return MB_SUCCESS;
  }
  /////////////////////////////////////////////////////////////////
 bool HalfFacetRep::find_matching_implicit_edge_in_cell( EntityHandle eid, EntityHandle *cid, int *leid)
  {
    ErrorCode error;
    
    // Find the edge vertices
    std::vector<EntityHandle> econn(2);
    error = mb->get_connectivity(&eid, 1, econn);
    if (MB_SUCCESS != error) return error;

    EntityHandle v_start = econn[0], v_end = econn[1];
    
    EntityHandle cell2origin, cell2end;
    error =mb->tag_get_data(v2hf_cid, &v_start, 1, &cell2origin);
    if (MB_SUCCESS != error) return error;
    error =mb->tag_get_data(v2hf_cid, &v_end, 1, &cell2end);
    if (MB_SUCCESS != error) return error;
    
    bool found = false;
    if (cell2origin == 0|| cell2end == 0){
      return found;
    }
    
    EntityHandle start_cell = *_cells.begin();
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    std::vector<EntityHandle> trackcells;
    std::queue<EntityHandle> cellq; 
    cellq.push(cell2origin);
    
    while (!cellq.empty()){
      //pop out the oldest one and mark as checked in ctags
      EntityHandle cell_id = cellq.front();
      cellq.pop();
      trackcells.push_back(cell_id);
      ctags[cell_id - start_cell] = true;
      
      //found = false;
      int lvid = -1, ltv = -1;
      
      std::vector<EntityHandle> conn(nvpc);
      error =mb->get_connectivity(&cell_id, 1, conn);
      if (MB_SUCCESS != error) return error;

      //locate v_origin in poped out tet, check if v_end is in
      for (int i = 0; i<nvpc; i++){
	if (v_start == conn[i]){
	  lvid = i;
	} 
	else if (v_end == conn[i]){
	  found = true;
	  ltv = i;
	}
      }
      
      if (found){
         *cid = cell_id;
         *leid = lConnMap3D[index].lookup_leids[lvid][ltv];

	 for (std::vector<EntityHandle>::iterator it = trackcells.begin(); it != trackcells.end(); ++it){
	   ctags[*it - start_cell] = false;
	 }
	 return found;
      }
      
      //push back new found unchecked incident tets of v_origin    
      std::vector<EntityHandle> ngb_cids(nfpc);
      error =mb->tag_get_data(sibhfs_cid,&cell_id,1,&ngb_cids[0]);
      if (MB_SUCCESS != error) return error;
      
      int nhf_thisv = lConnMap3D[index].v2hf_num[lvid];
      
      for (int i = 0; i < nhf_thisv; i++){
	int ind = lConnMap3D[index].v2hf[lvid][i];
	EntityHandle ngb = ngb_cids[ind];
	
	if (ngb != 0){
	  bool ngbtag = ctags[ngb-start_cell];
	  if  (ngbtag == false){
	    cellq.push(ngb);
	  }
	}
      }
    }
    
    return found;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool HalfFacetRep::find_matching_halfface(EntityHandle fid, EntityHandle *cid, int *lid)
  {
    ErrorCode error;
    EntityHandle start_cell = *_cells.begin();
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;
    int nvF = local_maps_2d(fid);

    std::vector<EntityHandle> fid_verts(nvF);
    error = mb->get_connectivity(&fid, 1, fid_verts);
    if (MB_SUCCESS != error) return error;

    EntityHandle cur_cid = 0;
    error = mb->tag_get_data(v2hf_cid, &fid_verts[0], 1, &cur_cid);
    if (MB_SUCCESS != error) return error;

    bool found = false;

    if (cur_cid != 0){
        EntityHandle Stkcells[100], trackcells[100];
        int Stksize = 0, count = -1;
        Stkcells[0] = cur_cid;

        /*std::stack<EntityHandle> Stkcells;
        std::vector<EntityHandle> trackcells;
        Stkcells.push(cur_cid);*/

        while (Stksize >= 0 ){
      //  while (!Stkcells.empty()){
            cur_cid = Stkcells[Stksize];
            Stksize -= 1 ;
            ctags[cur_cid - start_cell] = true;
            count += 1;
            trackcells[count] = cur_cid;

            std::cout<<"stksize = "<<Stksize<<std::endl;
            /*cur_cid = Stkcells.top();
            Stkcells.pop();
            ctags[cur_cid-start_cell] = true;
            trackcells.push_back(cur_cid);*/

            std::vector<EntityHandle> conn;
            error = mb->get_connectivity(&cur_cid, 1, conn);
            if (MB_SUCCESS != error) return error;

            // Search each half-face to match input face
            for(int i = 0; i < nfpc; ++i){
                int nv_curF = lConnMap3D[index].hf2v_num[i];
                if (nv_curF != nvF)
                  continue;

                // Connectivity of the current half-face
                std::vector<EntityHandle> vthisface(nvF);
                for(int l = 0; l < nvF; ++l){
                    int ind = lConnMap3D[index].hf2v[i][l];
                    vthisface[l] = conn[ind];
                  };

                // Match this half-face with input fid
                int direct,offset;
                bool they_match = CN::ConnectivityMatch(&vthisface[0],&fid_verts[0],nvF,direct,offset);

                if (they_match){
                    found = true;
                    cid[0] = cur_cid;
                    lid[0] = i;

                    for (int c = 0; c <= count; ++c){
                        ctags[trackcells[c]-start_cell] = false;
                      }

                   /* for (std::vector<EntityHandle>::iterator it = trackcells.begin(); it != trackcells.end(); ++it){
                        ctags[*it - start_cell] = false;
                      }*/
                    return found;
                  }
              }

	    // Add other cells that are incident on fid_verts[0]
	    std::vector<EntityHandle> sib_cids(nfpc);    
	    error = mb->tag_get_data(sibhfs_cid, &cur_cid, 1, &sib_cids[0]);
	    if (MB_SUCCESS != error) return error;

	    // Local id of fid_verts[0] in the cell
	    int lv0;
	    for (int i = 0; i< nvpc; ++i){
		if (conn[i] == fid_verts[0])
		  {
		    lv0 = i;
		  }
	      };

	    int nhf_thisv = lConnMap3D[index].v2hf_num[lv0];

	    // Add new cells into the stack
	    EntityHandle ngb; bool ngbtag;
	    for (int i = 0; i < nhf_thisv; ++i){
		int ind = lConnMap3D[index].v2hf[lv0][i];
		ngb = sib_cids[ind];

		if (ngb) {
		    ngbtag = ctags[ngb-start_cell];

		    if  (!ngbtag){
			//Stkcells.push(ngb);
			Stksize += 1;
			Stkcells[Stksize] = ngb;
		      }
		  }
	      }
	  }
      }
    return found;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode HalfFacetRep::get_neighbor_adjacencies_3d( EntityHandle cid, bool add_inent, std::vector<EntityHandle> &adjents)
  {
    ErrorCode error;
    int index = get_index_from_type(cid);
    int nfpc = lConnMap3D[index].num_faces_in_cell;
    
    if (cid != 0 ){
        if (add_inent)
          adjents.push_back(cid);

      std::vector<EntityHandle> sibcids(nfpc);
      std::vector<int> siblfids(nfpc);
      error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sibcids[0]);
      if (MB_SUCCESS != error) return error;      
      error = mb->tag_get_data(sibhfs_lfid, &cid, 1, &siblfids[0]);
      if (MB_SUCCESS != error) return error;

      for (int lfid = 0; lfid < nfpc; ++lfid){      
	if (sibcids[lfid] != 0) 
	  adjents.push_back(sibcids[lfid]);
      }    
    }

    return MB_SUCCESS; 
  }

} // namespace moab

