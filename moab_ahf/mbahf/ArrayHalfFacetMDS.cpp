/**
 * MOAB, a Mesh-Oriented datABase, is a software component for creating,
 * storing and accessing finite element mesh data.
 * 
 * Copyright 2004 Sandia Corporation.  Under the terms of Contract
 * DE-AC04-94AL85000 with Sandia Coroporation, the U.S. Government
 * retains certain rights in this software.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 */

#ifdef WIN32
#pragma warning (disable : 4786)
#endif

#include "ArrayHalfFacetMDS.hpp"
#include <iostream>
#include <assert.h>
#include <time.h>
#include <vector>
#include <queue>
#include <stack>
#include "moab/Core.hpp"
#include "moab/Range.hpp"
#include "moab/CN.hpp"

namespace moab {

  /*  ErrorCode ArrayHalfFacetMDS::initialize(){

    ErrorCode error;

    //    Range verts, edges, faces, cells;
    error = mb->get_entities_by_dimension( 0, 0, verts);
    if (MB_SUCCESS != error) return error;

    error = mb->get_entities_by_dimension( 0, 1, edges);
    if (MB_SUCCESS != error) return error;

    error = mb->get_entities_by_dimension( 0, 2, faces); 
    if (MB_SUCCESS != error) return error;
    
    error = mb->get_entities_by_dimension( 0, 3, cells);
    if (MB_SUCCESS != error) return error;
    
    int nverts = verts.size(); 
    int nedges = edges.size();
    int nfaces = faces.size();
    int ncells = cells.size();

    MTYPE mesh_type = get_mesh_type(nverts, nedges, nfaces, ncells);
 
    if (mesh_type == CURVE){
      // Tag sibhvs_eid, sibhvs_lvid, v2hv_eid, v2hv_lvid; 
      EntityHandle sdefval[2] = {0,0};  int sval[2] = {0,0};
      EntityHandle idefval = 0; int ival = 0;
      
      error = mb->tag_get_handle("SIBHVS_EID", 2, MB_TYPE_HANDLE, sibhvs_eid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
      error = mb->tag_get_handle("SIBHVS_LVID", 2, MB_TYPE_INTEGER, sibhvs_lvid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
      error = mb->tag_get_handle("V2HV_EID", 1, MB_TYPE_HANDLE, v2hv_eid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
      error = mb->tag_get_handle("V2HV_LVID", 1, MB_TYPE_INTEGER, v2hv_lvid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);  
      
      std::cout << "Created the half-vert tags" << std::endl;

      error = determine_sibling_halfverts(verts, edges, sibhvs_eid, sibhvs_lvid);
      error = determine_incident_halfverts(edges, v2hv_eid, v2hv_lvid);
      
    }
    
    else if (mesh_type == SURFACE){
      int nepf = local_maps_2d(*faces.begin()); 
      //Tag sibhes_fid, sibhes_leid, v2he_fid, v2he_leid;
  
      EntityHandle * sdefval = new EntityHandle[nepf];
      int * sval = new int[nepf];
      EntityHandle  idefval = 0;
      int ival = 0;
      for (int i=0; i<nepf; i++){ 
	sdefval[i]=0; 
	sval[i]=0;
      }
  
      error = mb->tag_get_handle("SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
      error = mb->tag_get_handle("SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
      error = mb->tag_get_handle("V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
      error = mb->tag_get_handle("V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
      std::cout << "Created the half-edge tags" << std::endl;

      error = determine_sibling_halfedges(faces, sibhes_fid, sibhes_leid);
      error = determine_incident_halfedges(faces, sibhes_fid, v2he_fid, v2he_leid);
      
      
      bool *ftags = new bool[faces.size()];
      for (int i=0; i < (int)faces.size(); ++i){
	ftags[i] = false;
      };

      delete [] sdefval;
      delete [] sval;

    } 
    else if (mesh_type == VOLUME){

      int index = get_index_from_type(*cells.begin());
      int nfpc = lConnMap3D[index].num_faces_in_cell;

      //Tag sibhfs_cid, sibhfs_lfid, v2hf_cid, v2hf_lfid;
      EntityHandle * sdefval = new EntityHandle[nfpc];
      int * sval = new int[nfpc];
      EntityHandle idefval = 0;	
      int ival = 0;
      for (int i = 0; i < nfpc; i++){
	sdefval[i] = 0;
	sval[i] = 0;
      }

      error = mb->tag_get_handle("SIBHFS_CID", nfpc, MB_TYPE_HANDLE, sibhfs_cid, MB_TAG_DENSE | MB_TAG_CREAT, sdefval);
      error = mb->tag_get_handle("SIBHFS_LFID", nfpc, MB_TYPE_INTEGER, sibhfs_lfid, MB_TAG_DENSE | MB_TAG_CREAT, sval);
      error = mb->tag_get_handle("V2HF_CID", 1, MB_TYPE_HANDLE, v2hf_cid, MB_TAG_DENSE | MB_TAG_CREAT, &idefval);  
      error = mb->tag_get_handle("V2HF_LFID", 1, MB_TYPE_INTEGER, v2hf_lfid, MB_TAG_DENSE | MB_TAG_CREAT, &ival);
      std::cout << "Created the half-face tags" << std::endl;

      error = determine_sibling_halffaces(cells, sibhfs_cid, sibhfs_lfid);
      error = determine_incident_halffaces(cells, sibhfs_cid, v2hf_cid, v2hf_lfid);
      
      
      bool * ctags = new bool[ncells];
      for (int i = 0; i<ncells; ++i){
	ctags[i] = false;
      };

      delete [] sdefval;
      delete [] sval;
    }
    
    else if (mesh_type == MIXED){
      
      int nepf = local_maps_2d(*faces.begin()); 
      int index = get_index_from_type(*cells.begin());
      int nfpc = lConnMap3D[index].num_faces_in_cell;
      
      //Tag sibhvs_eid, sibhes_fid, sibhfs_cid;
      //Tag sibhvs_lvid, sibhes_leid, sibhfs_lfid;
      //Tag v2hv_eid, v2he_fid, v2hf_cid;
      // Tag v2hv_lvid, v2he_leid, v2hf_lfid;
      
      EntityHandle sib_eid[2] = {0,0}, hv_eid = 0;
      int sib_lvid[2] = {0,0}, hv_lvid = 0;
      
      EntityHandle * sib_fid = new EntityHandle[nepf];
      int * sib_leid = new int[nepf]; 
      EntityHandle he_fid = 0; 
      int he_leid = 0;
      for (int i = 0; i < nepf; i++){
	sib_fid[i] = 0;
	sib_leid[i] = 0;
      }
      
      EntityHandle * sib_cid = new EntityHandle[nfpc];
      int * sib_lfid = new int[nfpc];
      EntityHandle hf_cid = 0;
      int hf_lfid = 0;
      for (int i = 0; i < nfpc; i++){
      sib_cid[i] = 0;
      sib_lfid[i] = 0;
      }
      
      
      error = mb->tag_get_handle("SIBHVS_EID", 2, MB_TYPE_HANDLE, sibhvs_eid, MB_TAG_DENSE | MB_TAG_CREAT, sib_eid);
      error = mb->tag_get_handle("SIBHVS_LVID", 2, MB_TYPE_INTEGER, sibhvs_lvid, MB_TAG_DENSE | MB_TAG_CREAT, sib_lvid);
      error = mb->tag_get_handle("SIBHES_FID", nepf, MB_TYPE_HANDLE, sibhes_fid, MB_TAG_DENSE | MB_TAG_CREAT, sib_fid);
      error = mb->tag_get_handle("SIBHES_LEID", nepf, MB_TYPE_INTEGER, sibhes_leid, MB_TAG_DENSE | MB_TAG_CREAT, sib_leid);
      error = mb->tag_get_handle("SIBHFS_CID", nfpc, MB_TYPE_HANDLE, sibhfs_cid, MB_TAG_DENSE | MB_TAG_CREAT, sib_cid);
      error = mb->tag_get_handle("SIBHFS_LFID", nfpc, MB_TYPE_INTEGER, sibhfs_lfid, MB_TAG_DENSE | MB_TAG_CREAT, sib_lfid);
      
      error = mb->tag_get_handle("V2HV_EID", 1, MB_TYPE_HANDLE, v2hv_eid, MB_TAG_DENSE | MB_TAG_CREAT, &hv_eid);  
      error = mb->tag_get_handle("V2HV_LVID", 1, MB_TYPE_INTEGER, v2hv_lvid, MB_TAG_DENSE | MB_TAG_CREAT, &hv_lvid);
      error = mb->tag_get_handle("V2HE_FID", 1, MB_TYPE_HANDLE, v2he_fid, MB_TAG_DENSE | MB_TAG_CREAT, &he_fid);  
      error = mb->tag_get_handle("V2HE_LEID", 1, MB_TYPE_INTEGER, v2he_leid, MB_TAG_DENSE | MB_TAG_CREAT, &he_leid);
      error = mb->tag_get_handle("V2HF_CID", 1, MB_TYPE_HANDLE, v2hf_cid, MB_TAG_DENSE | MB_TAG_CREAT, &hf_cid);  
      error = mb->tag_get_handle("V2HF_LFID", 1, MB_TYPE_INTEGER, v2hf_lfid, MB_TAG_DENSE | MB_TAG_CREAT, &hf_lfid);
      
      std::cout << "Created the tags for 1D (half-verts), 2D (half-edges) and 3D (half-faces)" << std::endl;
      
      error = determine_sibling_halfverts(verts, edges, sibhvs_eid, sibhvs_lvid);
      error = determine_incident_halfverts(edges, v2hv_eid, v2hv_lvid);
      error = determine_sibling_halfedges(faces, sibhes_fid, sibhes_leid);
      error = determine_incident_halfedges(faces, sibhes_fid, v2he_fid, v2he_leid);
      error = determine_sibling_halffaces(cells, sibhfs_cid, sibhfs_lfid);
      error = determine_incident_halffaces(cells, sibhfs_cid, v2hf_cid, v2hf_lfid);
      
      bool * ftags = new bool[nfaces];
      for (int i=0; i < nfaces; ++i){
	ftags[i] = false;
      };
      
      bool * ctags = new bool[ncells];
      for (int i = 0; i<ncells; ++i){
	ctags[i] = false;
      };

      delete [] sib_fid;
      delete [] sib_leid;
      delete [] sib_cid;
      delete [] sib_lfid;
      
    }
  }
  */

  /*******************************************************
   * deinitialize                                        *
   ******************************************************/
  /* ErrorCode ArrayHalfFacetMDS::deinitialize(){
    ErrorCode error;
    MTYPE mesh_type;
    error = get_mesh_type(verts.size(), edges.size(), faces.size(), cells.size(), mesh_type);    

    if (mesh_type == CURVE){
      error = mb->tag_delete(sibhvs_eid);
      error = mb->tag_delete(sibhvs_lvid);
      error = mb->tag_delete(v2hv_eid);
      error = mb->tag_delete(v2hv_lvid);
    }
    
    else if (mesh_type == SURFACE){
      error = mb->tag_delete(sibhes_fid);
      error = mb->tag_delete(sibhes_leid);
      error = mb->tag_delete(v2he_fid);
      error = mb->tag_delete(v2he_leid);
      delete [] sdefval;
      delete [] sval;
      delete [] ftags;
    }

    else if (mesh_type == VOLUME){
      error = mb->tag_delete(sibhfs_cid);
      error = mb->tag_delete(sibhfs_lfid);
      error = mb->tag_delete(v2hf_cid);
      error = mb->tag_delete(v2hf_lfid);
      delete [] sdefval;
      delete [] sval;
      delete [] ctags;
    }

    else if (mesh_type == MIXED){
      error = mb->tag_delete(sibhvs_eid);
      error = mb->tag_delete(sibhvs_lvid);
      error = mb->tag_delete(v2hv_eid);
      error = mb->tag_delete(v2hv_lvid);
      
      error = mb->tag_delete(sibhes_fid);
      error = mb->tag_delete(sibhes_leid);
      error = mb->tag_delete(v2he_fid);
      error = mb->tag_delete(v2he_leid);
      
      error = mb->tag_delete(sibhfs_cid);
      error = mb->tag_delete(sibhfs_lfid);
      error = mb->tag_delete(v2hf_cid);
      error = mb->tag_delete(v2hf_lfid);

      delete [] sib_fid;
      delete [] sib_leid;
      delete [] sib_cid;
      delete [] sib_lfid;
      delete [] ftags;
      delete [] ctags;
    }
  };
*/
  /******************************************************
  *                                *
  *******************************************************/
  MTYPE ArrayHalfFacetMDS::get_mesh_type(int nverts, int nedges, int nfaces, int ncells)
  {
    MTYPE mesh_type;
 
    if (nverts && nedges && (!nfaces) && (!ncells))
      mesh_type = CURVE; 
    else if (nverts && !nedges && nfaces && !ncells)
      mesh_type = SURFACE;
    else if (nverts && !nedges && !nfaces && ncells)
      mesh_type = VOLUME;
    else if (nverts && nedges && nfaces && ncells)
      mesh_type = MIXED;

    return mesh_type;
  }

  /******************************************************** 
  * 1D: sibhvs, v2hv, incident and neighborhood queries   *
  *********************************************************/
  ErrorCode ArrayHalfFacetMDS::determine_sibling_halfverts( Range &verts,  Range &edges, Tag sibhvs_eid, Tag sibhvs_lvid)
  {
    ErrorCode error;
    EntityHandle start_edge = *edges.begin();

    // Create pointers to the tags for direct access to memory
    int count, *sibhvs_lvid_ptr; EntityHandle *sibhvs_eid_ptr;
    error = mb->tag_iterate(sibhvs_eid, edges.begin(), edges.end(), count, (void*&)sibhvs_eid_ptr);
    if (MB_SUCCESS != error) return error;
    assert(count == (int) edges.size());
    error = mb->tag_iterate(sibhvs_lvid, edges.begin(), edges.end(), count, (void*&)sibhvs_lvid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) edges.size());
 
    std::vector<EntityHandle> conn(2), adj;   
    
    // Main Loop over all vertices 
    for (Range::iterator v_it = verts.begin(); v_it != verts.end(); ++v_it) {
      EntityHandle v = *v_it;  
      
      adj.clear();
      error = mb->get_adjacencies(&*v_it, 1, 1, false, adj); 
      if (MB_SUCCESS != error) return error;
   
      if (adj.size()>1){
	 error = mb->get_connectivity(&adj[0], 1, conn); 
	 if (MB_SUCCESS != error) return error;

	 EntityHandle first_eid = adj[0];
	 EntityHandle prev_eid = adj[0];
	 int first_lvid = 1;
	 if (conn[0]==v)
	   first_lvid = 0;      
	 int prev_lvid = first_lvid;
	 
	 for(int j=1; j<(int)adj.size();++j){	   
	   sibhvs_eid_ptr[2*(prev_eid-start_edge)+prev_lvid] = adj[j];

	   error = mb->get_connectivity(&adj[j], 1, conn); 
	   if (MB_SUCCESS != error) return error;
	   assert(conn.size()==2);	  

	   if (conn[0]==v){	     	     
	     sibhvs_lvid_ptr[2*(prev_eid-start_edge)+prev_lvid] = 0;
	     prev_lvid = 0;
	   }
	   else	{
	     sibhvs_lvid_ptr[2*(prev_eid-start_edge)+prev_lvid] = 1;
	     prev_lvid = 1;  	   
	   }
	   prev_eid = adj[j];
	 }

	 if (prev_eid != first_eid){	   
	   sibhvs_eid_ptr[2*(prev_eid-start_edge) + prev_lvid] = first_eid;	   
	   sibhvs_lvid_ptr[2*(prev_eid-start_edge) + prev_lvid] = first_lvid;	   
	 }	 
      }
    }  
    std::cout<<"Finished creating sibling half-verts map for curve mesh"<<std::endl;

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::determine_incident_halfverts(  Range &edges, Tag v2hv_eid, Tag v2hv_lvid){
    ErrorCode error;
    std::vector<EntityHandle> conn(2);

    for (Range::iterator e_it = edges.begin(); e_it != edges.end(); ++e_it){      
      conn.clear();
      error = mb->get_connectivity(&*e_it, 1, conn); assert(conn.size()==2); 
      if (MB_SUCCESS != error) return error;

      for(int i=0; i<2; ++i){
	EntityHandle v = conn[i], eid = 0;	 
	error = mb->tag_get_data(v2hv_eid, &v, 1, &eid); 
	if (MB_SUCCESS != error) return error;

	if (eid==0){
	  EntityHandle edg = *e_it;	  
	  int lvid = i;
	  error = mb->tag_set_data(v2hv_eid, &v, 1, &edg); 
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_set_data(v2hv_lvid, &v, 1, &lvid);
	  if (MB_SUCCESS != error) return error;
	}
      }      
    }  
    std::cout<<"Finished creating incident half-verts"<<std::endl;

    return MB_SUCCESS;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::get_upward_incidences_1d( EntityHandle vid, Tag sibhvs_eid, Tag sibhvs_lvid, Tag v2hv_eid, Tag v2hv_lvid, std::vector< EntityHandle > &adj_entities){
    ErrorCode error;     
    EntityHandle start_eid, eid, sibeid[2];
    int start_lid, lid, siblid[2];    
   
    error = mb->tag_get_data(v2hv_eid, &vid, 1, &start_eid);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(v2hv_lvid, &vid, 1, &start_lid);    
    if (MB_SUCCESS != error) return error;

    eid = start_eid; lid = start_lid;

    if (eid != 0){
      adj_entities.push_back(eid);       
      while (eid !=0) {	 
	  error = mb->tag_get_data(sibhvs_eid, &eid, 1, sibeid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhvs_lvid, &eid, 1, siblid);
	  if (MB_SUCCESS != error) return error;

	  eid = sibeid[lid];
	  lid = siblid[lid];
	  if ((!eid)||(eid == start_eid))
	    break;
	  adj_entities.push_back(eid);				 
      }
    }

    return MB_SUCCESS;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::get_neighbor_adjacencies_1d( EntityHandle eid, Tag sibhvs_eid, Tag sibhvs_lvid, std::vector<EntityHandle> &adj_entities){

    ErrorCode error;
    EntityHandle sib_eids[2], sibhv_eid;
    int sib_lids[2], sibhv_lid;    

    error = mb->tag_get_data(sibhvs_eid, &eid, 1, sib_eids);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(sibhvs_lvid, &eid, 1, sib_lids);
    if (MB_SUCCESS != error) return error;
    
    for (int lid = 0;lid < 2; ++lid){
      sibhv_eid = sib_eids[lid];
      sibhv_lid = sib_lids[lid];

      if (sibhv_eid != 0){
	adj_entities.push_back(sibhv_eid);	
	
	EntityHandle next_eid[2], hv_eid; 
	int next_lid[2], hv_lid;
	
	error = mb->tag_get_data(sibhvs_eid, &sibhv_eid, 1, next_eid);
	if (MB_SUCCESS != error) return error;
	error = mb->tag_get_data(sibhvs_lvid, &sibhv_eid, 1, next_lid);
	if (MB_SUCCESS != error) return error;
	
	hv_eid = next_eid[sibhv_lid];
	hv_lid = next_lid[sibhv_lid];
	
	while (hv_eid != 0){	    
	  if (hv_eid != eid)
	    adj_entities.push_back(hv_eid);
	  
	  error = mb->tag_get_data(sibhvs_eid, &hv_eid, 1, next_eid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhvs_lvid, &hv_eid, 1, next_lid);
	  if (MB_SUCCESS != error) return error;

	  if (next_eid[hv_lid] == sibhv_eid)
	    break;
	  hv_eid = next_eid[hv_lid];
	  hv_lid = next_lid[hv_lid];      
	}
      }
    } 

    return MB_SUCCESS;   
  }
  
  /*******************************************************
  * 2D: sibhes, v2he, incident and neighborhood queries  *
  ********************************************************/
  int ArrayHalfFacetMDS::local_maps_2d( EntityHandle face)
  {
      EntityType type = mb->type_from_handle(face);
      int nepf;
    if (type == MBTRI) nepf = 3; 
    else if (type ==MBQUAD) nepf = 4;
    
    return nepf;
  }
  /////////////////////
  ErrorCode ArrayHalfFacetMDS::local_maps_2d( EntityHandle face, int *next, int *prev)
  {
    // nepf: Number of edges per face
    // next: Local ids of next edges
    // prev: Local ids of prev edges
    EntityType type = mb->type_from_handle(face);
   
    if (type == MBTRI)
    {  
      next[0] = 1; next[1] = 2; next[2] = 0;
      prev[0] = 2; prev[1] = 0; prev[2] = 1;
    }
    else if (type ==MBQUAD)
    {
      next[0] = 1; next[1] = 2; next[2] = 3; next[3] = 0;
      prev[0] = 3; prev[1] = 0; prev[2] = 1; prev[3] = 2;
    }

    return MB_SUCCESS;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::determine_sibling_halfedges( Range &faces, Tag sibhes_fid, Tag sibhes_leid)
  {
    ErrorCode error;
    EntityHandle start_face = *faces.begin();  

    // Create pointers to the tags for direct access to memory    
    int count, *sibhes_leid_ptr; EntityHandle *sibhes_fid_ptr;
    error = mb->tag_iterate(sibhes_fid, faces.begin(), faces.end(), count, (void*&)sibhes_fid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) faces.size());
    error = mb->tag_iterate(sibhes_leid, faces.begin(), faces.end(), count, (void*&)sibhes_leid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) faces.size());

  
    int  id, kid, next[4]={1,2,3,0};
    bool hasthree = false;
    
    int nepf = local_maps_2d(start_face);
    if(nepf == 3) hasthree = true;
    
    std::vector<EntityHandle> conn(nepf);
   
    /* Main Loop over all faces */ 
    for (Range::iterator t = faces.begin(); t != faces.end(); ++t) {
      for (int k = 0; k < nepf; k++){
	if (sibhes_fid_ptr[nepf*(*t-start_face)+k] != 0)	  
	  continue;	       

	conn.clear(); 
	error = mb->get_connectivity(&*t, 1, conn); 
       	    
	std::vector<EntityHandle> curedg_verts(2), adj;

	kid = k + (hasthree & (k==2)); id = next[kid]; 
	curedg_verts[0] = conn[k]; curedg_verts[1] = conn[id];

	// Find all the incident faces on this half-edge
	error = mb->get_adjacencies(&curedg_verts[0], 2, 2, false, adj, Interface::INTERSECT);	
	
	//	std::cout<<"For fid = "<<*t<<", leid = "<<k<<" adj.size() = "<<adj.size()<<std::endl;

	if (adj.size() == 1)	  
	  continue;

	/* Step 1: Connect all half-edges corresponding to this edge*/
	EntityHandle first_fid = *t, prev_fid = *t;
	int first_leid = k, prev_leid = k;

	std::vector<EntityHandle> adj_conn(nepf);

	for (int j=0; j<(int)adj.size(); ++j){
	  if (adj[j] != *t){	    
	    adj_conn.clear();
	    error = mb->get_connectivity(&adj[j], 1, adj_conn); 

	    std::vector<EntityHandle> adjedg_verts(2);
	    for (int l=0; l<nepf; ++l){
	      kid = l + (hasthree & (l==2)); id=next[kid]; 
	      adjedg_verts[0] = adj_conn[l]; adjedg_verts[1] = adj_conn[id]; 

	      int direct,offset;	      
	      bool they_match = CN::ConnectivityMatch(&adjedg_verts[0],&curedg_verts[0],2,direct,offset);	     

	      if (they_match){
		sibhes_fid_ptr[nepf*(prev_fid-start_face)+prev_leid] = adj[j];
		sibhes_leid_ptr[nepf*(prev_fid-start_face)+prev_leid] = l;
		prev_fid = adj[j];
		prev_leid = l;
		break;
	      }
	    }
	  }
	}	   
	/* Step 2: Complete the cycle by connecting the first and last halfedges*/
	if (prev_fid != first_fid){
	  sibhes_fid_ptr[nepf*(prev_fid-start_face) + prev_leid] = first_fid;
	  sibhes_leid_ptr[nepf*(prev_fid-start_face) + prev_leid] = first_leid;
	}
      }
    }
    //    error = mb->remove_adjacencies
    std::cout<<"Finished ructing sibling halfedges map for a surface mesh"<<std::endl;

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::determine_incident_halfedges( Range &faces, Tag sibhes_fid, Tag v2he_fid, Tag v2he_leid)
  {
    ErrorCode error;    

    int nepf = local_maps_2d(*faces.begin());

    std::vector<EntityHandle> conn(nepf);
    std::vector<EntityHandle> sibfid(nepf);

    for (Range::iterator it = faces.begin(); it != faces.end(); ++it){      
      conn.clear();
      error = mb->get_connectivity(&*it, 1, conn); 
      if (MB_SUCCESS != error) return error;

      sibfid.clear();
      error = mb->tag_get_data(sibhes_fid, &*it, 1, &sibfid[0]);
      if (MB_SUCCESS != error) return error;

      for(int i=0; i<nepf; ++i){
	EntityHandle v = conn[i],  vfid = 0;
	error = mb->tag_get_data(v2he_fid, &v, 1, &vfid);
	if (MB_SUCCESS != error) return error;

	if ((vfid==0)||((vfid!=0) && (sibfid[i]==0))){
	  EntityHandle fid = *it;	  
	  int lid = i;
	  error = mb->tag_set_data(v2he_fid, &v, 1, &fid);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_set_data(v2he_leid, &v, 1, &lid);
	  if (MB_SUCCESS != error) return error;
	}
      }      
    }     
    std::cout<<"Finished creating incident half-edges map"<<std::endl;
    
    return MB_SUCCESS;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_2d( EntityHandle fid,  int leid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &adj_entities)
  {
    // Given an implicit half-edge <fid, leid>, find the incident faces.
    // If "bool add_iface" is true, the input face fid is included in the adj_entities list, otherwise, no.  
    ErrorCode error;

    int nepf = local_maps_2d(fid);
    if (fid == 0)
      return MB_SUCCESS;

    if (add_iface)
      adj_entities.push_back(fid);

    std::vector<EntityHandle> sib_fids(nepf);
    std::vector<int> sib_lids(nepf);
    EntityHandle curfid = fid;
    int curlid = leid;

    do {
      sib_fids.clear();
      sib_lids.clear();
      error = mb->tag_get_data(sibhes_fid, &curfid, 1, &sib_fids[0]);
      if (MB_SUCCESS != error) return error;
      error = mb->tag_get_data(sibhes_leid, &curfid, 1, &sib_lids[0]);
      if (MB_SUCCESS != error) return error;

      curfid = sib_fids[curlid];
      curlid = sib_lids[curlid];
      
      if(curfid == 0)
	break;

      adj_entities.push_back(curfid);

    }while (curfid != fid);

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_2d( EntityHandle fid,  int leid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &fids_1ring, std::vector<int> &leids_1ring)
  {
    // Given an implicit half-edge <fid, leid>, find the incident half-edges.
    ErrorCode error;
    int nepf = local_maps_2d(fid);

    if (fid == 0)
      return MB_SUCCESS;

    if (add_iface)
      {
	fids_1ring.push_back(fid);
	leids_1ring.push_back(leid);
      }
    
    std::vector<EntityHandle> sib_fids(nepf);
    std::vector<int> sib_lids(nepf);
    EntityHandle curfid = fid;
    int curlid = leid;
    
    do {
      sib_fids.clear();
      sib_lids.clear();
      error = mb->tag_get_data(sibhes_fid, &curfid, 1, &sib_fids[0]);
      if (MB_SUCCESS != error) return error;
      error = mb->tag_get_data(sibhes_leid, &curfid, 1, &sib_lids[0]);
      if (MB_SUCCESS != error) return error;
      
      curfid = sib_fids[curlid];
      curlid = sib_lids[curlid];

      if((curfid == 0)||(curfid == fid))
	break;

      fids_1ring.push_back(curfid);
      leids_1ring.push_back(curlid);
      
    }while (curfid != fid);

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_2d( EntityHandle fid,  int lid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, bool *ftags)
  {
    //Given an implicit half-edge <he_fid, he_lid>, add the incident half-edges to an input queue, if the half-edge doesn't exist in the queue. 

    ErrorCode error; 
    int nepf = local_maps_2d(fid);
    
    std::vector<EntityHandle> sib_fids(nepf);
    std::vector<int> sib_lids(nepf);
    EntityHandle curfid = fid;
    int curlid = lid;

    do {
      sib_fids.clear();
      sib_lids.clear();
      error = mb->tag_get_data(sibhes_fid, &curfid, 1, &sib_fids[0]);
      if (MB_SUCCESS != error) return error;
      error = mb->tag_get_data(sibhes_leid, &curfid, 1, &sib_lids[0]);
      if (MB_SUCCESS != error) return error;

      curfid = sib_fids[curlid];
      curlid = sib_lids[curlid];

      if(curfid == 0)
	break;

      if (!(ftags[curfid-*faces.begin()])){	
	queue_fid.push(curfid);
	queue_lid.push(curlid);
	}
      
    }while (curfid != fid);
    
    return MB_SUCCESS;
  } 

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::get_upward_incidences_2d( EntityHandle eid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, std::vector<EntityHandle> &adj_entities)
{

  // Given an explicit edge eid, find the incident faces. 

    ErrorCode error;    
    EntityHandle he_fid=0; int he_lid=0;

    // Step 1: Given an explicit edge, find a corresponding half-edge from the surface mesh.
    error = find_matching_halfedge(eid, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, &he_fid, &he_lid);
    if (MB_SUCCESS != error) return error;   
    
    // Step 2: If there is a corresponding half-edge, collect all sibling half-edges and store the incident faces. 
    error = get_upward_incidences_2d(he_fid, he_lid, sibhes_fid, sibhes_leid, true, adj_entities);  
    if (MB_SUCCESS != error) return error; 

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_2d( EntityHandle eid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, std::vector<EntityHandle> &fids_1ring, std::vector<int> &leids_1ring)
  {
    //Given an explicit edge, find the incident half-edges
    ErrorCode error;
    EntityHandle he_fid = 0; int he_lid = 0; 
    error = find_matching_halfedge(eid, faces, sibhes_fid, sibhes_leid, v2he_fid, v2he_leid, ftags, &he_fid, &he_lid);
    if (MB_SUCCESS != error) return error;

    error = get_upward_incidences_2d(he_fid, he_lid, sibhes_fid, sibhes_leid, true, fids_1ring, leids_1ring); 
    if (MB_SUCCESS != error) return error;

    return MB_SUCCESS;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::find_matching_halfedge( EntityHandle eid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, Tag v2he_fid, Tag v2he_leid, bool *ftags, EntityHandle *hefid, int *helid){
    ErrorCode error;

    std::vector<EntityHandle> conn(2);
    error = mb->get_connectivity(&eid, 1, conn);
    if (MB_SUCCESS != error) return error;

    EntityHandle fid; int lid;
    error = mb->tag_get_data(v2he_fid, &conn[0], 1, &fid);
    if (MB_SUCCESS != error) return error;
    error = mb->tag_get_data(v2he_leid, &conn[0], 1, &lid);
    if (MB_SUCCESS != error) return error;

    if (fid!=0){
      std::queue<EntityHandle> queue_fid;
      std::queue<int> queue_lid;
      std::vector<EntityHandle> trackfaces;

      EntityHandle vid = conn[0];

      error = gather_halfedges(vid, fid, lid, faces, sibhes_fid, sibhes_leid, ftags, queue_fid, queue_lid, trackfaces);
      if (MB_SUCCESS != error) return error;

      error = collect_and_compare(conn, faces, sibhes_fid, sibhes_leid, ftags, queue_fid, queue_lid, trackfaces, hefid, helid);      
      if (MB_SUCCESS != error) return error;

      for (std::vector<EntityHandle>::iterator it = trackfaces.begin(); it != trackfaces.end(); ++it){
	ftags[*it - (*faces.begin())] = false;
      }
    }    
    
    return MB_SUCCESS;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::gather_halfedges( EntityHandle vid,  EntityHandle he_fid,  int he_lid,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, bool *ftags, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces)
  {  
    ErrorCode error;
    EntityHandle he2_fid = 0; int he2_lid = 0;

    error = another_halfedge(vid, he_fid, he_lid, &he2_fid, &he2_lid);
    if (MB_SUCCESS != error) return error;

    queue_fid.push(he_fid); queue_lid.push(he_lid);
    queue_fid.push(he2_fid); queue_lid.push(he2_lid);
    trackfaces.push_back(he_fid);
    ftags[he_fid-*faces.begin()] = true;
    
    error = get_upward_incidences_2d(he_fid, he_lid, faces, sibhes_fid, sibhes_leid, queue_fid, queue_lid, ftags);
    if (MB_SUCCESS != error) return error;
    error = get_upward_incidences_2d(he2_fid, he2_lid, faces, sibhes_fid, sibhes_leid, queue_fid, queue_lid, ftags);    
    if (MB_SUCCESS != error) return error;

    return MB_SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  ErrorCode ArrayHalfFacetMDS::another_halfedge( EntityHandle vid,  EntityHandle he_fid,  int he_lid, EntityHandle *he2_fid, int *he2_lid)
  {    
    ErrorCode error;
    int nepf = local_maps_2d(he_fid);
    int * next = new int[nepf];
    int * prev = new int[nepf];
    error = local_maps_2d(he_fid, next, prev);
    if (MB_SUCCESS != error) return error;
    
    std::vector<EntityHandle> conn(nepf);
    error = mb->get_connectivity(&he_fid, 1, conn);
    if (MB_SUCCESS != error) return error;

    *he2_fid = he_fid;
    if (conn[he_lid] == vid)
      *he2_lid = prev[he_lid];
    else
      *he2_lid = next[he_lid];

    delete [] next;
    delete [] prev;
    return MB_SUCCESS;
  }
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::collect_and_compare(std::vector<EntityHandle> &edg_vert,  Range &faces, Tag sibhes_fid, Tag sibhes_leid, bool *ftags, std::queue<EntityHandle> &queue_fid, std::queue<int> &queue_lid, std::vector<EntityHandle> &trackfaces, EntityHandle *he_fid, int *he_lid)
  {
    ErrorCode error;
    int nepf = local_maps_2d(*he_fid); 
    int * next = new int[nepf];
    int * prev = new int[nepf];
    error = local_maps_2d(*he_fid, next, prev);
    if (MB_SUCCESS != error) return error;

    while (!queue_fid.empty())
      {
	EntityHandle curfid = queue_fid.front();
	int curlid = queue_lid.front();
	queue_fid.pop();
	queue_lid.pop();

	std::vector<EntityHandle> conn(nepf);
	error = mb->get_connectivity(&curfid, 1, conn);
	if (MB_SUCCESS != error) return error;

	int id = next[curlid];
	if (((conn[curlid]==edg_vert[0])&&(conn[id]==edg_vert[1]))||((conn[curlid]==edg_vert[1])&&(conn[id]==edg_vert[0]))){
	  *he_fid = curfid;
	  *he_lid = curlid;
	  break;
	}

	if (ftags[curfid-*faces.begin()])
	  continue;
	ftags[curfid-*faces.begin()] = true;
	trackfaces.push_back(curfid);

	EntityHandle he2_fid; int he2_lid;
	error = another_halfedge(edg_vert[0], curfid, curlid, &he2_fid, &he2_lid);
	if (MB_SUCCESS != error) return error;
	error = get_upward_incidences_2d(he2_fid, he2_lid, faces, sibhes_fid, sibhes_leid, queue_fid, queue_lid, ftags);
	if (MB_SUCCESS != error) return error;
       
      }
    
    delete [] next; 
    delete [] prev;
    return MB_SUCCESS;
  }
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::get_neighbor_adjacencies_2d( EntityHandle fid, Tag sibhes_fid, Tag sibhes_leid, bool add_iface, std::vector<EntityHandle> &adj_entities)
  {
    ErrorCode error; 
    if (fid != 0){
      if (add_iface)
	adj_entities.push_back(fid);
      
      int nepf = local_maps_2d(fid);
      for (int lid = 0; lid < nepf; ++lid){
	error = get_upward_incidences_2d(fid, lid, sibhes_fid, sibhes_leid, false, adj_entities);
	if (MB_SUCCESS != error) return error;
      }
    }
    
    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  int ArrayHalfFacetMDS::find_total_edges_2d(Range &faces, Tag sibhes_fid, Tag sibhes_leid)
  {
    ErrorCode error;
    EntityHandle firstF = *faces.begin();
    int nepf = local_maps_2d(firstF);
    int nfaces = faces.size();

    int total_edges = nepf*nfaces;
    
    bool *trackF = new bool(total_edges);
    for (int i = 0; i< total_edges; i++)
            trackF[i] = false;

    std::vector<EntityHandle> adj_fids;
    std::vector<int> adj_lids;

    for (Range::iterator f = faces.begin(); f != faces.end(); f++){
      for (int l = 0; l < nepf; ++l){

	adj_fids.clear();
	adj_lids.clear();

	int id = nepf*(*f-firstF)+l;
	if (!trackF[id])
	  {
	    error = get_upward_incidences_2d(*f,l, sibhes_fid, sibhes_leid, false, adj_fids, adj_lids);
	    if (MB_SUCCESS != error) return error;

	    //std::cout<<adj_fids.size()<<std::endl;

	    total_edges -= adj_fids.size();

	    for (int i = 0; i < (int)adj_fids.size(); i++)
	      trackF[nepf*(adj_fids[i]-firstF)+adj_lids[i]] = true;
	  };
      };
   };
    
    delete [] trackF;        
    return total_edges;
  }

  /*******************************************************
  * 3D: sibhfs, v2hf, incident and neighborhood queries  *
  ********************************************************/

  int ArrayHalfFacetMDS::get_index_from_type(EntityHandle cid)
  {
    int index;
    EntityType type = mb->type_from_handle(cid);
    if (type == MBTET)
      index = 0;
    else if (type == MBPYRAMID)
      index = 1;
    else if (type == MBPRISM)
      index = 2;
    else if (type == MBHEX)
      index = 3;

    return index;   
  }

  /////////////////////////////////////////////
   ArrayHalfFacetMDS::LocalMaps3D ArrayHalfFacetMDS::lConnMap3D[4] = 
    {
      // Tet
      {4, 6, 4, {3,3,3,3}, {{0,1,3},{1,2,3},{2,0,3},{0,2,1}},   {3,3,3,3},   {{0,2,3},{0,1,3},{1,2,3},{0,1,2}},   {{0,1},{1,2},{2,0},{0,3},{1,3},{2,3}},   {{3,0},{3,1},{3,2},{0,2},{0,1},{1,2}},   {{0,4,3},{1,5,4},{2,3,5},{2,1,0}},     {{-1,0,2,3},{0,-1,1,4},{2,1,-1,5},{3,4,5,-1}}},

      // Pyramid
      {5, 8, 5, {4,3,3,3,3}, {{0,3,2,1},{0,1,4},{1,2,4},{2,3,4},{3,0,4}},  {3,3,3,3,4},   {{0,1,4},{0,1,2},{0,2,3},{0,3,4},{1,2,3,4}},   {{0,1},{1,2},{2,3},{3,0},{0,4},{1,4},{2,4},{3,4}},   {{0,1},{0,2},{0,3},{0,4},{1,4},{1,2},{2,3},{3,4}},    {{3,2,1,0},{0,5,4},{1,6,5},{2,7,6},{3,4,7}},    {{-1,0,-1,3,4},{0,-1,1,-1,5},{-1,1,-1,2,6},{3,-1,2,-1,7},{4,5,6,7,-1}}},

      // Prism
      {6, 9, 5, {4,4,4,3,3}, {{0,1,4,3},{1,2,5,4},{0,3,5,2},{0,2,1},{3,4,5}},  {3,3,3,3,3}, {{0,2,3},{0,1,3},{1,2,3},{0,2,4},{0,1,4}},    {{0,1},{1,2},{2,0},{0,3},{1,4},{2,5},{3,4},{4,5},{5,3}},    {{0,3},{1,3},{2,3},{0,2},{0,1},{1,2},{0,4},{1,4},{2,4}},     {{4,6,3,0},{1,5,7,4},{3,8,5,2},{0,2,1},{6,7,8}},    {{-1,0,2,3,-1,-1},{0,-1,1,-1,4,-1},{2,1,-1,-1,-1,5},{3,-1,-1,-1,6,8},{-1,4,-1,6,-1,7},{-1,-1,5,8,7,-1}}},

      // Hex
      {8, 12, 6, {4,4,4,4,4,4}, {{0,1,5,4},{1,2,6,5},{2,3,7,6},{0,4,7,3},{0,3,2,1},{4,5,6,7}},   {3,3,3,3,3,3,3,3},   {{0,3,4},{0,1,4},{1,2,4},{2,3,4},{0,3,5},{0,1,5},{1,2,5},{2,3,5}},    {{0,1},{1,2},{2,3},{3,0},{0,4},{1,5},{2,6},{3,7},{4,5},{5,6},{6,7},{7,4}},     {{0,4},{1,4},{2,4},{3,4},{0,3},{0,1},{1,2},{2,3},{0,5},{1,5},{2,5},{3,5}},     {{0,5,8,4},{1,6,9,5},{2,7,10,6},{4,11,7,3},{3,2,1,0},{8,9,10,11}},     {{-1,0,-1,-1,4,-1,-1,-1},{0,-1,1,-1,-1,5,-1,-1},{-1,1,-1,2,-1,-1,6,-1},{-1,-1,2,-1,-1,-1,-1,7},{4,-1,-1,-1,-1,8,-1,11},{-1,5,-1,-1,8,-1,9,-1},{-1,-1,6,-1,-1,9,-1,10},{-1,-1,-1,7,11,-1,10,-1}}}
      
    };

  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::determine_sibling_halffaces( Range &cells, Tag sibhfs_cid, Tag sibhfs_lfid)
  {
    ErrorCode error;
    EntityHandle start_cell = *cells.begin();
  
    int count, *sibhfs_lfid_ptr; 
    EntityHandle *sibhfs_cid_ptr;
    error = mb->tag_iterate(sibhfs_cid, cells.begin(), cells.end(), count, (void*&)sibhfs_cid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) cells.size());
    error = mb->tag_iterate(sibhfs_lfid, cells.begin(), cells.end(), count, (void*&)sibhfs_lfid_ptr); 
    if (MB_SUCCESS != error) return error;
    assert(count == (int) cells.size());

    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;    
    std::cout<<"index = "<<index<<" nvpc = "<<nvpc<<", nfpc = "<<nfpc<<std::endl;

    std::vector<EntityHandle> conn(nvpc);
    /* Main Loop over all half-faces of the entire mesh */
    for (Range::iterator t = cells.begin(); t != cells.end(); ++t) {
      conn.clear();
      error = mb->get_connectivity(&*t, 1, conn); 
      if (MB_SUCCESS != error) return error;

      for (int c=0; c<(int)conn.size(); c++)
	std::cout<<"conn["<<c<<"] = "<<conn[c]<<" ";
      std::cout<<std::endl;

      for (int k = 0; k < nfpc; k++){
	if (sibhfs_cid_ptr[nfpc*(*t-start_cell)+k] != 0)
	  continue;

	int nv_curF = lConnMap3D[index].hf2v_num[k];
	std::cout<<" Cur_Face = "<<k<<", nv_curF = "<<nv_curF<<" : ";

	std::vector<EntityHandle> face_verts(nv_curF);
	for (int j=0; j < nv_curF; j++){
	  int ind = lConnMap3D[index].hf2v[k][j];
	  face_verts[j]=conn[ind];
	  std::cout<<"face_verts["<<j<<"] = "<<face_verts[j]<< " : ";
	};	
	
	std::vector<EntityHandle> adj;
	error = mb->get_adjacencies(&face_verts[0], nv_curF, 3, false, adj, Interface::INTERSECT);
	if (MB_SUCCESS != error) return error;

	std::cout<<" adj.size() = "<<adj.size();
	std::cout<<std::endl;
	if (adj.size() == 1)   
	  continue;
	
	EntityHandle first_cid = *t, prev_cid = *t;
	int first_hfid = k, prev_hfid = k;       

	/* Step 1: Connect half-faces sharing this face*/
	for (int j=0; j<(int)adj.size(); ++j){	  
	  if (adj[j] != *t){
	    std::vector<EntityHandle> adj_conn(nvpc);	    
	    error = mb->get_connectivity(&adj[j], 1, adj_conn); 
	    if (MB_SUCCESS != error) return error;

	    for (int l=0; l < nfpc; ++l){
	      int nv_adjF = lConnMap3D[index].hf2v_num[l];
	      if (nv_adjF != nv_curF)
		continue;

	      std::vector<EntityHandle> adj_verts(nv_adjF);
	      for(int i = 0; i < nv_adjF; i++){
		int ind = lConnMap3D[index].hf2v[l][i];
		adj_verts[i] = adj_conn[ind];
	      }
	      
	      int direct,offset;     	     
	      bool they_match = CN::ConnectivityMatch(&adj_verts[0],&face_verts[0],nv_adjF,direct,offset);	     
	      if (they_match){
		sibhfs_cid_ptr[nfpc*(prev_cid-start_cell)+prev_hfid] = adj[j];
		sibhfs_lfid_ptr[nfpc*(prev_cid-start_cell)+prev_hfid] = l;
		prev_cid = adj[j]; prev_hfid = l;
		break;
	      }
	    }
	  }
	}

	/* Step 2: Complete the cycle by connecting the first and last half-faces*/
	if (prev_cid != first_cid){
	  sibhfs_cid_ptr[nfpc*(prev_cid-start_cell) + prev_hfid] = first_cid;
	  sibhfs_lfid_ptr[nfpc*(prev_cid-start_cell) + prev_hfid] = first_hfid;
	}
      }
    }
    std::cout<<"Finished ructing sibling halffaces map for volume mesh"<<std::endl;  

    return MB_SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::determine_incident_halffaces( Range &cells, Tag sibhfs_cid, Tag v2hf_cid, Tag v2hf_lfid)
  {
    ErrorCode error;

    int index = get_index_from_type(*cells.begin());
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;
  
    std::vector<EntityHandle> conn(nvpc);
    std::vector<EntityHandle> sib_cids(nfpc);

    for (Range::iterator t_it = cells.begin(); t_it != cells.end(); ++t_it){      
      EntityHandle tet = *t_it;
      conn.clear();
      error = mb->get_connectivity(&tet, 1, conn);     
      if (MB_SUCCESS != error) return error;

      sib_cids.clear(); 
      error = mb->tag_get_data(sibhfs_cid, &tet, 1, &sib_cids[0]);
      if (MB_SUCCESS != error) return error;
      
      for(int i=0; i<nvpc; ++i){
	EntityHandle v = conn[i], vcid;	
	error = mb->tag_get_data(v2hf_cid, &v, 1, &vcid);
	if (MB_SUCCESS != error) return error;

	int nhf_pv = lConnMap3D[index].v2hf_num[i];

	for (int j=0; j < nhf_pv; ++j){
	  int ind = lConnMap3D[index].v2hf[i][j];
	  if (vcid==0){
	    error = mb->tag_set_data(v2hf_cid, &v, 1, &tet);
	    if (MB_SUCCESS != error) return error;
	    error = mb->tag_set_data(v2hf_lfid, &v, 1, &ind);
	    if (MB_SUCCESS != error) return error;
	    break;
	  }
	  else if ((vcid!=0) && (sib_cids[ind]==0)){
	    error = mb->tag_set_data(v2hf_cid, &v, 1, &tet);
	    if (MB_SUCCESS != error) return error;
	    error = mb->tag_set_data(v2hf_lfid, &v, 1, &ind);
	    if (MB_SUCCESS != error) return error;
	  }
	}
      }
    }
    
    std::cout<<"Finished creating incident half-faces"<<std::endl;
    return MB_SUCCESS;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::determine_border_vertices_tet( Range &cells, Tag sibhfs_cid, Tag isborder)
  {
    ErrorCode error;
    EntityHandle start_cell = *cells.begin();
    
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    int val= 1;
    std::vector<EntityHandle> conn(nvpc);
    std::vector<EntityHandle> sib_cids(nfpc);

    for(Range::iterator t= cells.begin(); t !=cells.end(); ++t){
      conn.clear();     
      error = mb->get_connectivity(&*t, 1, conn);
      if (MB_SUCCESS != error) return error;

      sib_cids.clear();
      error = mb->tag_get_data(sibhfs_cid, &*t, 1, &sib_cids[0]);   
      if (MB_SUCCESS != error) return error;
      
      for (int i = 0; i < nfpc; ++i){	
	if (sib_cids[i]==0){
	  int nvF = lConnMap3D[index].hf2v_num[i];
	  
	  for (int j = 0; j < nvF; ++j){
	    int ind = lConnMap3D[index].hf2v[i][j];
	    error = mb->tag_set_data(isborder, &conn[ind], 1, &val);	    
	    if (MB_SUCCESS != error) return error;
	  }
	}	
      }
    }    

    return MB_SUCCESS;
  }

 //////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_3d( EntityHandle eid,  Range &cells, Tag sibhfs_cid, Tag sibhfs_lfid, Tag v2hf_cid, bool *etags, std::vector<EntityHandle> &cells_1ring, std::vector<int> &leids_1ring)
  {
    ErrorCode error; 
    
    EntityHandle cid=0;
    int leid=0;
    //Find one incident tet
    error = find_matching_implicit_edge_in_cell(eid, cells, sibhfs_cid, v2hf_cid, etags, &cid, &leid);
    if (MB_SUCCESS != error) return error;
    
    std::cout<<"cid = "<<cid<<", leid = "<<leid<<std::endl;

    //Find all incident tets
    error =get_upward_incidences_3d(cid, leid, sibhfs_cid, sibhfs_lfid, cells_1ring, leids_1ring);
    if (MB_SUCCESS != error) return error;

    return MB_SUCCESS;
  }

  //////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_3d( EntityHandle cid,  int leid, Tag sibhfs_cid, Tag sibhfs_lfid, std::vector<EntityHandle> &cids_1ring, std::vector<int> &leids_1ring)
  {
    ErrorCode error;

    int index = get_index_from_type(cid);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    EntityHandle start_cell = cid;
    cids_1ring.push_back(cid);
    leids_1ring.push_back(leid);

    //Loop over the two incident half-faces on this edge
    for(int i=0; i<2; i++)
      {
	std::vector<EntityHandle> conn(nvpc);
	error =mb->get_connectivity(&cid, 1, conn);
	if (MB_SUCCESS != error) return error;
	
	int id = lConnMap3D[index].e2v[leid][i];
	EntityHandle vert2 = conn[id];
	EntityHandle cur_cell = start_cell;
	int cur_leid = leid;
	int edge_ver = i;

	std::cout<<"id = "<<id<<", vert2 = "<<vert2<<", cur_cell = "<<cur_cell<<", cur_leid = "<<cur_leid<<", edge_ver = "<<edge_ver<<std::endl;

	while(true){
	  int lfid = lConnMap3D[index].e2hf[cur_leid][edge_ver];
	  
	  std::vector<EntityHandle> sibcids(nfpc);
	  std::vector<int> siblfids(nfpc);
	  error = mb->tag_get_data(sibhfs_cid, &cur_cell,1, &sibcids[0]);
	  if (MB_SUCCESS != error) return error;
	  error = mb->tag_get_data(sibhfs_lfid, &cur_cell, 1, &siblfids[0]);
	  if (MB_SUCCESS != error) return error;
	  
	  cur_cell = sibcids[lfid];
	  int sibhf = siblfids[lfid];
	  
	  //Check if loop reached starting cell or a boundary
	  if (cur_cell == start_cell ||  cur_cell==0)
	    break;
	  

	  std::vector<EntityHandle> temp_conn(nvpc);
	  error =mb->get_connectivity(&cur_cell, 1, temp_conn);
	  if (MB_SUCCESS != error) return error;

	  //Find the local edge id wrt to sibhf
	  int temp_lvid = -1;
	  int nv_curF = lConnMap3D[index].hf2v_num[sibhf];
	  
	  for (int j=0; j<nv_curF; j++){
	    temp_lvid = lConnMap3D[index].hf2v[sibhf][j];
	    EntityHandle temp_vert2 = temp_conn[temp_lvid];
	    
	    if (vert2 == temp_vert2){
	      cur_leid = lConnMap3D[index].f2leid[sibhf][j];
	      std::cout<<" cur_cell ="<<cur_cell<<", cur_leid = "<<cur_leid<<std::endl;
	      break;
	    }
	  }

	  if (temp_lvid == lConnMap3D[index].e2v[cur_leid][0])
	    edge_ver = 0;
	  else
	    edge_ver = 1;
        

	  //Memory allocation
	  if (cids_1ring.capacity()<=cids_1ring.size()){
	    if (100<cids_1ring.size()){
	      //std::cout<<"Too many incident tets"<<std::endl;
	      break;
	    }
	    cids_1ring.reserve(20+cids_1ring.size());
	    leids_1ring.reserve(20+leids_1ring.size());
	  }

	  //insert new incident tetrahedra and local edge ids
	  cids_1ring.push_back(cur_cell);
	  leids_1ring.push_back(cur_leid);
	}
	//Loop back
	if (cur_cell != 0)
	  break;
      }
    return MB_SUCCESS;
  }
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode  ArrayHalfFacetMDS::get_upward_incidences_3d( EntityHandle fid,  Range &cells, Tag sibhfs_cid, Tag v2hf_cid,  bool *ctags, std::vector<EntityHandle> &adj_entities)
  {
    ErrorCode error;
    EntityHandle start_cell = *cells.begin();
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    int nvF = local_maps_2d(fid); 
    std::vector<EntityHandle> fid_verts(nvF);
    error = mb->get_connectivity(&fid, 1, fid_verts);
    if (MB_SUCCESS != error) return error;

    // Step 1: Find matching half-face
    EntityHandle cid; int lid;
    error = mb->tag_get_data(v2hf_cid, &fid_verts[0], 1, &cid);
    if (MB_SUCCESS != error) return error;
  
    if (cid != 0){    
      std::vector<EntityHandle> trackcells;
      std::stack<EntityHandle> Stkcells;
      Stkcells.push(cid);

      int * lv= new int[nvF];           
      bool found = false;

      while (!Stkcells.empty()){
	cid = Stkcells.top();
	Stkcells.pop();
	ctags[cid-start_cell] = true;
	trackcells.push_back(cid);

	for (int l = 0; l < nvF; ++l)
	  lv[l] = -1;       

	std::vector<EntityHandle> conn(nvpc);
	error = mb->get_connectivity(&cid, 1, conn);
	if (MB_SUCCESS != error) return error;

	for(int i = 0; i < nfpc; ++i){
	  int nv_curF = lConnMap3D[index].hf2v_num[i];
	  if (nv_curF != nvF)
	    continue;

	  int counter = 0;
	  for(int l = 0; l < nvF; ++l){
	    int ind = lConnMap3D[index].hf2v[i][l];
	    if (conn[ind] == fid_verts[l]){
	      lv[l] = ind;
	      counter += 1;
	    }
	  }

	  if (counter == nvF){
	    found = true;
	    lid = i;
	
	    for (std::vector<EntityHandle>::iterator it = trackcells.begin(); it != trackcells.end(); ++it){
	      ctags[*it - start_cell] = false;
	    }
	    break;
	  }
	}

	std::vector<EntityHandle> sib_cids(nfpc);
	EntityHandle ngb; int ngbtag;
	error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sib_cids[0]);
	if (MB_SUCCESS != error) return error;

	int lv0 = lv[0];
	int nhf_thisv = lConnMap3D[index].v2hf_num[lv0];

	for (int i = 0; i < nhf_thisv; ++i){
	  int ind = lConnMap3D[index].v2hf[lv0][i]; 	  
	  ngb = sib_cids[ind]; 
	 
	  if (ngb) {
	    ngbtag = ctags[ngb-start_cell];       
	    if  (!ngbtag){
	      Stkcells.push(ngb);
	    }
	  }
	}
      }
      
      // Step 2: Collect incident cells
      if (found){
	error = get_upward_incidences_3d(cid, lid, nfpc, sibhfs_cid, adj_entities);
	if (MB_SUCCESS != error) return error;
      }

      delete [] lv;
    }

    return MB_SUCCESS;
  }
  ///////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_upward_incidences_3d( EntityHandle cid,  int lfid, int nfpc, Tag sibhfs_cid, std::vector<EntityHandle> &adj_entities)
  {
    ErrorCode error;
    std::vector<EntityHandle> sibcids(nfpc); 
    error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sibcids[0]);
    if (MB_SUCCESS != error) return error;

    EntityHandle sibcid = sibcids[lfid];
    if (sibcid > 0)
      {
	adj_entities.push_back(cid);
	adj_entities.push_back(sibcid);
      }
    else if (sibcid == 0)
      {
	adj_entities.push_back(cid);
      }

    return MB_SUCCESS;
  }
  /////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::find_matching_implicit_edge_in_cell( EntityHandle eid,  Range &cells, Tag sibhfs_cid, Tag v2hf_cid, bool *ctags, EntityHandle *cid, int *leid)
  {
    ErrorCode error;
    
    // Find the edge vertices
    std::vector<EntityHandle> econn(2);
    error = mb->get_connectivity(&eid, 1, econn);
    if (MB_SUCCESS != error) return error;

    EntityHandle v_start = econn[0], v_end = econn[1];
    
    EntityHandle cell2origin, cell2end;
    error =mb->tag_get_data(v2hf_cid, &v_start, 1, &cell2origin);
    if (MB_SUCCESS != error) return error;
    error =mb->tag_get_data(v2hf_cid, &v_end, 1, &cell2end);
    if (MB_SUCCESS != error) return error;
    
    if (cell2origin == 0|| cell2end == 0){
      return MB_SUCCESS;
    }
    
    EntityHandle start_cell = *cells.begin();
    int index = get_index_from_type(start_cell);
    int nvpc = lConnMap3D[index].num_verts_in_cell;
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    std::vector<EntityHandle> trackcells;
    std::queue<EntityHandle> cellq; 
    cellq.push(cell2origin);
    
    while (!cellq.empty()){
      //pop out the oldest one and mark as checked in ctags
      EntityHandle cell_id = cellq.front();
      cellq.pop();
      trackcells.push_back(cell_id);
      ctags[cell_id - start_cell] = true;
      
      bool found = false;
      int lvid = -1, ltv = -1;
      
      std::vector<EntityHandle> conn(nvpc);
      error =mb->get_connectivity(&cell_id, 1, conn);
      if (MB_SUCCESS != error) return error;

      //locate v_origin in poped out tet, check if v_end is in
      for (int i = 0; i<nvpc; i++){
	if (v_start == conn[i]){
	  lvid = i;
	} 
	else if (v_end == conn[i]){
	  found = true;
	  ltv = i;
	}
      }
      
      if (found){
         *cid = cell_id;
         *leid = lConnMap3D[index].lookup_leids[lvid][ltv];

	 for (std::vector<EntityHandle>::iterator it = trackcells.begin(); it != trackcells.end(); ++it){
	   ctags[*it - start_cell] = false;
	 }
         return MB_SUCCESS;
      }
      
      //push back new found unchecked incident tets of v_origin    
      std::vector<EntityHandle> ngb_cids(nfpc);
      error =mb->tag_get_data(sibhfs_cid,&cell_id,1,&ngb_cids[0]);
      if (MB_SUCCESS != error) return error;
      
      int nhf_thisv = lConnMap3D[index].v2hf_num[lvid];
      
      for (int i = 0; i < nhf_thisv; i++){
	int ind = lConnMap3D[index].v2hf[lvid][i];
	EntityHandle ngb = ngb_cids[ind];
	
	if (ngb != 0){
	  bool ngbtag = ctags[ngb-start_cell];
	  if  (ngbtag == false){
	    cellq.push(ngb);
	  }
	}
      }
    }
    
    return MB_SUCCESS;
  }
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ErrorCode ArrayHalfFacetMDS::get_neighbor_adjacencies_3d( EntityHandle cid, Tag sibhfs_cid, std::vector<EntityHandle> &adj_entities)
  {
    ErrorCode error;

    int index = get_index_from_type(cid);
    int nfpc = lConnMap3D[index].num_faces_in_cell;

    if (cid != 0 ){
      //adj_entities.push_back(cid);

      std::vector<EntityHandle> sibcids(nfpc);
      error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sibcids[0]);
      if (MB_SUCCESS != error) return error;      

      for (int lfid = 0; lfid < nfpc; ++lfid){      
	if (sibcids[lfid] != 0) 
	  adj_entities.push_back(sibcids[lfid]);
      }    
    }

    return MB_SUCCESS; 
  }
 
  //////////////////
  ErrorCode ArrayHalfFacetMDS::get_neighbor_adjacencies_3d( EntityHandle cid, Tag sibhfs_cid, Tag sibhfs_lfid, std::vector<EntityHandle> &ngb_cids, std::vector<int> &ngb_lfids)
  {
    ErrorCode error;
    
    int index = get_index_from_type(cid);
    int nfpc = lConnMap3D[index].num_faces_in_cell;
    
    if (cid != 0 ){
      std::vector<EntityHandle> sibcids(nfpc);
      std::vector<int> siblfids(nfpc);
      error = mb->tag_get_data(sibhfs_cid, &cid, 1, &sibcids[0]);
      if (MB_SUCCESS != error) return error;      
      error = mb->tag_get_data(sibhfs_lfid, &cid, 1, &siblfids[0]);
      if (MB_SUCCESS != error) return error;

      for (int lfid = 0; lfid < nfpc; ++lfid){      
	if (sibcids[lfid] != 0) 
	  {
	    ngb_cids.push_back(sibcids[lfid]);
	    ngb_lfids.push_back(siblfids[lfid]);
	  }
      }    
    }

    return MB_SUCCESS; 
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // namespace moab

